
# Generics

==Generics allow types and method to operate on objecs of various types.==
***

> Generics enabled types (classes and interfaces) to be parameters when defining classes, interfaces and methods. Type parameters provide a way to re-use the same code with different types. 

## Formal Parameters

These are **values** where as type parameters are **types**.

## Benefits

* Stronger type checking
* Elimination of type casts
* Implementation of Generic algorithms

## Type Parameters

E.g. ```<T1, T2, T3> ```  
The above are called *type parameters* or *type variables*.
Type variables can be any ***non-primitive types***.

## Generic Type Invocation

### Type Argument

When defining a generic type; the type being passed is called ***type argument***

```java
Box<Integer> box = new Box<>();
```

The ```<Integer>``` is called as type argument.

### The Diamond

```<>``` is called as ***The Diamond***. (Introduced from Java 7)

### Raw Type

Generic class or interface without any type arguments is called as ***Raw type***.

```java
Box rawBox = new Box();
```

When using raw type we  get pre-generic behaviour. 
==Raw type returns `Object` type.==

## Bounded Type Parameter

==Bounded type parameters restrict the types that can used as type arguments==

```java
public <U extends Number> void inspect() {/* */}
```

`extends` keyword is used for both **classes and interfaces**.

Multiple Bounds

:	```<T extends A & B & C>```

	if there are classes they should be mentioned first. Otherwise we get a compile time error.

If `A` is subclass of `B` then `Box<A>` *is **not** a sub-class of* `Box<B>`

```mermaid
flowchart BT
	id1["Collection&lt;String&gt;"] --> id2["List&lt;String&gt;"] --> id3["ArrayList&lt;String&gt;"]
```

## Type Inference

==Compilers ability to infer the type arguments by looking at each method invocation and corresponding declaration.==

If we want to mention the type arguments for a generic method:

```java
BoxDemo.<Integer>addBox(/* arguments */);
```

If we don't use [the diamond](#the-diamond) then the constructor is considered as a raw type and will give an unchecked coversion warning.

```java
Map<String, Integer> map = new HashMap();
```

[The diamond](#the-diamond)  is missing in the above example which will result in a raw type.

### Target Type

Java will infer the target type based on where an expression is present. 
Until Java 7; method arguments were nopt included in the target type inference. 

```java
void processString(List<String> stringList) {/* */}
```

In Java 7 : 
```java
processString(Collections.<String>emptyList());
```

In Java 8 : 
```java
processString(Collections.emptyList());
```

# Type Erasure
==To implement generics, the Java compiler applies type erasure== to:
- Replace all type parameters in generic types with their bounds or Object if the type parameters are unbounded. The produced bytecode, therefore, contains only ordinary classes, interfaces, and methods.
- Insert type casts if necessary to preserve type safety.
- Generate bridge methods to preserve polymorphism in extended generic types.

## Effects of Type Erasure and Bridge Methods
Given the following two classes:
```java
public class Node<T> {

    public T data;

    public Node(T data) { this.data = data; }

    public void setData(T data) {
        System.out.println("Node.setData");
        this.data = data;
    }
}

public class MyNode extends Node<Integer> {
    public MyNode(Integer data) { super(data); }

    public void setData(Integer data) {
        System.out.println("MyNode.setData");
        super.setData(data);
    }
}
```

Consider the following code:
```java
MyNode mn = new MyNode(5);
Node n = mn;            // A raw type - compiler throws an unchecked warning
n.setData("Hello");     // Causes a ClassCastException to be thrown.
Integer x = mn.data;
```

```java
After type erasure, this code becomes:

MyNode mn = new MyNode(5);
Node n = mn;            // A raw type - compiler throws an unchecked warning
                        // Note: This statement could instead be the following:
                        //     Node n = (Node)mn;
                        // However, the compiler doesn't generate a cast because
                        // it isn't required.
n.setData("Hello");     // Causes a ClassCastException to be thrown.
Integer x = (Integer)mn.data;
```

After type erasure, the Node and MyNode classes become:
```java
public class Node {

    public Object data;

    public Node(Object data) { this.data = data; }

    public void setData(Object data) {
        System.out.println("Node.setData");
        this.data = data;
    }
}

public class MyNode extends Node {

    public MyNode(Integer data) { super(data); }

    public void setData(Integer data) {
        System.out.println("MyNode.setData");
        super.setData(data);
    }
}
```

After type erasure, the method signatures do not match; the Node.setData(T) method becomes Node.setData(Object). As a result, the MyNode.setData(Integer) method does not override the Node.setData(Object) method.

To solve this problem and preserve the [polymorphism](https://docs.oracle.com/javase/tutorial/java/IandI/polymorphism.html) of generic types after type erasure, the Java compiler generates a bridge method to ensure that subtyping works as expected.

For the MyNode class, the compiler generates the following bridge method for setData:
```java
class MyNode extends Node {

    **// Bridge method generated by the compiler
    //
    public void setData(Object data) {
        setData((Integer) data);
    }**

    public void setData(Integer data) {
        System.out.println("MyNode.setData");
        super.setData(data);
    }

    // ...
}
```

The bridge method `MyNode.setData(object)` delegates to the original `MyNode.setData(Integer)` method. As a result, the `n.setData("Hello");` statement calls the method `MyNode.setData(Object)`, and a `ClassCastException` is thrown because `"Hello"` can't be cast to `Integer`.

# Restrictions on Generics
-   [Cannot Instantiate Generic Types with Primitive Types](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#instantiate)
-   [Cannot Create Instances of Type Parameters](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#createObjects)
-   [Cannot Declare Static Fields Whose Types are Type Parameters](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#createStatic)
-   [Cannot Use Casts or instanceof With Parameterized Types](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#cannotCast)
-   [Cannot Create Arrays of Parameterized Types](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#createArrays)
-   [Cannot Create, Catch, or Throw Objects of Parameterized Types](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#cannotCatch)
-   [Cannot Overload a Method Where the Formal Parameter Types of Each Overload Erase to the Same Raw Type](https://docs.oracle.com/javase/tutorial/java/generics/restrictions.html#cannotOverload)
