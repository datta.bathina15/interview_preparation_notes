# Queue

==A queue is a collection used to hold elements prior to processing.==

> Queue typically but not necesasrily follows a FIFO order

## Queue methods

Queue method exists in two forms.

1.  One that ~~returns~~ throws an exception upon failure
2.  Other return an special value e.g. *null* or *false* depending on the operation.

|  Type of Operation   |  Throws an exception   |  Returns a value   |
| --- | --- | --- |
| Insert | add(E) | offer(e) |
| Remove | remove() | poll() |
| Examine | element() | peek() |

## Bounded Queues

It is possible to restrict the number of elements held by a queue. Such queues are called as **Bounded Queues**.

Some implementations in `java.util.concurrent` ==are bounded==.
The implementations in `java.util` ==are not bounded==.

## Operations

`add()` throws `IllegalStateException` when the operation fails.
`offer(e)` method is ==inteded **solely** for bouded queues== returns ***false*** when it fails.

`remove()` and `poll()` removes the head element and return it. Which element is removed depends on the ordering policy of the queue. 

if `remove()` fails it returns `NoSuchElementException`.

`poll()` fails it return `null`.

The `element()` and `peek()` returns the head ==but do not remove it==. Failure scenario is similar to `remove()` and `poll()` respectively.

### Example 
The below program uses priority queue to perform heapsort.

```java
static <E> List<E> heapSort(Collection<E> c) {
    Queue<E> queue = new PriorityQueue<E>(c);
    List<E> result = new ArrayList<E>();

    while (!queue.isEmpty())
        result.add(queue.remove());

    return result;
}
```