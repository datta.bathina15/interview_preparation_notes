# General-Purpose List Implementations

## ArrayList

> Resizable-array implementation of the List interface. 

ArrayList offers constant-time positional access.

## LinkedList

> Doubly-linked list implementation of the List and Deque interfaces. Implements all optional list operations, and permits all elements (including null). 

==If you frequently add elements to the beginning of the List or iterate over the List to delete elements from its interior, you should consider using LinkedList.==

Positional access requires linear-time in a LinkedList and constant-time in an ArrayList.

LinkedList also implements the Queue interface.

# Special-Purpose List Implementations

> CopyOnWriteArrayList is a List implementation backed up by a copy-on-write array. This implementation is similar in nature to CopyOnWriteArraySet.

This implementation is well suited to maintaining event-handler lists, in which change is infrequent, and traversal is frequent and potentially time-consuming.

> If you need synchronization, a Vector will be slightly faster than an ArrayList synchronized with Collections.synchronizedList. But Vector has loads of legacy operations, so be careful to always manipulate the Vector with the List interface or else you won't be able to replace the implementation at a later time.

## Vector

The Vector class implements a growable array of objects. 

As of the **Java 2 platform v1.2, this class was retrofitted to implement the List interface**, making it a member of the Java Collections Framework. 

Unlike the new collection implementations, **Vector is synchronized**. If a thread-safe implementation is not needed, it is recommended to use ArrayList in place of Vector.