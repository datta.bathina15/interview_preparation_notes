# Components

Components are the main building block for Angular applications. Each component consists of:
- An HTML template that declares what renders on the page
- A TypeScript class that defines behavior
- A CSS selector that defines how the component is used in a template
- Optionally, CSS styles applied to the template

## Componant creation through Angular CLI

```
ng generate component <component-name>
```

## Specifying a component's CSS selector

==A selector instructs Angular to instantiate this component wherever it finds the corresponding tag in template HTML.==

```js
@Component({
  selector: 'app-component-overview',
})
```

## Defining a component's template

A template is a block of HTML that tells Angular how to render the component in your application. Define a template for your component in one of two ways: by referencing an external file, or directly within the component.

External template file example:
```js
@Component({
	selector: 'app-component-overview',
	templateUrl: './component-overview.component.html',
})
```

Template withing the component example:
```js
@Component({
	selector: 'app-component-overview',
	template: '<h1>Hello World!</h1>',
})
```

If the template has to span multiple lines use **'**.
```js
@Component({
  selector: 'app-component-overview',
  template: `
    <h1>Hello World!</h1>
    <p>This template definition spans multiple lines.</p>
  `
})
```

## Declaring a component's styles

To declare the styles for a component in a separate file, add a `styleUrls` property to the `@Component` decorator.
```js
@Component({
  selector: 'app-component-overview',
  templateUrl: './component-overview.component.html',
  styleUrls: ['./component-overview.component.css']
})
```

To declare the styles within the component, add a `styles` property to the `@Component` decorator that contains the styles you want to use.
```js
@Component({
  selector: 'app-component-overview',
  template: '<h1>Hello World!</h1>',
  styles: ['h1 { font-weight: normal; }']
})
```

## Lifecycle Hooks 🚲
A component instance has a lifecycle that starts when Angular instantiates the component class and renders the component view along with its child views.The lifecycle continues with change detection, as Angular checks to see when data-bound properties change, and updates both the view and the component instance as needed. The lifecycle ends when Angular destroys the component instance and removes its rendered template from the DOM.

Directives have a similar lifecycle, as Angular creates, updates, and destroys instances in the course of execution.

| Hook method                      | Purpose                                                                                                                                              | Timing                                                                                                                                                                                                                                                     |
|----------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ngOnChanges()                    | Respond when Angular sets or resets data-bound input properties. The method receives a SimpleChanges object of current and previous property values. | Called before ngOnInit() (if the component has bound inputs) and whenever one or more data-bound input properties change.   NOTE:  If your component has no inputs or you use it without providing any inputs, the framework will not call ngOnChanges().  |
| ngOnInit()                       | Initialize the directive or component after Angular first displays the data-bound properties and sets the directive or component's input properties. | Called once, after the first ngOnChanges(). ngOnInit() is still called even when ngOnChanges() is not (which is the case when there are no template-bound inputs).                                                                                         |
| ngDoCheck()                      | Detect and act upon changes that Angular can't or won't detect on its own.                                                                           | Called immediately after ngOnChanges() on every change detection run, and immediately after ngOnInit() on the first run.                                                                                                                                   |
| ngAfterContentInit()             | Respond after Angular projects external content into the component's view, or into the view that a directive is in.                                  | Called once after the first ngDoCheck().                                                                                                                                                                                                                   |
| ngAfterContentChecked()          | Respond after Angular checks the content projected into the directive or component.                                                                  | Called after ngAfterContentInit() and every subsequent ngDoCheck().                                                                                                                                                                                        |
| ngAfterViewInit()                | Respond after Angular initializes the component's views and child views, or the view that contains the directive.                                    | Called once after the first ngAfterContentChecked().                                                                                                                                                                                                       |
| ngAfterViewChecked()             | Respond after Angular checks the component's views and child views, or the view that contains the directive.                                         | Called after the ngAfterViewInit() and every subsequent ngAfterContentChecked().                                                                                                                                                                           |
| ngOnDestroy()                    | Cleanup just before Angular destroys the directive or component. Unsubscribe Observables and detach event handlers to avoid memory leaks.            | Called immediately before Angular destroys the directive or component.                                                                                                                                                                                     |


[If the above table is too ugly, then click here to see the table in angular documentation.](https://angular.io/guide/lifecycle-hooks#lifecycle-event-sequence)

## Sharing data between child and parent directives and components
### Parent to child
#input

The `@Input()` decorator in a child component or directive signifies that the property can receive its value from its parent component.

To use `@Input()`, you must configure the parent and child.

#### Configuring the child component
**src/app/item-detail/item-detail.component.ts**
```typescript
import { Component, Input } from '@angular/core'; // First, import Input
export class ItemDetailComponent {
  @Input() item = ''; // decorate the property with @Input()
}
```

**src/app/item-detail/item-detail.component.html**
```html
<p>
  Today's item: {{item}}
</p>
```

`@Input()` decorates the property `item`, which has a type of `string`, however, `@Input()` properties can have any type, such as `number`, `string`, `boolean`, or `object`. The value for `item` comes from the parent component.

####  Configuring the parent component
Use [property binding](https://angular.io/guide/property-binding) to bind the `item` property in the child to the `currentItem` property of the parent.
**src/app/app.component.html**
```html
<app-item-detail [item]="currentItem"></app-item-detail>
```

### Child to Parent
The `@Output` decorator in a child component or directive lets data flow from the child to the parent.
The child component uses the `@Output` property to raise an event to notify the parent of the change. To raise an event, an `@Output` must have the type of `EventEmitter`, which is a class in `@angular/core` that you use to emit custom events.

#### Configuring the child component
**src/app/item-output/item-output.component.ts**
```typescript
import { Output, EventEmitter } from '@angular/core';
export class ItemOutputComponent {

  @Output() newItemEvent = new EventEmitter<string>();

  addNewItem(value: string) {
    this.newItemEvent.emit(value);
  }
}
```

**src/app/item-output/item-output.component.html**
```html
<label for="item-input">Add an item:</label>
<input type="text" id="item-input" #newItem>
<button type="button" (click)="addNewItem(newItem.value)">Add to parent's list</button>
```
The `(click)` event is bound to the `addNewItem()` method in the child component class. The `addNewItem()` method takes as its argument the value of the `#newItem.value` property.

#### Configuring the parent component
**src/app/app.component.ts**
```typescript
export class AppComponent {
  items = ['item1', 'item2', 'item3', 'item4'];

  addItem(newItem: string) {
    this.items.push(newItem);
  }
}
```

**src/app/app.component.html**
```html
<app-item-output (newItemEvent)="addItem($event)"></app-item-output>
```

The event binding, `(newItemEvent)='addItem($event)'`, connects the event in the child, `newItemEvent`, to the method in the parent, `addItem()`.

The `$event` contains the data that the user types into the `<input>` in the child template UI.

[Simpler Example](https://angular.io/guide/component-interaction#parent-listens-for-child-event)

### Using `@Input` and `@Output` together
```typescript
<app-input-output
  [item]="currentItem"
  (deleteRequest)="crossOffItem($event)">
</app-input-output>
```

## Parent calls an `@ViewChild`
The parent-child relationship of the components is not established within each components respective _class_ with the _local variable_ technique. Because the _class_ instances are not connected to one another, the parent _class_ cannot access the child _class_ properties and methods.

When the parent component _class_ requires that kind of access, **_inject_** the child component into the parent as a **_ViewChild_**.

```typescript
import { AfterViewInit, ViewChild } from '@angular/core';
import { Component } from '@angular/core';
import { CountdownTimerComponent } from './countdown-timer.component';

@Component({
  selector: 'app-countdown-parent-vc',
  template: `
    <h3>Countdown to Liftoff (via ViewChild)</h3>
    <button type="button" (click)="start()">Start</button>
    <button type="button" (click)="stop()">Stop</button>
    <div class="seconds">{{ seconds() }}</div>
    <app-countdown-timer></app-countdown-timer>
  `,
  styleUrls: ['../assets/demo.css']
})
export class CountdownViewChildParentComponent implements AfterViewInit {

  @ViewChild(CountdownTimerComponent)
  private timerComponent!: CountdownTimerComponent;

  seconds() { return 0; }

  ngAfterViewInit() {
    // Redefine `seconds()` to get from the `CountdownTimerComponent.seconds` ...
    // but wait a tick first to avoid one-time devMode
    // unidirectional-data-flow-violation error
    setTimeout(() => this.seconds = () => this.timerComponent.seconds, 0);
  }

  start() { this.timerComponent.start(); }
  stop() { this.timerComponent.stop(); }
}
```

## Content projection
Content projection is a pattern in which you insert, or _project_, the content you want to use inside another component.

It's like react's prop.children. Like when you want to render the content passed in the component reference in the parent html.

| Content projection             | Details                                                                                                  |
|--------------------------------|----------------------------------------------------------------------------------------------------------|
| Single-slot content projection | With this type of content projection, a component accepts content from a single source.                  |
| Multi-slot content projection  | In this scenario, a component accepts content from multiple sources.                                     |
| Conditional content projection | Components that use conditional content projection render content only when specific conditions are met. |

```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-zippy-basic',
  template: `
    <h2>Single-slot content projection</h2>
    <ng-content></ng-content>
  `
})
export class ZippyBasicComponent {}
```

The most basic form of content projection is _single-slot content projection_. Single-slot content projection refers to creating a component into which you can project one component.

To create a component that uses single-slot content projection:

1.  [Create a component](https://angular.io/guide/component-overview#creating-a-component).
2.  In the template for your component, add an `<ng-content>` element where you want the projected content to appear.

[Full Documentation for other types of content projection](https://angular.io/guide/content-projection#content-projection)

