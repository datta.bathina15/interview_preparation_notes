# SELECT

^d9835b

```sql
SELECT column1, column2, ...
  FROM tablename;
  
SELECT * FROM tablename;
```

## Operators

### AND
```sql
SELECT column1, column2, ...
FROM table_name
WHERE condition1 AND condition2 AND condition3 ...;

-- Example
SELECT * FROM patients WHERE first_name='John' AND city='Toronto';
```

### OR
```sql
SELECT column1, column2, ...
FROM table_name
WHERE condition1 OR condition2 OR condition3 ...;

-- Example
SELECT * FROM patients WHERE city='Hamilton' OR city='Toronto';
```

### NOT
```sql
SELECT column1, column2, ...
FROM table_name
WHERE NOT condition;

-- Example
SELECT * FROM patients WHERE NOT province_id = 'ON';
```

## ORDER BY

```sql
SELECT column1, column2, ...
    FROM table_name
    ORDER BY column1, column2, ... ASC|DESC;

SELECT * FROM patients
    ORDER BY first_name;
	
-- Descending order example
SELECT * FROM patients
    ORDER BY first_name DESC;
```

### Multiple Columns
```sql
SELECT * FROM patients
    ORDER BY first_name, last_name;
	
SELECT * FROM patients
    ORDER BY first_name ASC, last_name DESC;
```

## LIKE

The LIKE operator is used in a WHERE clause to search for a specified pattern in a column.

There are two wildcards often used in conjunction with the LIKE operator:

- The percent sign (%) represents zero, one, or multiple characters

- The underscore sign (_) represents one, single character

```sql
SELECT column1, column2, ...
  FROM table_name
  WHERE column LIKE pattern;
```

| LIKE Operator                | Description                                                                  |
|------------------------------|------------------------------------------------------------------------------|
| WHERE first_name LIKE 'a%'   | Finds any values that start with "a"                                         |
| WHERE first_name LIKE '%a'   | Finds any values that end with "a"                                           |
| WHERE first_name LIKE '%or%' | Finds any values that have "or" in any position                              |
| WHERE first_name LIKE '\_r%' | Finds any values that have "r" in the second position |
| WHERE first_name LIKE 'a_%'  | Finds any values that start with "a" and are at least 2 characters in length |
| WHERE first_name LIKE 'a__%' | Finds any values that start with "a" and are at least 3 characters in length |
| WHERE first_name LIKE 'a%o'  | Finds any values that start with "a" and ends with "o" |

## IN

The IN operator allows you to specify multiple values in a WHERE clause.

```sql
SELECT column_name(s)
FROM table_name
WHERE column_name IN (value1, value2, ...);

-- OR

SELECT column_name(s)
FROM table_name
WHERE column_name IN (SELECT STATEMENT);

-- Example
SELECT * FROM patients
WHERE province_id IN ('SK', 'AB', 'MB');
```

## BETWEEN

The BETWEEN operator selects values within a given range. The values can be numbers, text, or dates.

```sql
SELECT column_name(s) FROM table_name
  WHERE column_name BETWEEN value1 AND value2;
  
-- The following SQL statement selects all patients with a weight between 100 and 120:
SELECT * FROM patients
  WHERE weight BETWEEN 100 AND 120;

-- To display the products outside the range of the previous example, use NOT BETWEEN:
SELECT * FROM patients
  WHERE weight NOT BETWEEN 100 AND 120;

-- Between with text
SELECT * FROM patients
  WHERE first_name BETWEEN 'Alex' AND 'Ben'

-- BETWEEN with IN Example
SELECT * FROM patients
	WHERE weight NOT BETWEEN 100 AND 120
	AND province_id NOT IN ('ON', 'SK', 'AB');
```

## GROUP BY

The GROUP BY statement groups rows that have the same values into summary rows, like "find the number of patients in each province".

```sql
SELECT column_name(s)
  FROM table_name
  WHERE condition
  GROUP BY column_name(s)
```

## HAVING
The HAVING clause was added to SQL because the WHERE keyword cannot be used with aggregate functions.

```sql
SELECT column_name(s)
  FROM table_name
  WHERE condition
  GROUP BY column_name(s)
  HAVING condition
  ORDER BY column_name(s);

--  Example
 SELECT COUNT(*), first_name
  FROM patients
  GROUP BY first_name
  HAVING count(*) > 30;
```

### HAVING vs. WHERE

The [WHERE](https://www.sqltutorial.org/sql-where/) clause applies the condition to individual rows before the rows are summarized into groups by the GROUP BY clause. However, the HAVING clause applies the condition to the groups after the rows are grouped into groups.

## DISTINCT

The SELECT DISTINCT statement is used to return only distinct (different) values.

```sql
SELECT DISTINCT column1, column2, ...
  FROM table_name;
  
-- Example
SELECT DISTINCT first_name FROM patients;
```

## NULL Values

It is not possible to test for NULL values with comparison operators, such as =, <, or <>.

We will have to use the IS NULL and IS NOT NULL operators instead.
#### IS  NULL
```sql
SELECT column_names
	FROM table_name
	WHERE column_name IS NULL;

-- Example
SELECT *
	FROM patients
	WHERE allergies IS NULL;
```

### IS NOT NULL

```sql
SELECT column_names
	FROM table_name
	WHERE column_name IS NOT NULL;
```

## Joins
**SQL Join** statement is used to combine data or rows from two or more tables based on a common field between them. Different types of Joins are as follows: 

- INNER JOIN
- LEFT JOIN
- RIGHT JOIN
- FULL JOIN
- CARTESIAN/CROSS JOIN
- SELF JOIN

### INNER JOIN
The INNER JOIN keyword selects all rows from both the tables as long as the condition is satisfied. This keyword will create the result-set by combining all rows from both the tables where the condition satisfies i.e value of the common field will be the same.

```sql
SELECT table1.column1,table1.column2,table2.column1,....
	FROM table1 
	INNER JOIN table2
	ON table1.matching_column = table2.matching_column;
```

![Inner Join|300](https://blog.codinghorror.com/content/images/uploads/2007/10/6a0120a85dcdae970b012877702708970c-pi.png)

### LEFT JOIN
This join returns all the rows of the table on the left side of the join and matches rows for the table on the right side of the join.

For the rows for which there is no matching row on the right side, the result-set will contain _null_. LEFT JOIN is also known as LEFT OUTER JOIN.

```sql
SELECT table1.column1,table1.column2,table2.column1,....
	FROM table1 
	LEFT JOIN table2
	ON table1.matching_column = table2.matching_column;
```

![Left Join|300](https://i.stack.imgur.com/VkAT5.png)

### RIGHT JOIN
RIGHT JOIN is similar to LEFT JOIN. 
This join returns all the rows of the table on the right side of the join and matching rows for the table on the left side of the join. 
==For the rows for which there is no matching row on the left side, the result-set will contain _null_. ==
RIGHT JOIN is also known as RIGHT OUTER JOIN.

```sql
SELECT table1.column1,table1.column2,table2.column1,....
	FROM table1 
	RIGHT JOIN table2
	ON table1.matching_column = table2.matching_column;
```

![Right Join|300](https://media.geeksforgeeks.org/wp-content/uploads/20220515095048/join.jpg)

### FULL JOIN
FULL JOIN creates the result-set by combining results of both LEFT JOIN and RIGHT JOIN. The result-set will contain all the rows from both tables. ==For the rows for which there is no matching, the result-set will contain _NULL_ values.==

![FULL Join|300](https://i.stack.imgur.com/3Ll1h.png)

```sql
SELECT table1.column1,table1.column2,table2.column1,....
	FROM table1 
	FULL JOIN table2
	ON table1.matching_column = table2.matching_column;
```

### CARTESIAN JOIN (CROSS JOIN)
The CARTESIAN JOIN is also known as CROSS JOIN. In a CARTESIAN JOIN there is a join for each row of one table to every row of another table. This usually happens when the matching column or WHERE condition is not specified.

- In the absence of a WHERE condition the CARTESIAN JOIN will behave like a CARTESIAN PRODUCT . i.e., the number of rows in the result-set is the product of the number of rows of the two tables.
- In the presence of WHERE condition this JOIN will function like a INNER JOIN.
- Generally speaking, Cross join is similar to an inner join where the join-condition will always evaluate to True

```sql
SELECT table1.column1 , table1.column2, table2.column1...
FROM table1
CROSS JOIN table2;
```

![Example](https://www.sqltutorial.org/wp-content/uploads/2018/01/SQL-CROSS-JOIN.png)

### SELF JOIN
In SELF JOIN a table is joined to itself. That is, each row of the table is joined with itself and all other rows depending on some conditions.

```sql
SELECT a.coulmn1 , b.column2
FROM table_name a, table_name b
WHERE some_condition;
```

To perform the self-join, we use either an [[DQL#INNER JOIN|inner join]] or [[DQL#LEFT JOIN|left join]] clause.

```sql
SELECT
	column1,
	column2,
	column3,
        ...
FROM
	table1 A
INNER JOIN table1 B ON B.column1 = A.column2;
```

