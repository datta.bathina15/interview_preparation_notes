| Interfaces | Hash table Implementations | Resizable array Implementations | Tree Implementations | Linked list Implementations | Hash table + Linked list Implementations |
| --- | --- | --- | --- | --- | --- |
| Set | HashSet |     | TreeSet |     | LinkedHashSet |
| List |     | ArrayList |     | LinkedList |     |
| Queue |     |     |     |     |     |
| Deque |     | ArrayDeque |     | LinkedList |     |
| Map | HashMap |     | TreeMap |     | LinkedHashMap |

> Each of the general-purpose implementations provides all optional operations contained in its interface. All permit null elements, keys, and values. None are synchronized (thread-safe). **All have fail-fast iterators**, which detect illegal concurrent modification during iteration and fail quickly and cleanly rather than risking arbitrary, non-deterministic behavior at an undetermined time in the future. All are Serializable and all support a public clone method.

