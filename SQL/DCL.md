# GRANT

```sql
GRANT privileges_names ON object TO user;
-- Example
GRANT SELECT, INSERT, DELETE, UPDATE ON Users TO 'Amit'@'localhost;
```

![Privilleges](https://media.geeksforgeeks.org/wp-content/uploads/sqltable.jpg)

# REVOKE

```sql
revoke privilege_name on object_name
	from {user_name | public | role_name}
	
-- Example
revoke insert, 
	select on accounts from Ram
```
