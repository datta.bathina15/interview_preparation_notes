These SQL commands are mainly categorized into four categories as: 
1. [[DDL]] – Data Definition Language
2. [[DQL]] – Data Query Language
3. [[DML]] – Data Manipulation Language
4. [[DCL]] – Data Control Language

![Categories of SQL Command](https://media.geeksforgeeks.org/wp-content/uploads/20210920153429/new.png)

# DDL

DDL or Data Definition Language actually consists of the SQL commands that can be used to define the database schema.

- [[DDL#CREATE|CREATE]]: This command is used to create the database or its objects (like table, index, function, views, store procedure, and triggers). 
- [[DDL#DROP|DROP]]: This command is used to delete objects from the database.
- [[DDL#ALTER|ALTER]]: This is used to alter the structure of the database.
- [[DDL#TRUNCATE|TRUNCATE]]: This is used to remove all records from a table, including all spaces allocated for the records are removed.
- COMMENT: This is used to add comments to the data dictionary.
- RENAME: This is used to rename an object existing in the database.

# DQL

DQL statements are used for performing queries on the data within schema objects. 

- [[DQL#^d9835b|SELECT]]: It is used to retrieve data from the database.

# DML

The SQL commands that deals with the manipulation of data present in the database belong to DML or Data Manipulation Language and this includes most of the SQL statements. 

- INSERT : It is used to insert data into a table.
- UPDATE: It is used to update existing data within a table.
- DELETE : It is used to delete records from a database table.
- LOCK: Table control concurrency.
- CALL: Call a PL/SQL or JAVA subprogram.
- EXPLAIN PLAN: It describes the access path to data.

# DCL

DCL includes commands such as GRANT and REVOKE which mainly deal with the rights, permissions, and other controls of the database system. 

- GRANT: This command gives users access privileges to the database.
- REVOKE: This command withdraws the user’s access privileges given by using the GRANT command.

# TCL – Transaction Control Language

- COMMIT: Commits a Transaction.
- ROLLBACK: Rollbacks a transaction in case of any error occurs.
- SAVEPOINT:Sets a savepoint within a transaction.
- SET TRANSACTION: Specify characteristics for the transaction.

