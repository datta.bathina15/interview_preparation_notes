# Wildcards

`?` is called a wildcard to represent ==unknown types==.

Wildcards are used as type parameters, field or local variables & some times as return types.

***Wildcards are ==never== used as ==type arguments for a generic method invocation==, a genric class instance creation or a super type.***

## Upperbound

```java
<? extends UpperBoundClass>
```

E.g:

```java
    public static void process(List<? extends Number> list) { /**/ }
```

## Unbounded Wildcards

`<?>`

E.g. `public static void process(List<?> list) { /* */ }`

## Use cases

- Writing a method that can be implement using functionality provided in object class.
- When the code using the methods in the generic class do not depend on the type parameter.

E.g. `Class<?>` is often used because most of the methods in `Class<T>` do not depend on **T**.

`List<?>` allows ==null== to be inserted.
`List<Object>` doesn't allow null.

[Ref video on upper and lower bounds (or covariance and contravariance)](https://youtu.be/tlAGSScIu_w)

## Lower bound 
`<T super Integer>`

Lower bounds means the type arguments can be the class or it's super classes.

# Wildcard Capture
Compiler infers the type of a wildcard from the code. 