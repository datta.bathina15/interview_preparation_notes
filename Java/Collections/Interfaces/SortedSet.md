# SortedSet

A `SortedSet` is a `Set` in which elementes are stored in **ascending** order.

In addition to the operations provided by `Set`, `SortedSet` has the following operations:
* Range-View
* Endpoints
* Comparator Access

```java
public interface SortedSet<E> extends Set<E> {
    // Range-view
    SortedSet<E> subSet(E fromElement, E toElement);
    SortedSet<E> headSet(E toElement);
    SortedSet<E> tailSet(E fromElement);

    // Endpoints
    E first();
    E last();

    // Comparator access
    Comparator<? super E> comparator();
}
```

