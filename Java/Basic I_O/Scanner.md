==A Scanner breaks its input into tokens using a delimiter pattern, which by default matches whitespace.== The resulting tokens may then be converted into values of different types using the various next methods. 

For example, this code allows a user to read a number from System.in:

```java
Scanner sc = new Scanner(System.in);
int i = sc.nextInt();
```

As another example, this code allows long types to be assigned from entries in a file myNumbers:

```java
Scanner sc = new Scanner(new File("myNumbers"));
while (sc.hasNextLong()) {
  long aLong = sc.nextLong();
}
```    

The scanner can also use delimiters other than whitespace. This example reads several items in from a string:

```java
String input = "1 fish 2 fish red fish blue fish";
Scanner s = new Scanner(input).useDelimiter("\\s*fish\\s*");
System.out.println(s.nextInt());
System.out.println(s.nextInt());
System.out.println(s.next());
System.out.println(s.next());
s.close();
```     

prints the following output:

 1
 2
 red
 blue
     