# Collection interface
The collection interface is the most basic interface present in the collections framework.
The `Collection` interface contains methods that perform basic operations, such as 
- `int size()`
- `boolean isEmpty()`
- `boolean contains(Object element)`
- `boolean add(E element)`
- `boolean remove(Object element)`
- `Iterator<E> iterator()`
