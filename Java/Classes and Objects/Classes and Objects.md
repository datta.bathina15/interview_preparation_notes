# Access Specifiers

| Modifier    | Class | Package | Subclass | World |
|-------------|-------|---------|----------|-------|
| public      | Y     | Y       | Y        | Y     |
| protected   | Y     | Y       | Y        | N     |
| no modifier(default) | Y     | Y       | N        | N     |
| private     | Y     | N       | N        | N     |

Access level modifiers determine whether other classes can use a particular field or invoke a particular method. There are two levels of access control:
- At the top level—`public`, or _package-private_ (no explicit modifier).
- At the member level—`public`, `private`, `protected`, or _package-private_ (no explicit modifier).

Outer classes can only be declared `public` or _package private_.

# Instantiating a Class
The new operator instantiates a class by allocating memory for a new object and returning a reference to that memory. The new operator also invokes the object constructor.

The new operator requires a single, postfix argument: a call to a constructor. **The name of the constructor provides the name of the class to instantiate.**

# `this`
Within an instance method or a constructor, `this` is a reference to the _current object_ — the object whose method or constructor is being called. You can refer to any member of the current object from within an instance method or a constructor by using `this`.

## Using `this` with a Field
```java
public class Point {
    public int x = 0;
    public int y = 0;
    
    //constructor
    public Point(int x, int y) {
        this.x = x; // <----
        this.y = y; // <----
    }
}
```

## Using `this` with a Constructor
From within a constructor, you can also use the `this` keyword to call another constructor in the same class. Doing so is called an **_explicit constructor invocation_**.

```java
public class Rectangle {
    private int x, y;
    private int width, height;
        
    public Rectangle() {
        this(0, 0, 1, 1); // <----
    }
    public Rectangle(int width, int height) {
        this(0, 0, width, height); // <----
    }
    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    ...
}
```

