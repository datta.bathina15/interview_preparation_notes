# Locks

Synchronized code relies on a simple kind of reentrant lock. This kind of lock is easy to use, but has many limitations. More sophisticated locking idioms are supported by the `java.util.concurrent.locks` package.

Lock objects work very much like the implicit locks used by synchronized code. As with implicit locks, only one thread can own a Lock object at a time. Lock objects also support a wait/notify mechanism, through their associated [Condition](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/locks/Condition.html) objects.

**The biggest advantage of Lock** objects over implicit locks **is their ability to back out of an attempt to acquire a lock**. The tryLock method backs out if the lock is not available immediately or before a timeout expires (if specified). The lockInterruptibly method backs out if another thread sends an interrupt before the lock is acquired.

# Executors

There's a close connection between the task being done by a new thread, as defined by its Runnable object, and the thread itself, as defined by a Thread object. This works well for small applications, but in large-scale applications, it makes sense to **separate thread management and creation from the rest of the application. Objects that encapsulate these functions are known as executors.**

## Executor

An object that executes submitted Runnable tasks. This interface provides a way of decoupling task submission from the mechanics of how each task will be run, including details of thread use, scheduling, etc. An Executor is normally used instead of explicitly creating threads. For example, rather than invoking new `Thread(new(RunnableTask())).start()` for each of a set of tasks, you might use:
```java
 Executor executor = anExecutor;
 executor.execute(new RunnableTask1());
 executor.execute(new RunnableTask2());
```

> However, the definition of execute is less specific. The low-level idiom creates a new thread and launches it immediately. Depending on the Executor implementation, execute may do the same thing, **but is more likely to use an existing worker thread to run r, or to place r in a queue to wait for a worker thread to become available**.

### The ExecutorService Interface

> The ExecutorService interface supplements execute with a similar, but more versatile **`submit`** method. Like execute, `submit` accepts `Runnable` objects, but also accepts `Callable` objects, which allow the task to return a value.
> The submit method returns a Future object, which is used to retrieve the Callable return value and to manage the status of both Callable and Runnable tasks.

An executor that provides methods to **manage termination** and methods that procude a `Future` for tracking progress of one or more asynchronous tasks.

An ExecutorService can be shut down, which will cause it to reject new tasks. Two different methods are provided for shutting down an ExecutorService. The `shutdown()` method will allow previously submitted tasks to execute before terminating, while the `shutdownNow()` method prevents waiting tasks from starting and attempts to stop currently executing tasks.

#### Callable

`Callable` is a Functional interface. 

> A task that returns a result and may throw an exception. Implementors define a single method with no arguments called call.

> The Callable interface is similar to Runnable, in that both are designed for classes whose instances are potentially executed by another thread. A Runnable, however, does not return a result and cannot throw a checked exception.

#### Future

A Future represents the result of an asynchronous computation. Methods are provided to check if the computation is complete, to wait for its completion, and to retrieve the result of the computation.

E.g:
```java
interface ArchiveSearcher { String search(String target); }
 class App {
   ExecutorService executor = ...
   ArchiveSearcher searcher = ...
   void showSearch(final String target)
       throws InterruptedException {
     Future<String> future
       = executor.submit(new Callable<String>() {
         public String call() {
             return searcher.search(target);
         }});
     displayOtherThings(); // do other things while searching
     try {
       displayText(future.get()); // use future
     } catch (ExecutionException ex) { cleanup(); return; }
   }
 }
```

### The ScheduledExecutorService Interface

> The ScheduledExecutorService interface supplements the methods of its parent ExecutorService with `schedule`, which executes a Runnable or Callable task after a specified delay. 

In addition, the interface defines `scheduleAtFixedRate` and `scheduleWithFixedDelay`, which executes specified tasks repeatedly, at defined intervals.

The schedule methods create tasks with various delays and return a task object that can be used to cancel or check execution. The `scheduleAtFixedRate` and `scheduleWithFixedDelay` methods create and execute tasks that run periodically until cancelled.

Commands submitted using the Executor.execute(Runnable) and ExecutorService submit methods are **scheduled with a requested delay of zero**. 

**Zero and negative delays (but not periods) are also allowed** in schedule methods, and are t**reated as requests for immediate execution**.

## Thread Pools

Thread objects use a significant amount of memory, and in a large-scale application, allocating and deallocating many thread objects creates a significant memory management overhead. **Using worker threads minimizes the overhead due to thread creation.**

Most of the **executor** implementations in `java.util.concurrent` use **thread pools, which consist of worker threads**. This kind of thread exists separately from the Runnable and Callable tasks it executes and is often used to execute multiple tasks.

One **common type** of thread pool is the **fixed thread pool**. This type of pool always has a specified number of threads running; if a thread is somehow terminated while it is still in use, it is automatically replaced with a new thread. **Tasks are submitted to the pool via an internal queue**, which holds extra tasks whenever there are more active tasks than threads.

An important advantage of the fixed thread pool is that applications using it degrade gracefully.

A simple way to create an executor that uses a fixed thread pool is to invoke the `newFixedThreadPool` factory method in `java.util.concurrent.Executors`.

The `Executors` class consists of several utility methods for  `Executor`, `ExecutorService`, `ScheduledExecutorService`, `ThreadPool` and `Callable` classes. 

## Fork/Join Framework

The fork/join framework is an implementation of the ExecutorService interface that helps you take advantage of multiple processors. It is designed for work that can be broken into smaller pieces recursively. 

As with any ExecutorService implementation, the fork/join framework distributes tasks to worker threads in a thread pool. 

==The fork/join framework is distinct because it uses a work-stealing algorithm.== Worker threads that run out of things to do can steal tasks from other threads that are still busy.

The center of the fork/join framework is the `ForkJoinPool` class, an extension of the `AbstractExecutorService` class. `ForkJoinPool` implements the core work-stealing algorithm and can execute ForkJoinTask processes.

Use `ForkJoinTask` class to create the tasks to be performed. Typically it's subclasses are used, like:
* `RecursiveTask` - Recursive task which returns results.
* `RecursiveAction` - Does not return results.

After your `ForkJoinTask` subclass is ready, create the object that represents all the work to be done and pass it to the `invoke()` method of a `ForkJoinPool` instance.

[Example program - ForkBlur](https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/essential/concurrency/examples/ForkBlur.java)

### Standard Implementations

In Java SE, the `parallelsort` method in `Arrays` class used the fork/join framework. 

`java.util.streams` package also uses the framework. 

# Concurent Collections

- **BlockingQueue** - See /collections/implementations/queue
	- defines a first-in-first-out data structure that blocks or times out when you attempt to add to a full queue, or retrieve from an empty queue.
- **ConcurrentMap** is a subinterface of `java.util.Map` that defines **useful atomic operations**. These operations **remove or replace a key-value pair only if the key is present, or add a key-value pair only if the key is absent.** Making these operations atomic helps avoid synchronization. The standard general-purpose implementation of `ConcurrentMap` is `ConcurrentHashMap`, which is a concurrent analog of `HashMap`.
- **ConcurrentNavigableMap** is a subinterface of `ConcurrentMap` that supports approximate matches. The standard general-purpose implementation of `ConcurrentNavigableMap` is `ConcurrentSkipListMap`, which is a concurrent analog of `TreeMap`.

All of these collections help avoid Memory Consistency Errors by defining a happens-before relationship between an operation that adds an object to the collection with subsequent operations that access or remove that object.

# Atomic Variables

The [java.util.concurrent.atomic](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/package-summary.html) package defines classes that support atomic operations on single variables. All classes have get and set methods that work like reads and writes on volatile variables. That is, a set has a happens-before relationship with any subsequent get on the same variable. The atomic compareAndSet method also has these memory consistency features, as do the simple atomic arithmetic methods that apply to integer atomic variables.

E.g:
```java
import java.util.concurrent.atomic.AtomicInteger;

class AtomicCounter {
    private AtomicInteger c = new AtomicInteger(0);

    public void increment() {
        c.incrementAndGet();
    }

    public void decrement() {
        c.decrementAndGet();
    }

    public int value() {
        return c.get();
    }

}
```

# Concurrent Random Numbers

In JDK 7, `java.util.concurrent` includes a convenience class, `ThreadLocalRandom`, for applications that expect to use random numbers from multiple threads or ForkJoinTasks.

E.g:
```java
int r = ThreadLocalRandom.current() .nextInt(4, 77);
```