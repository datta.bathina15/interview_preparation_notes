# Comparable

==A `Comprable` interface provides the **natural ordering of a class**, which allows the objects of a class to be sorted automatically.==

> This interface imposes a total ordering on the objects of each class that implements it.

If we try to sort a list, the elements which do not implement `Comparable`, `Collections.sort(list)` will throw a `ClassCastException`.

Similarly, `Collections.sort(list, comparator)` will throw a `ClassCastException` if the elements cannot be compared using the `comprator`.

Elements that can be compared to one another are called mutually comparable.

The Comparable interface consists of the following method.
```java
public interface Comparable<T> {
    public int compareTo(T o);
}
```

The compareTo method compares the receiving object with the specified object and returns a negative integer, 0, or a positive integer depending on whether the receiving object is less than, equal to, or greater than the specified object. If the specified object cannot be compared to the receiving object, the method throws a ClassCastException.

# Comparator

==Comprator is used to sort the list of elements which do not implement the `Comprable` interface or to sort some objects in an order other than their natural ordering.==

> A comparison function, which imposes a total ordering on some collection of objects. 

> An object that encapsulated ordering.

The Comparator interface consists of a single method.
```java
public interface Comparator<T> {
    int compare(T o1, T o2);
}
```
The compare method compares its two arguments, returning a negative integer, 0, or a positive integer depending on whether the first argument is less than, equal to, or greater than the second. If either of the arguments has an inappropriate type for the Comparator, the compare method throws a ClassCastException.

Much of what was said about `Comparable` applies to `Comparator` as well. Writing a `compare` method is nearly identical to writing a `compareTo` method, except that the former gets both objects passed in as arguments.

# Consistent with Equals

The ordering imposed by a comparator c on a set of elements S is said to be consistent with equals if and only if c.compare(e1, e2)==0 has the same boolean value as e1.equals(e2) for every e1 and e2 in S.

Applicable to both comparable and comprator. 