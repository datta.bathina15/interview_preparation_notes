# Dequeue

Pronounced 'Deck'. It's a ==double ended queue==.

It supports insertions and deletions from both ends of the queue.

## Dequeue methods

| Type of Operation | First Element (Beginning of the `Deque` instance) | Last Element (End of the `Deque` instance) |
| --- | --- | --- |
| Insert | addFirst(e)<br><br>offerFirst(e) | addLast(e)<br><br>offerLast(e) |
| Remove | removeFirst()<br><br>pollFirst() | removeLast()<br><br>pollLast() |
| Examine | getFirst()<br><br>peekFirst() | getLast()<br><br>peekLast() |

## Additional Methods provided in interface

* `removeFirstOccurance(e)` 
*  `removeLastOccurance(e)`

Both methods remove the element as the name suggests. If the element does not exist; the Deque **remains unchanges**. 

The methods return a boolean based on the operation. They return `true` if the element **exists in the `Deque`**.