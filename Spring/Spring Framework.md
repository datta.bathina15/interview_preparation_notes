# Tight Coupling

Tight coupling is the situation in which a component is tightly coupled with it's dependencies.
In general, tight coupling is usually not good because it reduces the flexibility and re-usability of the code.

- The tightly coupled object is an object that needs to know about other objects and is usually highly dependent on each other's interfaces.
- Changing one object in a tightly coupled application often requires changes to a number of other objects.

# Loose Coupling

Loose coupling depends on interfaces to reduce the dependecy of a object.

- Loose coupling is a design goal to reduce the inter-dependencies between components of a system with the goal of reducing the risk that changes in one component will require changes in any other component.
- Loose coupling is a much more generic concept intended to increase the flexibility of the system, make it more maintainable and makes the entire framework more stable.

# Inversion of Control

> It is a process whereby objects define their dependencies (that is, the other objects they work with) only through constructor arguments, arguments to a factory method, or properties that are set on the object instance after it is constructed or returned from a factory method. The container then injects those dependencies when it creates the bean. This process is fundamentally the inverse (hence the name, Inversion of Control) of the bean itself controlling the instantiation or location of its dependencies by using direct construction of classes or a mechanism such as the Service Locator pattern. IoC is also known as dependency injection (DI).

Instead of bean creating or getting it's dependencies, the container (or the framework) that creates the bean injects the depenencied to the bean.

[Martin Fowler's Definition](https://martinfowler.com/bliki/InversionOfControl.html)

(also known as the Hollywood Principle - "Don't call us, we'll call you")

## Containers

==The `BeanFactory` interface provides an advanced configuration mechanism capable of managing any type of object.== `ApplicationContext` is a **sub-interface** of `BeanFactory`. It adds:

- Easier integration with Spring’s AOP features
- Message resource handling (for use in internationalization)
- Event publication
- Application-layer specific contexts such as the WebApplicationContext for use in web applications.

The `BeanFactory` provides the **configuration framework and basic functionality**, and the `ApplicationContext` adds more **enterprise-specific functionality**.

A bean is an object that is instantiated, assembled, and managed by a Spring IoC container.

`ApplicationContext` is preferred over `BeanFactory`.

## Container Overview

The `org.springframework.context.ApplicationContext` interface represents the Spring IoC container and is responsible for instantiating, configuring, and assembling the beans.

The container gets its instructions on what objects to instantiate, configure, and assemble by reading **configuration metadata**. The configuration metadata is represented in **XML, Java annotations, or Java code**.

Example of XML based initialization and usage.

```java
// create and configure beans
ApplicationContext context = new ClassPathXmlApplicationContext("services.xml", "daos.xml");

// retrieve configured instance
PetStoreService service = context.getBean("petStore", PetStoreService.class);

// use configured instance
List<String> userList = service.getUsernameList();
```

`getBean()` methods second argument is for type safety. The methods checks if the returned bean matched the type given as the second argument.
It throws `BeanNotOfRequiredTypeException`.

# Annotations

## @Component Scan

To autodetect these classes and register the corresponding beans, you need to add `@ComponentScan` to your `@Configuration` class, where the basePackages attribute is a common parent package for the two classes.

```java
@Configuration
@ComponentScan(basePackages = "org.example")
public class AppConfig  {
    // ...
}
```

For SpringBoot the `@SpringBootApplication` annotation automatically scans the package in which it is present and all it's sub-packages.

## @Component

`@Component` is a class level annotation. During the component scan, Spring Framework automatically detects classes annotated with `@Component`
Spring will:
- Scan our application for classes annotated with _@Component_
- Instantiate them and inject any specified dependencies into them
- Inject them wherever needed

## @Repository

DAO or Repository classes usually represent the database access layer in an application, and should be annotated with `@Repository`.

When the annotations is used **automatic persistance exception translation** is enabled. When using a persistence framework, such as Hibernate, native exceptions thrown within classes annotated with `@Repository` will be automatically translated into subclasses of Spring's `DataAccessExeption`.

## @Service

The **business logic**(business facade) of an application usually resides within the service layer, so we’ll use the `@Service` annotation to indicate that a class belongs to that layer.

## @Controller

`@Controller` is a class level annotation, which tells the Spring Framework that this class serves as a controller in Spring MVC

## @Configuration

Configuration classes can contain bean definition methods annotated with @Bean

```java
@Configuration
class VehicleFactoryConfig {

    @Bean
    Engine engine() {
        return new Engine();
    }

}
```

The @Configuration annotation indicates that the class is a source of bean definitions.

# Beans
Beans are created using the configuration metadata provided to the Container. 

These bean definitions are called `BeanDefinition` objects.

| Property                 | Explained in…​           |
|--------------------------|--------------------------|
| Class                    | Instantiating Beans      |
| Name                     | Naming Beans             |
| Scope                    | Bean Scopes              |
| Constructor arguments    | Dependency Injection     |
| Properties               | Dependency Injection     |
| Autowiring mode          | Autowiring Collaborators |
| Lazy initialization mode | Lazy-initialized Beans   |
| Initialization method    | Initialization Callbacks |
| Destruction method       | Destruction Callbacks    |

The `@Bean` annotation supports specifying arbitrary initialization and destruction callback methods, much like Spring XML’s `init-method` and `destroy-method` attributes on the `bean` element, as the following example shows:
```java
public class BeanOne {

    public void init() {
        // initialization logic
    }
}

public class BeanTwo {

    public void cleanup() {
        // destruction logic
    }
}

@Configuration
public class AppConfig {

    @Bean(initMethod = "init")
    public BeanOne beanOne() {
        return new BeanOne();
    }

    @Bean(destroyMethod = "cleanup")
    public BeanTwo beanTwo() {
        return new BeanTwo();
    }
}
```

## Bean Lifecycle
![Bean LifeCycle](https://miro.medium.com/max/1400/1*hYEB_jkLc0yb6BB9D-LS1g.png)

![Bean lifecycle callback methods](https://howtodoinjava.com/wp-content/uploads/Spring-bean-life-cycle.png)

## BeanPostProcessor
`BeanPostProcessor` defines callback methods to provide own instantiation logic, dependency resolution etc. 
It has two methods:
1. `postProcessAfterInitialization(Object bean, String beanName)`
2. `postProcessBeforeInitialization(Object bean, String beanName)`

You can configure multiple `BeanPostProcessor` instances, and you can control the order in which these `BeanPostProcessor` instances run by setting the `order` property.

```java
package scripting;

import org.springframework.beans.factory.config.BeanPostProcessor;

public class InstantiationTracingBeanPostProcessor implements BeanPostProcessor {

    // simply return the instantiated bean as-is
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean; // we could potentially return any object reference here...
    }

    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("Bean '" + beanName + "' created : " + bean.toString());
        return bean;
    }
}
```

## BeanFactoryPostProcessor
The `BeanFactoryPostProcessor` is similar to `BeanPostProcessor`  with one major difference:
The `BeanFactoryPostProccessor` operates on the configuration metadata. **That is, the Spring IoC container lets a `BeanFactoryPostProcessor` read the configuration metadata and potentially change it _before_ the container instantiates any beans other than `BeanFactoryPostProcessor` instances.**
 
# Bean Scopes

| Scope | Description |
| --- | --- |
| singleton | (Default) Scopes a single bean definition to a single object instance for each Spring IoC container. |
| prototype | Scopes a single bean definition to any number of object instances. A bean with the prototype scope will return a different instance every time it is requested from the container. |
| request | scopes a single bean definition to the lifecycle of a single HTTP request. That is, each HTTP request has its own instance of a bean created off the back of a single bean definition. Only valid in the context of a web-aware Spring ApplicationContext. |
| session | Scopes a single bean definition to the lifecycle of an HTTP Session. Only valid in the context of a web-aware Spring ApplicationContext. |
| application | Scopes a single bean definition to the lifecycle of a ServletContext. Only valid in the context of a web-aware Spring ApplicationContext. |
| websocket | Scopes a single bean definition to the lifecycle of a WebSocket. Only valid in the context of a web-aware Spring ApplicationContext. |

The Gang of Four Singleton is different from Spring Singleton. In GoF singleton, the instance is created once per ClassLoader. The scope of the Spring singleton is best described as being per-container and per-bean.

Singleton is not thread safe.

E.g: Singleton
```java
@Bean
@Scope("singleton")
public Person personSingleton() {
    return new Person();
}
```
or
```java
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
```

E.g: Request Scope
```java
@Bean
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public HelloMessageGenerator requestScopedBean() {
    return new HelloMessageGenerator();
}
```
The proxyMode attribute is necessary because at the moment of the instantiation of the web application context, there is no active request. Spring creates a proxy to be injected as a dependency, and instantiates the target bean when it is needed in a request.

We can also use a @RequestScope composed annotation that acts as a shortcut for the above definition:
```java
@Bean
@RequestScope
public HelloMessageGenerator requestScopedBean() {
    return new HelloMessageGenerator();
}
```

# Dependency Injection

DI exists in two major variants:
- Constructor injection
- Setter injection

## Circular dependencies

If you use predominantly constructor injection, it is possible to create an unresolvable circular dependency scenario.

For example: Class A requires an instance of class B through constructor injection, and class B requires an instance of class A through constructor injection. 

==If you configure beans for classes A and B to be injected into each other, the Spring IoC container detects this circular reference at runtime, and throws a **`BeanCurrentlyInCreationException`**.==

One possible solution is to edit the source code of **some classes to be configured by setters** rather than constructors. Alternatively, avoid constructor injection and use setter injection only. In other words, although it is not recommended, you can configure circular dependencies with setter injection.

## Autowiring

Advantages:
- Autowiring can significantly reduce the need to specify properties or constructor arguments. (A Child bean can inherit configuration data of the parent bean [Ref]( https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-child-bean-definitions) )
- Autowiring can update a configuration as your objects evolve. For example, if you need to add a dependency to a class, that dependency can be satisfied automatically without you needing to modify the configuration.

There are four autowiring modes:

| Mode | Explanation|
| -- | -- |
| no (Can be ignored I guess 🤷) | (Default) No autowiring. Bean references must be defined by ref elements. |
| byName | Autowiring by property name. Spring looks for a bean with the same name as the property that needs to be autowired. For example, if a bean definition is set to autowire by name and it contains a master property (that is, it has a setMaster(..) method), Spring looks for a bean definition named master and uses it to set the property. |
| byType | Lets a property be autowired if exactly one bean of the property type exists in the container. If more than one exists, a fatal exception is thrown, which indicates that you may not use byType autowiring for that bean. If there are no matching beans, nothing happens (the property is not set). If more than one bean of the same type is available in the container, the framework will throw `NoUniqueBeanDefinitionException` |
| constructor | Analogous to byType but applies to constructor arguments. If there is not exactly one bean of the constructor argument type in the container, a fatal error is raised. |

**If more than one bean of the same type is available in the container, the framework will throw NoUniqueBeanDefinitionException**, indicating that more than one bean is available for autowiring.

### Fine-tuning Annotation-based Autowiring with @Primary

Because autowiring by type may lead to multiple candidates, it is often necessary to have more control over the selection process. One way to accomplish this is with Spring’s @Primary annotation. @Primary indicates that a particular bean should be given preference when multiple beans are candidates to be autowired to a single-valued dependency. If exactly one primary bean exists among the candidates, it becomes the autowired value.

```java
@Configuration
public class MovieConfiguration {

    @Bean
    @Primary
    public MovieCatalog firstMovieCatalog() { ... }

    @Bean
    public MovieCatalog secondMovieCatalog() { ... }

    // ...
}
```

```java
public class MovieRecommender {

    @Autowired
    private MovieCatalog movieCatalog;

    // ...
}
```

### Fine-tuning Annotation-based Autowiring with Qualifiers

`@Primary` is an effective way to use autowiring by type with several instances when one primary candidate can be determined. When you need more control over the selection process, you can use Spring’s `@Qualifier `annotation. You can associate qualifier values with specific arguments, narrowing the set of type matches so that a specific bean is chosen for each argument.

**It's worth noting that if both the `@Qualifier` and `@Primary` annotations are present, then the @Qualifier annotation will have precedence.** Basically, `@Primary` defines a **default**, while `@Qualifier` is **very specific**.

```java
public class MovieRecommender {

    @Autowired
    @Qualifier("main")
    private MovieCatalog movieCatalog;

    // ...
}
```

```java
public class MovieRecommender {

    private MovieCatalog movieCatalog;

    private CustomerPreferenceDao customerPreferenceDao;

    @Autowired
    public void prepare(@Qualifier("main") MovieCatalog movieCatalog,
            CustomerPreferenceDao customerPreferenceDao) {
        this.movieCatalog = movieCatalog;
        this.customerPreferenceDao = customerPreferenceDao;
    }

    // ...
}
```

> If there is no other resolution indicator (such as a qualifier or a primary marker), for a non-unique dependency situation, Spring matches the injection point name (that is, the field name or parameter name) against the target bean names and chooses the same-named candidate, if any.

## Method Injection
Suppose singleton bean **A needs to use non-singleton (prototype) bean B**, perhaps on each method invocation on A. The container creates the singleton bean A only once, and thus only gets one opportunity to set the properties. The container cannot provide bean A with a new instance of bean B every time one is needed.

A solution is to forego some inversion of control. You can [make bean A aware of the container](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-factory-aware) by implementing the `ApplicationContextAware` interface, and by [making a `getBean("B")` call to the container](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#beans-factory-client) ask for (a typically new) bean B instance every time bean A needs it.

The injection through getter method is called Method Injection.

### Lookup Method Injection

>Lookup method injection is the ability of the container to override methods on container-managed beans and return the lookup result for another named bean in the container. The lookup typically involves a prototype bean.

The Spring Framework implements this method injection by using bytecode generation from the CGLIB library to dynamically generate a subclass that overrides the method.

**_@Lookup_ is useful for:**
-   Injecting a prototype-scoped bean into a singleton bean (similar to _Provider_)
-   Injecting dependencies procedurally

```java
@Component
@Scope("prototype")
public class SchoolNotification {
    // ... prototype-scoped state
}
```

```java
@Component
public class StudentServices {

    // ... member variables, etc.

    @Lookup
    public SchoolNotification getNotification() {
        return null;
    }

    // ... getters and setters
}
```


**Using Abstract methods**

```java
public abstract class CommandManager {

    public Object process(Object commandState) {
        Command command = createCommand();
        command.setState(commandState);
        return command.execute();
    }

    @Lookup
    protected abstract Command createCommand(); // <-----
}
```

### Limitations
-   **Concrete methods are also necessary for component scanning,** which requires concrete classes to pick up in order for them to be compatible with Spring’s component scanning rules where **abstract classes get ignored by default**. This limitation does not apply to explicitly registered or explicitly imported bean classes.
-   A further key limitation is that **lookup methods do not work with** factory methods and in particular not with **`@Bean` methods** in configuration classes, since, in that case, the container is not in charge of creating the instance and therefore cannot create a runtime-generated subclass on the fly.

# Context Dependency Injection(CDI) - JSR 330

CDI (Contexts and Dependency Injection) is a standard dependency injection framework included in Java EE 6 and higher.

It allows us to manage the lifecycle of stateful components via domain-specific lifecycle contexts and inject components (services) into client objects in a type-safe way.

| Spring | javax.inject.* | javax.inject restrictions / comments |
| -- | -- | -- |
| @Autowired | @Inject | @Inject has no 'required' attribute. Can be used with Java 8’s Optional instead. |
| @Component | @Named / @ManagedBean | JSR-330 does not provide a composable model, only a way to identify named components. |
| @Scope("singleton") | @Singleton |The JSR-330 default scope is like Spring’s prototype. However, in order to keep it consistent with Spring’s general defaults, a JSR-330 bean declared in the Spring container is a singleton by default. In order to use a scope other than singleton, you should use Spring’s @Scope annotation. javax.inject also provides a @Scope annotation. Nevertheless, this one is only intended to be used for creating your own annotations. |
| @Qualifier | @Qualifier / @Named | javax.inject.Qualifier is just a meta-annotation for building custom qualifiers. Concrete String qualifiers (like Spring’s @Qualifier with a value) can be associated through javax.inject.Named. |
| @Value | - | no equivalent |
| @Required | - | no equivalent |
| @Lazy | - | no equivalent |
| ObjectFactory | Provider | javax.inject.Provider is a direct alternative to Spring’s ObjectFactory, only with a shorter get() method name. It can also be used in combination with Spring’s @Autowired or with non-annotated constructors and setter methods. |
| @Primary | @Default | - |
| - | @Alternative | Like @Qulaifier in spring but does not take qualifier names. |