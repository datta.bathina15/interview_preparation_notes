# Views
Views in SQL are kind of virtual tables. A view also has rows and columns as they are in a real table in the database. We can create a view by selecting fields from one or more tables present in the database. A View can either have all the rows of a table or specific rows based on certain condition.

**Syntax**:
```sql
CREATE VIEW view_name AS
SELECT column1, column2.....
FROM table_name
WHERE condition;
```

**Creating View from a single table:**
```sql
CREATE VIEW DetailsView AS
SELECT NAME, ADDRESS
FROM StudentDetails
WHERE S_ID < 5;
```

## Updating Views
There are certain conditions needed to be satisfied to update a view. If any one of these conditions is **not** met, then we will not be allowed to update the view.

1.  The SELECT statement which is used to create the view **should not include GROUP BY clause or ORDER BY clause**.
2.  The SELECT statement **should not have the DISTINCT** keyword.
3.  The View **should have all NOT NULL values**.
4.  The view **should not be created using nested queries or complex queries**.
5.  The view **should be created from a single table**. If the view is created using multiple tables then we will not be allowed to update the view.

### CREATE OR REPLACE VIEW
```sql
CREATE OR REPLACE VIEW view_name AS
SELECT column1,coulmn2,..
FROM table_name
WHERE condition;
```
