# Dependency injection in Angular
Dependency injection, or DI, is a design pattern in which a class requests dependencies from external sources rather than creating them.

Angular's DI framework provides dependencies to a class upon instantiation. Use Angular DI to increase flexibility and modularity in your applications.

# Creating an injectable service
To Create a Service:
```bash
ng generate service heroes/hero
```

This command creates the following default `HeroService`.

src/app/heroes/hero.service.ts (CLI-generated)
```ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class HeroService {
  constructor() { }
}
```

The `@Injectable()` decorator specifies that Angular can use this class in the DI system. 

The metadata, `providedIn: 'root'`, means that the `HeroService` is visible throughout the application.

# Injecting services
To inject a dependency in a component's `constructor()`, supply a constructor argument with the dependency type. The following example specifies the `HeroService` in the `HeroListComponent` constructor. The type of `heroService` is `HeroService`.

src/app/heroes/hero-list.component (constructor signature)
```ts
constructor(heroService: HeroService)
```

> [!INFO]
> An angular short hand for injecting the service and loading it into a class field would be by specifying an access specifier.
> E.g: **constructor(==*private*== heroService: HeroService)**

## Using services in other services
When a service depends on another service, follow the same pattern as injecting into a component.

The service **that will be injected with another service needs to be decorated with @Injectable()**. 

Example:
src/app/heroes/hero.service
```ts
import { Injectable } from '@angular/core';
import { HEROES } from './mock-heroes';
import { Logger } from '../logger.service';

@Injectable({
  providedIn: 'root',
})
export class HeroService {

  constructor(private logger: Logger) {  }

  getHeroes() {
    this.logger.log('Getting heroes ...');
    return HEROES;
  }
}
```

src/app/logger.service
```ts
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class Logger {
  logs: string[] = []; // capture logs for testing

  log(message: string) {
    this.logs.push(message);
    console.log(message);
  }
}
```

# Dependency providers

> The "providers" arrrays tells angular how to load the dependency.

By configuring providers, you can make services available to the parts of your application that need them.

The following example is the class provider syntax for providing a `Logger` class in the `providers` array.

```ts
providers: [Logger]
```

Angular expands the `providers` value into a full provider object as follows.
```ts
[{ provide: Logger, useClass: Logger }]
```

The expanded provider configuration is an object literal with two properties:

- The `provide` property holds the [token](https://angular.io/guide/dependency-injection-providers#token) that serves as the key for both locating a dependency value and configuring the injector. 
- **The second property is a provider definition object, which tells the injector how to create the dependency value.** The provider-definition key can be `useClass`, as in the example. It can also be `useExisting`, `useValue`, or `useFactory`. Each of these keys provides a different type of dependency, as discussed in the following section.

## Specifying an alternative class provider (useClass)
```ts
[{ provide: Logger, useClass: EvenBetterLogger }]
```

```ts
@Injectable()
export class EvenBetterLogger extends Logger {
  constructor(private userService: UserService) { super(); }

  override log(message: string) {
    const name = this.userService.user.name;
    super.log(`Message to ${name}: ${message}`);
  }
}
```

## Aliasing class providers (useExisting)
```ts
[ NewLogger,
  // Alias OldLogger w/ reference to NewLogger
  { provide: OldLogger, useExisting: NewLogger}]
```

In the following example, the injector injects the singleton instance of `NewLogger` when the component asks for either the new or the old logger. **In this way, `OldLogger` is an alias for `NewLogger`.**

## Injecting an object (useObject)
To inject an object, configure the injector with the `useValue` option.
```ts
[{ provide: Logger, useValue: SilentLogger }]
```

```ts
// An object in the shape of the logger service
function silentLoggerFn() {}

export const SilentLogger = {
  logs: ['Silent logger says "Shhhhh!". Provided via "useValue"'],
  log: silentLoggerFn
};
```

## Injecting a configuration object (useValue)
```ts
export const HERO_DI_CONFIG: AppConfig = {
  apiEndpoint: 'api.heroes.com',
  title: 'Dependency Injection'
};
```

```ts
providers: [
  UserService,
  { provide: APP_CONFIG, useValue: HERO_DI_CONFIG }
],
```

## Using factory providers (useFactory)
src/app/heroes/hero.service.ts (excerpt)
```ts
constructor(
  private logger: Logger,
  private isAuthorized: boolean) { }

getHeroes() {
  const auth = this.isAuthorized ? 'authorized ' : 'unauthorized';
  this.logger.log(`Getting heroes for ${auth} user.`);
  return HEROES.filter(hero => this.isAuthorized || !hero.isSecret);
}
```

src/app/heroes/hero.service.provider.ts (excerpt)
```ts
const heroServiceFactory = (logger: Logger, userService: UserService) =>
  new HeroService(logger, userService.user.isAuthorized);
```

src/app/heroes/hero.service.provider.ts (excerpt)
```ts
export const heroServiceProvider =
  { provide: HeroService,
    useFactory: heroServiceFactory,
    deps: [Logger, UserService]
  };
```

- The `useFactory` field specifies that the provider is a factory function whose implementation is `heroServiceFactory`
- The `deps` property is an array of [provider tokens](https://angular.io/guide/dependency-injection-providers#token). The `Logger` and `UserService` classes serve as tokens for their own class providers. The injector resolves these tokens and injects the corresponding services into the matching `heroServiceFactory` factory function parameters.

# Hierarchial injectors
In Angular, DI is hirerarchial. If the dependency is declared in Module level, then it is available to all the components under it. If it's declared on app component level, then it is available to all the children of app-component. If the dependency is declared on a component without any children, then it's available only to that component.  It does not propogate upwards to component's parents.

## Two injector hierarchies

| Injector hierarchies      | Details                                                                                                                                                           |
|---------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| ModuleInjector hierarchy  | Configure a ModuleInjector in this hierarchy using an @NgModule() or @Injectable() annotation.                                                                    |
| ElementInjector hierarchy | Created implicitly at each DOM element. An ElementInjector is empty by default unless you configure it in the providers property on @Directive() or @Component(). |

### `ModuleInjector`

The `ModuleInjector` can be configured in one of two ways:
-   Using the `@Injectable()` `providedIn` property to refer to `@NgModule()`, or `root`
-   Using the `@NgModule()` `providers` array

> [!INFO]
> Using the `@Injectable()` `providedIn` property is preferable to the `@NgModule()` `providers` array because with `@Injectable()` `providedIn`, optimization tools can perform tree-shaking, which removes services that your application isn't using and results in smaller bundle sizes.

### `ElementInjector`
Angular creates `ElementInjector`s implicitly for each DOM element.
Providing a service in the `@Component()` decorator using its `providers` or `viewProviders` property configures an `ElementInjector`.

```ts
@Component({
  …
  providers: [{ provide: ItemService, useValue: { name: 'lamp' } }]
})
export class TestComponent
```

> [!INFO]
> If both module has a service configured and the same service is configured in the `providers` array of the `@Component`, a new service is created & supplied to the component instead of sharing the instance provided by the module.

## Resolution rules
When resolving a token for a component/directive, Angular resolves it in two phases:

1.  Against the `ElementInjector` hierarchy (its parents).
2.  Against the `ModuleInjector` hierarchy (its parents).

When a component declares a dependency, Angular tries to satisfy that dependency with its own `ElementInjector`. 

If the component's injector lacks the provider, it passes the request up to its parent component's `ElementInjector`.

The requests keep forwarding up until Angular finds an injector that can handle the request or runs out of ancestor `ElementInjector`s.

If Angular doesn't find the provider in any `ElementInjector`s, it goes back to the element where the request originated and looks in the `ModuleInjector` hierarchy. 

If Angular still doesn't find the provider, it throws an error.