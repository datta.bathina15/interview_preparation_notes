# Processes

Process has a self-contained execution environments. They have complete and private set of runtime resources i.e. each process has it's own memory space. 

> To facilitate communication between processes, most operating systems support Inter Process Communication (IPC) resources, such as pipes and sockets. IPC is used not just for communication between processes on the same system, but processes on different systems.

Most implementations of the Java Virtual machine runs as a single process.
 
A Java application can create processes using the [ProcessBuilder](https://docs.oracle.com/javase/8/docs/api/java/lang/ProcessBuilder.html) object.

# Thread

A thread is sometime's called as *light weight process*. Both processes and threads provide an execution environment. 

> Threads exist within a process — every process has at least one. Threads share the process's resources, including memory and open files. This makes for efficient, but potentially problematic, communication.

Every Java application has at-least one thread called **main** thread. The main thread has the ability to create other threads.
