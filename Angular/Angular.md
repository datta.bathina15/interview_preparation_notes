# Angluar CLI

You use the Angular CLI to create projects, generate application and library code, and perform a variety of ongoing development tasks such as testing, bundling, and deployment.

```
npm install -g @angular/cli
```

## To create a new project 

```
ng new my-app
```

## Run the application

1. Navigate to my-app folder
2. Run the following command
	```
	ng serve --open
	```

The --open (or just -o) option automatically opens your browser to **http://localhost:4200/**.

## Workspace and project configuration

A single workspace configuration file, ==angular.json==, is created at the top level of the workspace. This is where you can set per-project defaults for CLI command options, and specify configurations to use when the CLI builds a project for different targets.

## CLI command-language syntax

```
ng commandNameOrAlias requiredArg [optionalArg] [options]
```

| Command |	Details |
| -- | -- |
| ng build | Compiles an Angular app into an output directory. |
| ng serve | Builds and serves your application, rebuilding on file changes. |
| ng generate |	Generates or modifies files based on a schematic. |
| ng test |	Runs unit tests on a given project. |
| ng e2e |	Builds and serves an Angular application, then runs end-to-end tests. |
