# Reactive forms vs Template-Driven forms

| 					  | Reactive            | Template-driven       |               |
|---------------------|---------------------|--------------------------------------| 
| Setup of form model | Explicit, created in component class | Implicit, created by directives |
| Data model          | Structured and immutable             | Unstructured and mutable        |
| Data flow           | Synchronous                          | Asynchronous                    |
| Form validation     | Functions                            | Directives                      |
| Testablility        | Easy to setup and test               | Requires set up to test the forms |


Reactive forms can be considered when the application if form driven or we are already using reactive patterns for building the application. They are scalable, easy to test and reusable. They provide direct access to the underlying forms API and use synchronous data flow.

Template-driven forms **rely on directive** to create and manipulte data. They are prefferen in simple scenarios like form for subscribing to news letter. They abstract away from the underlying forms API and use Asynchronous data flow. 

# Setting up the form model
The two forms keep track of the values changes. The two approaches share the underlying building blocks but differ in how these are created and manage the form-control instances.

## Common form foundation classes

| Base classes         | Details                                                                           |
|----------------------|-----------------------------------------------------------------------------------|
| FormControl          | Tracks the value and validation status of an individual form control.             |
| FormGroup            | Tracks the same values and status for a collection of form controls.              |
| FormArray            | Tracks the same values and status for an array of form controls.                  |
| ControlValueAccessor | Creates a bridge between Angular FormControl instances and built-in DOM elements. |

| Module | Exports |
| ----- | ----- |
|  FormsModule  |  Exports the required providers and directives for template-driven forms, making them available for import by NgModules that import this module.  |
|  ReactiveFormsModule  |  Exports the required infrastructure and directives for reactive forms, making them available for import by NgModules that import this module.  |



# Data flow in reactive forms
## View to model
1. User inputs the data on the field in view.
2. The form input emits an "input" event with the latest value.
3. The control value accessor listening for the events relays the new value to the `FormControl` instance.
4. The `FormControl` instance emits the new value to the `valueChanges` observable.
5. Any subscribers to the `valueChanges` observable recieve the new value.

## Model to View
1. The user calls the `favoriteColorControl.setValue()` which update the `FormControl` value.
2. The `FormControl` emits the new value through the `valueChanges` observable.
3. Any subscribers to the `valueChanges` observable recieve the new value.
4. The control value accessor on the form element updates the element with new value. 

# Data flow in template-driven forms
## View to model
1.  The user types _Blue_ into the input element.
2.  The input element emits an "input" event with the value _Blue_.
3.  The control value accessor attached to the input triggers the `setValue()` method on the `FormControl` instance.
4.  The `FormControl` instance emits the new value through the `valueChanges` observable.
5.  Any subscribers to the `valueChanges` observable receive the new value.
6.  The control value accessor also calls the `NgModel.viewToModelUpdate()` method which emits an `ngModelChange` event.
7.  Because the component template uses two-way data binding for the `favoriteColor` property, the `favoriteColor` property in the component is updated to the value emitted by the `ngModelChange` event (_Blue_).

## Model to view
1.  The `favoriteColor` value is updated in the component.
2.  Change detection begins.
3.  During change detection, the `ngOnChanges` lifecycle hook is called on the `NgModel` directive instance because the value of one of its inputs has changed.
4.  The `ngOnChanges()` method queues an async task to set the value for the internal `FormControl` instance.
5.  Change detection completes.
6.  On the next tick, the task to set the `FormControl` instance value is executed.
7.  The `FormControl` instance emits the latest value through the `valueChanges` observable.
8.  Any subscribers to the `valueChanges` observable receive the new value.
9.  The control value accessor updates the form input element in the view with the latest `favoriteColor` value.

# Reactive forms
There are three steps to using form controls.
1.  Register the reactive forms module in your application. This module declares the reactive-form directives that you need to use reactive forms.
2.  Generate a new `FormControl` instance and save it in the component.
3.  Register the `FormControl` in the template.

**Register the reactive forms module:**

src/app/app.module.ts (excerpt)
```ts
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    // other imports ...
    ReactiveFormsModule
  ],
})
export class AppModule { }
```

**Generate a new `FormControl`  :**

src/app/name-editor/name-editor.component.ts
```ts
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-name-editor',
  templateUrl: './name-editor.component.html',
  styleUrls: ['./name-editor.component.css']
})
export class NameEditorComponent {
  name = new FormControl('');
}
```

**Register the control in the template:**

src/app/name-editor/name-editor.component.html
```html
<label for="name">Name: </label>
<input id="name" type="text" [formControl]="name">

<p>Value: {{ name.value }}</p>
```

## FormGroup
**Create a `FormGroup` instance.**
src/app/profile-editor/profile-editor.component.ts
```ts
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})
export class ProfileEditorComponent {
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
  });
}
```

**Associate the `FormGroup` model and view**
src/app/profile-editor/profile-editor.component.html
```html
<form [formGroup]="profileForm">

  <label for="first-name">First Name: </label>
  <input id="first-name" type="text" formControlName="firstName">

  <label for="last-name">Last Name: </label>
  <input id="last-name" type="text" formControlName="lastName">
  <p>Complete the form to enable button.</p>
  <button type="submit" [disabled]="!profileForm.valid">Submit</button>
</form>
```

The `formControlName` input provided by the `FormControlName` directive binds each individual input to the form control defined in `FormGroup`. The form controls communicate with their respective elements. They also communicate changes to the form group instance, which provides the source of truth for the model value.

>[!INFO]
>Notice the button element in the snippet. It is disable by property binding the `valid` property of the `FormGroup`

**Save form data**

The `FormGroup` directive listens for the `submit` event emitted by the `form` element and emits an `ngSubmit` event that you can bind to a callback function. Add an `ngSubmit` event listener to the `form` tag with the `onSubmit()` callback method.

src/app/profile-editor/profile-editor.component.html
```html
<form [formGroup]="profileForm" (ngSubmit)="onSubmit()">
```

Use `EventEmitter` to keep the form encapsulated and to provide the form value outside the component. The following example uses `console.warn` to log a message to the browser console.

src/app/profile-editor/profile-editor.component.ts
```ts
onSubmit() {
  // TODO: Use EventEmitter with form value
  console.warn(this.profileForm.value);
}
```

### Creating nested form groups
src/app/profile-editor/profile-editor.component.ts
```ts
import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})
export class ProfileEditorComponent {
  profileForm = new FormGroup({
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    address: new FormGroup({
      street: new FormControl(''),
      city: new FormControl(''),
      state: new FormControl(''),
      zip: new FormControl('')
    })
  });
}
```

src/app/profile-editor/profile-editor.component.html
```html
<div formGroupName="address">
  <h2>Address</h2>

  <label for="street">Street: </label>
  <input id="street" type="text" formControlName="street">

  <label for="city">City: </label>
  <input id="city" type="text" formControlName="city">

  <label for="state">State: </label>
  <input id="state" type="text" formControlName="state">

  <label for="zip">Zip Code: </label>
  <input id="zip" type="text" formControlName="zip">
</div>
```

### Updating parts of the data model
There are two ways to update the model value:

| Methods      | Details                                                                                                                                                             |
|--------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| setValue()   | Set a new value for an individual control. The setValue() method strictly adheres to the structure of the form group and replaces the entire value for the control. |
| patchValue() | Replace any properties defined in the object that have changed in the form model.                                                                                   |

#### setValue()

Sets the value of the `FormGroup`. It accepts an object that matches the structure of the group, with control names as keys.

**Throws `Error` When strict checks fail**, such as setting the value of a control that doesn't exist or if you exclude a value of a control that does exist.

```ts
const form = new FormGroup({
  first: new FormControl(),
  last: new FormControl()
});

console.log(form.value);   // {first: null, last: null}

form.setValue({first: 'Nancy', last: 'Drew'});
console.log(form.value);   // {first: 'Nancy', last: 'Drew'}
```

#### patchValue()

It accepts an object with control names as keys, and does its best to match the values to the correct controls in the group. 

It does not throw error. 

```ts
const form = new FormGroup({
   first: new FormControl(),
   last: new FormControl()
});
console.log(form.value);   // {first: null, last: null}

form.patchValue({first: 'Nancy'});
console.log(form.value);   // {first: 'Nancy', last: null}
```

## FormBuilder Service
`FormBuilder` service provides convinience methods to generate form controls. 

**Import the FormBuilder class**

src/app/profile-editor/profile-editor.component.ts
```ts
import { FormBuilder } from '@angular/forms';
```

**Inject the FormBuilder service**

src/app/profile-editor/profile-editor.component.ts (constructor)
```ts
constructor(private fb: FormBuilder) { }
```

**Generate form controls**

src/app/profile-editor/profile-editor.component.ts (form builder)
```ts
import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})
export class ProfileEditorComponent {
  profileForm = this.fb.group({
    firstName: [''],
    lastName: [''],
    address: this.fb.group({
      street: [''],
      city: [''],
      state: [''],
      zip: ['']
    }),
  });

  constructor(private fb: FormBuilder) { }
}
```

## FormArray
As with form group instances, you can dynamically insert and remove controls from form array instances, and the form array instance value and validation status is calculated from its child controls. However, you don't need to define a key for each control by name, so this is a great option if you don't know the number of child values in advance.

**Import the [FormArray](https://angular.io/api/forms/FormArray) class**

src/app/profile-editor/profile-editor.component.ts (import)
```ts
import { FormArray } from '@angular/forms';
```

**Define a `FormArray` control**

src/app/profile-editor/profile-editor.component.ts (aliases form array)
```ts
profileForm = this.fb.group({
  firstName: ['', Validators.required],
  lastName: [''],
  address: this.fb.group({
    street: [''],
    city: [''],
    state: [''],
    zip: ['']
  }),
  aliases: this.fb.array([  // <-----
    this.fb.control('')
  ])
});
```

**Access the `FormArray` control**

src/app/profile-editor/profile-editor.component.ts (aliases getter)
```ts
get aliases() {
  return this.profileForm.get('aliases') as FormArray;
}
```

>[!INFO]
>Because the returned control is of the type `AbstractControl`, you need to provide an explicit type to access the method syntax for the form array instance. 

Define a method to dynamically insert an alias control into the alias's form array. The `FormArray.push()` method inserts the control as a new item in the array. 

src/app/profile-editor/profile-editor.component.ts (add alias)
```ts
addAlias() {
  this.aliases.push(this.fb.control(''));
}
```

**Display the form array in the template**

To attach the aliases from your form model, you must add it to the template. Similar to the `formGroupName` input provided by `FormGroupNameDirective`, `formArrayName` binds communication from the form array instance to the template with `FormArrayNameDirective`. 

src/app/profile-editor/profile-editor.component.html (aliases form array template)
```html
<div formArrayName="aliases">
  <h2>Aliases</h2>
  <button type="button" (click)="addAlias()">+ Add another alias</button>

  <div *ngFor="let alias of aliases.controls; let i=index">
    <!-- The repeated alias template -->
    <label for="alias-{{ i }}">Alias:</label>
    <input id="alias-{{ i }}" type="text" [formControlName]="i">
  </div>
</div>
```

The `*ngFor` directive iterates over each form control instance provided by the aliases form array instance. Because form array elements are unnamed, you assign the index to the `i` variable and pass it to each control to bind it to the `formControlName` input. 

## Typed Forms

==As of Angular 14, reactive forms are strictly typed by default.==

When upgrading to Angular 14, an included migration will automatically replace all the forms classes in your code with corresponding untyped versions.
```ts
const login = new UntypedFormGroup({
    email: new UntypedFormControl(''),
    password: new UntypedFormControl(''),
});
```

Each `Untyped` symbol has exactly the same semantics as in previous Angular versions, so your application should continue to compile as before. By removing the `Untyped` prefixes, you can incrementally enable the types.

### Nullability
TypeScript will enforce that you always handle the possibility that the control has become `null`. If you want to make this control non-nullable, **you may use the `nonNullable` option**. This will cause the control to reset to its intial value, instead of `null`:

```ts
const email = new FormControl('angularrox@gmail.com', {nonNullable: true});
email.reset();
console.log(email.value); // angularrox@gmail.com
```

### Specifying an Explicit Type
It is possible to specify the type, instead of relying on inference. Consider a control that is initialized to `null`. Because the initial value is null, TypeScript will infer `FormControl<null>`, which is narrower than we want.

```ts
const email = new FormControl(null);
email.setValue('angularrox@gmail.com'); // Error!
```

To prevent this, we explicitly specify the type as `string|null`:
```ts
const email = new FormControl<string|null>(null);
email.setValue('angularrox@gmail.com');
```

### Optional Controls and Dynamic Groups
```ts
interface LoginForm {
    email: FormControl<string>;
    password?: FormControl<string>;
}

const login = new FormGroup<LoginForm>({
    email: new FormControl('', {nonNullable: true}),
    password: new FormControl('', {nonNullable: true}),
});

login.removeControl('password');
```

In this form, we explicitly specify the type, which allows us to make the `password` control optional. TypeScript will enforce that only optional controls can be added or removed.

### FormRecord
Tracks the value and validity state of a collection of `FormControl` instances, each of which has the same value type.
```ts
const addresses = new FormRecord<FormControl<string|null>>({});
addresses.addControl('Andrew', new FormControl('2340 Folsom St'));
```

Any control of type `string|null` can be added to this `FormRecord`.

### FormBuilder and NonNullableFormBuilder
This type is shorthand for specifying `{nonNullable: true}` on every control, and can eliminate significant boilerplate from large non-nullable forms.

```ts
const fb = new FormBuilder();
const login = fb.nonNullable.group({
    email: '',
    password: '',
});
```

# Validation
## Validating input in template-driven forms
To add validation to a template-driven form, you add the same validation attributes as you would with [native HTML form validation](https://developer.mozilla.org/docs/Web/Guide/HTML/HTML5/Constraint_validation). **Angular uses directives to match these attributes with validator functions in the framework.**

Every time the value of a form control changes, Angular runs validation and generates either a list of validation errors that results in an `INVALID` status, or null, which results in a VALID status.

You can then inspect the control's state by exporting `ngModel` to a local template variable. The following example exports `NgModel` into a variable called `name`:

template/hero-form-template.component.html (name)
```html
<input type="text" id="name" name="name" class="form-control"
      required minlength="4" appForbiddenName="bob"
      [(ngModel)]="hero.name" #name="ngModel">

<div *ngIf="name.invalid && (name.dirty || name.touched)"
    class="alert">

  <div *ngIf="name.errors?.['required']">
    Name is required.
  </div>
  <div *ngIf="name.errors?.['minlength']">
    Name must be at least 4 characters long.
  </div>
  <div *ngIf="name.errors?.['forbiddenName']">
    Name cannot be Bob.
  </div>

</div>
```

## Validating input in reactive forms
In a reactive form, **the source of truth is the component class**. Instead of adding validators through attributes in the template, you add validator functions directly to the form control model in the component class. **Angular then calls these functions whenever the value of the control changes.**

### Validator functions

| Validator type   | Details                                                                                                                                                                                                             |
|------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Sync validators  | Synchronous functions that take a control instance and immediately return either a set of validation errors or null. Pass these in as the second argument when you instantiate a FormControl.                       |
| Async validators | Asynchronous functions that take a control instance and return a Promise or Observable that later emits a set of validation errors or null. Pass these in as the third argument when you instantiate a FormControl. |

Angular only runs async validators if all sync validators pass.

### Built-in validator functions
The same built-in validators that are available as attributes in template-driven forms, such as `required` and [minlength](https://angular.io/api/forms/MinLengthValidator), are all available to use as functions from the [Validators](https://angular.io/api/forms/Validators) class. For a full list of built-in validators, see the [Validators](https://angular.io/api/forms/Validators) API reference.

reactive/hero-form-reactive.component.ts (validator functions)
```ts
ngOnInit(): void {
  this.heroForm = new FormGroup({
    name: new FormControl(this.hero.name, [
      Validators.required,
      Validators.minLength(4),
      forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
    ]),
    alterEgo: new FormControl(this.hero.alterEgo),
    power: new FormControl(this.hero.power, Validators.required)
  });

}

get name() { return this.heroForm.get('name'); }

get power() { return this.heroForm.get('power'); }
```

**All of these validators are synchronous, so they are passed as the second argument.** Notice that you can support multiple validators by passing the functions in as an array.

reactive/hero-form-reactive.component.html (name with error msg)
```html
<input type="text" id="name" class="form-control"
      formControlName="name" required>

<div *ngIf="name.invalid && (name.dirty || name.touched)"
    class="alert alert-danger">

  <div *ngIf="name.errors?.['required']">
    Name is required.
  </div>
  <div *ngIf="name.errors?.['minlength']">
    Name must be at least 4 characters long.
  </div>
  <div *ngIf="name.errors?.['forbiddenName']">
    Name cannot be Bob.
  </div>
</div>
```

## Defining custom validators
shared/forbidden-name.directive.ts (forbiddenNameValidator)
```ts
/** A hero's name can't match the given regular expression */
export function forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const forbidden = nameRe.test(control.value);
    return forbidden ? {forbiddenName: {value: control.value}} : null;
  };
}
```

The function is a factory that takes a regular expression to detect a _specific_ forbidden name and returns a validator function.

In this sample, the forbidden name is "bob", so the validator rejects any hero name containing "bob". Elsewhere it could reject "alice" or any name that the configuring regular expression matches.

The validation error object typically has a property whose name is the validation key, `'forbiddenName'`, and whose value is an arbitrary dictionary of values that you could insert into an error message, `{name}`.

### Adding custom validators to reactive forms

reactive/hero-form-reactive.component.ts (validator functions)
```ts
this.heroForm = new FormGroup({
  name: new FormControl(this.hero.name, [
    Validators.required,
    Validators.minLength(4),
    forbiddenNameValidator(/bob/i) // <-- Here's how you pass in the custom validator.
  ]),
  alterEgo: new FormControl(this.hero.alterEgo),
  power: new FormControl(this.hero.power, Validators.required)
});
```

### Adding custom validators to template-driven forms

We need to add the validator as a directive.
Register the directive with `NG_VALIDATORS` provider. [NG_VALIDATORS](https://angular.io/api/forms/NG_VALIDATORS) is a predefined provider with an extensible collection of validators.

The directive should also implement `Validator` interface.

```ts
@Directive({
  selector: '[appForbiddenName]',
  providers: [{provide: NG_VALIDATORS, useExisting: ForbiddenValidatorDirective, multi: true}]
})
export class ForbiddenValidatorDirective implements Validator {
  @Input('appForbiddenName') forbiddenName = '';

  validate(control: AbstractControl): ValidationErrors | null {
    return this.forbiddenName ? forbiddenNameValidator(new RegExp(this.forbiddenName, 'i'))(control)
                              : null;
  }
}
```

Once the `ForbiddenValidatorDirective` is ready, you can add its selector, `appForbiddenName`, to any input element to activate it. For example:

```html
<input type="text" id="name" name="name" class="form-control"
      required minlength="4" appForbiddenName="bob"
      [(ngModel)]="hero.name" #name="ngModel">
```

## Cross field validation
These are custom validators that verify multiple fields using the AbstractControl object passed into the method.

### For reactive forms
```ts
const heroForm = new FormGroup({
  'name': new FormControl(),
  'alterEgo': new FormControl(),
  'power': new FormControl()
}, { validators: identityRevealedValidator });
```

shared/identity-revealed.directive.ts
```ts
/** A hero's name can't match the hero's alter ego */
export const identityRevealedValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
  const name = control.get('name');
  const alterEgo = control.get('alterEgo');

  return name && alterEgo && name.value === alterEgo.value ? { identityRevealed: true } : null;
};
```

The `identity` validator implements the [ValidatorFn](https://angular.io/api/forms/ValidatorFn) interface. It takes an Angular control object as an argument and returns either null if the form is valid, or [ValidationErrors](https://angular.io/api/forms/ValidationErrors) otherwise.

We must use the `FormControl`'s `get` method to fetch various form controls required for validation.

```html
<div *ngIf="heroForm.errors?.['identityRevealed'] && (heroForm.touched || heroForm.dirty)" class="cross-validation-error-message alert alert-danger">
    Name cannot match alter ego.
</div>
```

### For template-driven forms
For template-driven forms create custom validators thrrough a directive with NG_PROVIDERS  token. 
The directive(class) should implement the `Validator` interface.

shared/identity-revealed.directive.ts
```ts
@Directive({
  selector: '[appIdentityRevealed]',
  providers: [{ provide: NG_VALIDATORS, useExisting: IdentityRevealedValidatorDirective, multi: true }]
})
export class IdentityRevealedValidatorDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return identityRevealedValidator(control);
  }
}
```

**You must add the new directive to the HTML template. *Because the validator must be registered at the highest level in the form*, the following template puts the directive on the `form` tag.**

template/hero-form-template.component.html.
```html
<form #heroForm="ngForm" appIdentityRevealed>
<div *ngIf="heroForm.errors?.['identityRevealed'] && (heroForm.touched || heroForm.dirty)" class="cross-validation-error-message alert">
    Name cannot match alter ego.
</div>
```

To provide better user experience, an appropriate error message appears when the form is invalid.

## Control status CSS classes
Angular automatically mirrors many control properties onto the form control element as CSS classes. Use these classes to style form control elements according to the state of the form. The following classes are currently supported.

-   `.ng-valid`
-   `.ng-invalid`
-   `.ng-pending`
-   `.ng-pristine`
-   `.ng-dirty`
-   `.ng-untouched`
-   `.ng-touched`
-   `.ng-submitted` (enclosing form element only)

forms.css (status classes)
```css
.ng-valid[required], .ng-valid.required  {
  border-left: 5px solid #42A948; /* green */
}

.ng-invalid:not(form)  {
  border-left: 5px solid #a94442; /* red */
}

.alert div {
  background-color: #fed3d3;
  color: #820000;
  padding: 1rem;
  margin-bottom: 1rem;
}

.form-group {
  margin-bottom: 1rem;
}

label {
  display: block;
  margin-bottom: .5rem;
}

select {
  width: 100%;
  padding: .5rem;
}
```

## Asynchronous Validators
Asynchronous validators implemenmt the `AsyncValidatorFn`  and `AsyncValidator` interfaces. These are very similar to their synchronous counterparts, with the following differences.
1. The validator must return a Promise or an Observable
2. **The observable must returned must be finite.** Meaning the observable must complete the action at some point. To convert an infinite observable to a finite observable we can pipe the observable through a filtering operation like `first`, `last`, `take`, `takeUntil`

**Async Validation occurs only after synchronous validation is successful.** This check lets forms avoid potentially expensive async validation processes (such as an HTTP request) if the more basic validation methods have already found invalid input.

After asynchronous validation begins, the form control enters a `pending` state. Inspect the control's `pending` property and use it to give visual feedback about the ongoing validation operation.

### Implementing a custom async validator
In the following example, an async validator ensures that heroes pick an alter ego that is not already taken. New heroes are constantly enlisting and old heroes are leaving the service, so the list of available alter egos cannot be retrieved ahead of time. To validate the potential alter ego entry, the validator must initiate an asynchronous operation to consult a central database of all currently enlisted heroes.

```ts
@Injectable({ providedIn: 'root' })
export class UniqueAlterEgoValidator implements AsyncValidator {
  constructor(private heroesService: HeroesService) {}

  validate(
    control: AbstractControl
  ): Observable<ValidationErrors | null> {
    return this.heroesService.isAlterEgoTaken(control.value).pipe(
      map(isTaken => (isTaken ? { uniqueAlterEgo: true } : null)),
      catchError(() => of(null))
    );
  }
}
```

The `validate()` method pipes the response through the `map` operator and transforms it into a validation result.

The method then, like any validator, returns `null` if the form is valid, and [ValidationErrors](https://angular.io/api/forms/ValidationErrors) if it is not. This validator handles any potential errors with the `catchError` operator. In this case, the validator treats the `isAlterEgoTaken()` error as a successful validation, because failure to make a validation request does not necessarily mean that the alter ego is invalid. You could handle the error differently and return the `ValidationError` object instead.

### Adding async validators to reactive forms
To use an async validator in reactive forms, begin by injecting the validator into the constructor of the component class.
```ts
constructor(private alterEgoValidator: UniqueAlterEgoValidator) {}
```

Then, pass the validator function directly to the `FormControl` to apply it.
```ts
const alterEgoControl = new FormControl('', {
  asyncValidators: [this.alterEgoValidator.validate.bind(this.alterEgoValidator)],
  updateOn: 'blur'
});
```

### Adding async validators to template-driven forms
To use an async validator in template-driven forms, create a new directive and register the `NG_ASYNC_VALIDATORS` provider on it.

```ts
@Directive({
  selector: '[appUniqueAlterEgo]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: forwardRef(() => UniqueAlterEgoValidatorDirective),
      multi: true
    }
  ]
})
export class UniqueAlterEgoValidatorDirective implements AsyncValidator {
  constructor(private validator: UniqueAlterEgoValidator) {}

  validate(
    control: AbstractControl
  ): Observable<ValidationErrors | null> {
    return this.validator.validate(control);
  }
}
```

template/hero-form-template.component.html (unique-alter-ego-input)
```html
<input type="text"
         id="alterEgo"
         name="alterEgo"
         #alterEgo="ngModel"
         [(ngModel)]="hero.alterEgo"
         [ngModelOptions]="{ updateOn: 'blur' }"
         appUniqueAlterEgo> /* <--- */
```

### Optimizing performance of async validators
Angular triggers the validation for every change in the input. This might lead to unneccessary calls to Backend. 

This can be prevented by making the async validator to trigger on events like `submit` or `blur`.

With template-driven forms, set the property in the template.
```html
<input [(ngModel)]="name" [ngModelOptions]="{updateOn: 'blur'}">
```

With reactive forms, set the property in the `FormControl` instance.
```ts
new FormControl('', {updateOn: 'blur'});
```
