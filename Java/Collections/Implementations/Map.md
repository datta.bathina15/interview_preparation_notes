# General-Purpose Map Implementations

## HashMap

> Hash table based implementation of the Map interface. This implementation provides all of the optional map operations, and **permits null values and the null key**. (The HashMap class is roughly equivalent to Hashtable, except that it is unsynchronized and permits nulls.) This class makes no guarantees as to the order of the map; in particular, it does not guarantee that the order will remain constant over time.

HashMap has two parameters

- **Initial Capacity** \- *Capacity is the number of buckets in the hash table*, and the initial capacity is simply the capacity at the time the hash table is created.
- **Load Factor** \- It's a measure of *how full a hashmap is allowed to get* before it's capacity can automatically be increased. Default load factor is **.75**.

### When to use

if you want maximum speed and don't care about iteration order, use HashMap

## TreeMap

> A ==Red-Black tree== based NavigableMap implementation. The map is sorted according to the natural ordering of its keys, or by a Comparator provided at map creation time, depending on which constructor is used.

> This implementation provides guaranteed log(n) time cost for the containsKey, get, put and remove operations. Algorithms are adaptations of those in Cormen, Leiserson, and Rivest's Introduction to Algorithms.

Must be *Consistent with Equals* (See interfaces/ordering/# Consistent with Equals)

### when to use
If you need SortedMap operations or key-ordered Collection-view iteration, use TreeMap

## LinkedHashMap

> Hash table and linked list implementation of the Map interface, with predictable iteration order. This implementation differs from HashMap in that it maintains a ==doubly-linked== list running through all of its entries.

Like `HashMap` it provides constant-time performance for the basic operations (add, contains and remove),

Iteration over the collection-views of a LinkedHashMap requires time proportional to the size of the map, regardless of its capacity. Iteration over a HashMap is likely to be more expensive, requiring time proportional to its capacity. 

* * *
### When to use

If you want near-HashMap performance and insertion-order iteration, use LinkedHashMap.
* * *

`LinkedHashMap` provides two capabilities that are not available with `LinkedHashSet`:
1. **You can order it based on key access rather than insertion**. In other words, merely looking up the value associated with a key brings that key to the end of the map.
2. **LinkedHashMap provides the `removeEldestEntry` method**, which may be overridden to impose a policy for removing stale mappings automatically when new mappings are added to the map. This makes it very easy to implement a custom cache.

# Special Purpose Implementations

## EnumMaps

> EnumMap, which is internally implemented as an array, is a high-performance Map implementation for use with enum keys. This implementation combines the richness and safety of the Map interface with a speed approaching that of an array. If you want to map an enum to a value, you should always use an EnumMap in preference to an array.

## WeakHashMap

> WeakHashMap is an implementation of the Map interface that stores only weak references to its keys. Storing only weak references allows a key-value pair to be garbage-collected when its key is no longer referenced outside of the WeakHashMap. This class provides the easiest way to harness the power of weak references. It is useful for implementing "registry-like" data structures, where the utility of an entry vanishes when its key is no longer reachable by any thread.

## IdentityHashMap

> This class implements the Map interface with a hash table, using reference-equality in place of object-equality when comparing keys (and values). 

In other words, in an IdentityHashMap, two keys k1 and k2 are considered equal if and only if (k1&#61;&#61;k2). (In normal Map implementations (like HashMap) two keys k1 and k2 are considered equal if and only if (k1&#61;&#61;null ? k2&#61;&#61;null : k1.equals(k2)).) 

> This class is useful for topology-preserving object graph transformations, such as serialization or deep-copying. To perform such transformations, you need to maintain an identity-based "node table" that keeps track of which objects have already been seen. Identity-based maps are also used to maintain object-to-meta-information mappings in dynamic debuggers and similar systems. Finally, identity-based maps are useful in thwarting "spoof attacks" that are a result of intentionally perverse equals methods because IdentityHashMap never invokes the equals method on its keys. An added benefit of this implementation is that it is fast.

# Concurrent Implementations

> The java.util.concurrent package contains the ConcurrentMap interface, which extends Map with atomic putIfAbsent, remove, and replace methods, and the ConcurrentHashMap implementation of that interface.

> ConcurrentHashMap is a **highly concurrent**, **high-performance** implementation **backed up by a hash table**. **This implementation never blocks when performing retrievals and allows the client to select the concurrency level for updates.** It is intended as a drop-in replacement for Hashtable: in addition to implementing ConcurrentMap, it supports all the legacy methods peculiar to Hashtable. Again, if you don't need the legacy operations, be careful to manipulate it with the ConcurrentMap interface.