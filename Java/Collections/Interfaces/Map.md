# Map interface

==A `Map` object holds a key and a corresponding value.==

*It cannot contain duplicate values*

*A key can at most map to one value*

Two maps are **equal** if they ==represent the same key-value mappings==

## Basic Operations
The basic operations of Map (`put`, `get`, `containsKey`, `containsValue`, `size`, and `isEmpty`) behave exactly like their counterparts in `Hashtable`. 

## Bulk Operations

* `putAll()`
* `clear()`

## Collections View

Three views are provided.

1. keySet - Get the keys in the map **returned as SET**. `Map.keySet()`.
2. Values  - Get all the values in the map. This is **not a Set** as multiple keys can map to the same value. `Map.values()` returns a `Collection<V>`.
3. entrySet - Key - Value pairs contained in the map. Will be given as `Set` of `Map.Entry<K,V`  i.e `Set<Map.Entry<K,V>>` . Any changes to these will be reflected back in the backing Map. `Map.entrySet()` return `Set<Map.Entry<K,V>>`.

E.g Program iterating the keys

```java
	for(KeyType k : map.keySet()) {
		System.out.println("K: " + k);
	}
```

E.g. Program for iterating the entrySet view.

```java
	for(Map.Entry<K,V> entry: map.entrySet()) {
		System.out.println("Key: " + entry.getKey() + " and Value: " + entry.getValue());
	}
```

==The collection views supports removal of elements (assuming the backing map supports the operation). But they do not **support addition operations in any of the views** as `put` and `putAll` are present for the same.== It wouldn't make sense for the `keySet` and `values` view and it is not neccessary for `entrySet` view as the Map already does the same with `put` and `putAll`.

## Fancy uses of Collection Views: Map Algebra

* `containsAll()` - Subset operation
* `retainAll()`	 - intersection operation - Remove all elements from A which are not present in B.
* `removeAll()` - Set Difference - Remove all elements from A which are also present in B.

Whether the first Map contains all the key-value mappings in the second.
```java
if (m1.entrySet().containsAll(m2.entrySet())) {
    ...
}
```

Along similar lines, suppose you want to know whether two Map objects contain mappings for all of the same keys.
```java
if (m1.keySet().equals(m2.keySet())) {
    ...
}
```

## MultiMap

A `Map` which maps multiple values to a key.

No interface is provided in the Collections Framework as these are not used often.

```java
Map<String, List<String>> m = new HashMap<String, List<String>>();
```