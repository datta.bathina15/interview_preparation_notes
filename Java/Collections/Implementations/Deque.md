# General-Purpose Deque Implementations

ArrayDeque - Resizeable Array implementation.

LinkedList - Doubly-linked list implementation

LinkedList is more flexible. 

Null values are allowed in `LinkedList` but not in `ArrayDeque`.

`ArrayDeque` are more effecient in adding and removal. 

`LinkedList` is best at removing the current element during iteration. They are not ideal to iterate. They consume more memory.

# Concurrent Deque Implementations

 The **LinkedBlockingDeque** class is the concurrent implementation of the Deque interface. If the deque is empty then methods such as takeFirst and takeLast wait until the element becomes available, and then retrieves and removes the same element. 
 
 It's optionally-bounded blocking deque based on linked nodes. 