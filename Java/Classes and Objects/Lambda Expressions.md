Lambda expressions enable you to treat functionality as method argument, or code as data.

```java
public class Calculator {
	// Functional interface 
    interface IntegerMath {
        int operation(int a, int b);   
    }
  
    public int operateBinary(int a, int b, IntegerMath op) {
        return op.operation(a, b);
    }
 
    public static void main(String... args) {
    
        Calculator myApp = new Calculator();
        IntegerMath addition = (a, b) -> a + b;
        IntegerMath subtraction = (a, b) -> a - b;
        System.out.println("40 + 2 = " +
            myApp.operateBinary(40, 2, addition));
        System.out.println("20 - 10 = " +
            myApp.operateBinary(20, 10, subtraction));    
    }
}
```

# Method References
> You use [lambda expressions](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html) to create anonymous methods. Sometimes, however, a lambda expression does nothing but call an existing method. In those cases, it's often clearer to refer to the existing method by name. Method references enable you to do this; they are compact, **easy-to-read lambda expressions for methods that already have a name**.

## Kinds of Method References


| Kind                                                                        | Syntax                               | Examples                                                             |
|-----------------------------------------------------------------------------|--------------------------------------|----------------------------------------------------------------------|
| Reference to a static method                                                | ContainingClass::staticMethodName    | Person::compareByAge MethodReferencesExamples::appendStrings |
| Reference to an instance method of a particular object                      | containingObject::instanceMethodName | myComparisonProvider::compareByName myApp::appendStrings2    |
| Reference to an instance method of an arbitrary object of a particular type | ContainingType::methodName           | String::compareToIgnoreCase   String::concat                   |
| Reference to a constructor                                                  | ClassName::new                       | HashSet::new                                                         |

The following example, [`MethodReferencesExamples`](https://docs.oracle.com/javase/tutorial/java/javaOO/examples/MethodReferencesExamples.java), contains examples of the first three types of method references:
```java
import java.util.function.BiFunction;

public class MethodReferencesExamples {
    
    public static <T> T mergeThings(T a, T b, BiFunction<T, T, T> merger) {
        return merger.apply(a, b);
    }
    
    public static String appendStrings(String a, String b) {
        return a + b;
    }
    
    public String appendStrings2(String a, String b) {
        return a + b;
    }

    public static void main(String[] args) {
        
        MethodReferencesExamples myApp = new MethodReferencesExamples();

        // Calling the method mergeThings with a lambda expression
        System.out.println(MethodReferencesExamples.
            mergeThings("Hello ", "World!", (a, b) -> a + b));
        
        // Reference to a static method
        System.out.println(MethodReferencesExamples.
            mergeThings("Hello ", "World!", MethodReferencesExamples::appendStrings));

        // Reference to an instance method of a particular object        
        System.out.println(MethodReferencesExamples.
            mergeThings("Hello ", "World!", myApp::appendStrings2));
        
        // Reference to an instance method of an arbitrary object of a
        // particular type
        System.out.println(MethodReferencesExamples.
            mergeThings("Hello ", "World!", String::concat));
    }
}
```

### Reference to an Instance Method of an Arbitrary Object of a Particular Type

The following is an example of a reference to an instance method of an arbitrary object of a particular type:
```java
String[] stringArray = { "Barbara", "James", "Mary", "John",
    "Patricia", "Robert", "Michael", "Linda" };
Arrays.sort(stringArray, String::compareToIgnoreCase);
```

The equivalent lambda expression for the method reference `String::compareToIgnoreCase` would have the formal parameter list `(String a, String b)`, where `a` and `b` are arbitrary names used to better describe this example. The method reference would invoke the method `a.compareToIgnoreCase(b)`.

Similarly, the method reference `String::concat` would invoke the method `a.concat(b)`.