# API Design

## Parameters

**Always use Collections interfaces for method parameters.** Try to use the least specific type as possible like `Collection` or `Map`.  `List` is also fine as we might need to depend on the order of the elements. **It depends on the requirements.**
==But always use **interfaces** for parameters.==

## Return Values

It's ok to return an object of any type that implements or extends the collections interfaces. 

In one sense, return values should have the opposite behavior of input parameters: **It's best to return the most specific applicable collection interface** rather than the most general.

## Legacy APIs

Either retrofit the legacy code to use the standard collections interfaces OR try to define an adapter using the abstract classes (see custom implementations) if retrofitting is impossible.