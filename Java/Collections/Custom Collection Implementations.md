Collection framework provides ==Abstract classes== for the interfaces which can be extended to implement custom implementations. 

The following list summarizes the abstract implementations:
* **AbstractCollection** — a Collection that is neither a Set nor a List. At a minimum, you must provide the iterator and the size methods.
* **AbstractSet** — a Set; use is identical to AbstractCollection.
* **AbstractList** — a List backed up by a random-access data store, such as an array. At a minimum, you must provide the positional access methods (get and, optionally, set, remove, and add) and the size method. The abstract class takes care of listIterator (and iterator).
* **AbstractSequentialList** — a List backed up by a sequential-access data store, such as a linked list. At a minimum, you must provide the listIterator and size methods. The abstract class takes care of the positional access methods. (This is the opposite of AbstractList.)
* **AbstractQueue** — at a minimum, you must provide the offer, peek, poll, and size methods and an iterator supporting remove.
* **AbstractMap** — a Map. At a minimum you must provide the entrySet view. This is typically implemented with the AbstractSet class. If the Map is modifiable, you must also provide the put method.

E.g. 
```java
public static <T> List<T> asList(T[] a) {
    return new MyArrayList<T>(a);
}

private static class MyArrayList<T> extends AbstractList<T> {

    private final T[] a;

    MyArrayList(T[] array) {
        a = array;
    }

    public T get(int index) {
        return a[index];
    }

    public T set(int index, T element) {
        T oldValue = a[index];
        a[index] = element;
        return oldValue;
    }

    public int size() {
        return a.length;
    }
}
```