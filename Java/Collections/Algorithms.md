# Sorting

```java
Collections.sort(list)
```

The sort operation uses ==a highly optimized merge sort== that is fast and stable.
* fast - Faster than quick sort. Runtime is `n log(n)` and runs subtantially faster on nearly sorted lists. A quicksort is generally considered to be faster than a merge sort but isn't stable and doesn't guarantee `n log(n)` performance.
* Stable - It doesn't reorder equal elements. This is important if you sort the same list repeatedly on different attributes. If a user of a mail program sorts the inbox by mailing date and then sorts it by sender, the user naturally expects that the now-contiguous list of messages from a given sender will (still) be sorted by mailing date. This is guaranteed only if the second sort was stable.

```java
Collections.sort(list, comparator)
```

# Shuffling

Destroys the order that may be present in the list. 
> That is, this algorithm reorders the List based on input from a source of randomness such that all possible permutations occur with equal likelihood, assuming a fair source of randomness.

E.g Shuffling with a given source of randomness. 
```java
import java.util.*;

public class Shuffle {
    public static void main(String[] args) {
        List<String> list = new ArrayList<String>();
        for (String a : args)
            list.add(a);
        Collections.shuffle(list, new Random());
        System.out.println(list);
    }
}
```

E.g Shuffling using default source of randomness.
```java
import java.util.*;

public class Shuffle {
    public static void main(String[] args) {
        List<String> list = Arrays.asList(args);
        Collections.shuffle(list);
        System.out.println(list);
    }
}
```

# Routine Data Manipulation

The Collections class provides five algorithms for doing routine data manipulation on List objects, all of which are pretty straightforward:

* **reverse** — reverses the order of the elements in a List.
* **fill** — overwrites every element in a List with the specified value. This operation is useful for reinitializing a List.
* **copy** — takes two arguments, a destination List and a source List, and copies the elements of the source into the destination, overwriting its contents. The destination List must be at least as long as the source. If it is longer, the remaining elements in the destination List are unaffected.
* **swap** — swaps the elements at the specified positions in a List.
* **addAll** — adds all the specified elements to a Collection. The elements to be added may be specified individually or as an array.

# Searching

The **binarySearch** algorithm searches for a specified element in a sorted List. This algorithm has two forms. 
* The first takes a List and an element to search for (the "search key"). This form assumes that the List is sorted in ascending order according to the natural ordering of its elements. 
* The second form takes a Comparator in addition to the List and the search key, and assumes that the List is sorted into ascending order according to the specified Comparator. The sort algorithm can be used to sort the List prior to calling binarySearch.

If the List contains the search key, its index is returned. 

If not, the return value is (-(insertion point) - 1), where the insertion point is 
* The point at which the value would be inserted into the List
* Or the index of the first element greater than the value
* Or list.size() if all elements in the List are less than the specified value. 

The return value will always be < 0 if the given key is not found. The above formula ensures that.

# Composition

The frequency and disjoint algorithms test some aspect of the composition of one or more Collections:

* **frequency** — counts the number of times the specified element occurs in the specified collection
* **disjoint** — determines whether two Collections are disjoint; that is, whether they contain no elements in common


# Finding Extreme Values

The `min` and the `max` algorithms return, respectively, the minimum and maximum element contained in a specified Collection. Both of these operations come in two forms. 
* The simple form takes only a Collection and returns the minimum (or maximum) element according to the elements' natural ordering.
* The second form takes a **Comparator** in addition to the Collection and returns the minimum (or maximum) element according to the specified Comparator.