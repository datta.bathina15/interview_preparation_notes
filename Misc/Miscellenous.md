# REST vs RESTful
REST - **R**epresentational **S**tate **T**ransfer
- It must be stateless
- It must access the resources through URI only
- It must not contain inbuilt encryption
- It must not contain session
- It must communicate through HTTP protocol only
- It must perform CRUD operations using HTTP verbs like GET, POST, PUT, DELETE.
- It must return data only in the form of XML OR JSON, atom, oData etc.

A web API that obeys the [REST constraints](https://en.wikipedia.org/wiki/Representational_state_transfer#Architectural_constraints "Representational state transfer") is informally described as _RESTful_.

`REST based services` follow some of the above principles and not all.

`RESTFUL services` means it follows all the above principles.

# **Hypermedia as the engine of application state ([HATEOAS](https://en.wikipedia.org/wiki/HATEOAS "HATEOAS"))** 
**Hypermedia as the engine of application state ([HATEOAS](https://en.wikipedia.org/wiki/HATEOAS "HATEOAS"))** - Having accessed an initial URI for the REST application—analogous to a human Web user accessing the [home page](https://en.wikipedia.org/wiki/Home_page "Home page") of a website—a REST client should then be able to use server-provided links dynamically to discover all the available resources it needs. As access proceeds, the server responds with text that includes [hyperlinks](https://en.wikipedia.org/wiki/Hyperlink "Hyperlink") to other resources that are currently available. There is no need for the client to be hard-coded with information regarding the structure or dynamics of the application.