# Java 8
1. Streams API
2. Lambda's
3. Method References
4. Default methods
5. Streams API
6. New Date API (LocalDate, LocalDateTime)
7. Nashhorn Javascript engine

# Java 11
1. New String methods
	- isBlank
	- lines
	- stripLeading
	- stripTrailing
2. New File methods
	- readString
	- writeString
3. Collection toArray()
	- toArray() method is added to the Collection interface
4. Static not() in Predicate interface
5. Local variable syntax for lambdas
6. HTTP Client (java.net.http)
7. Running Java Files
	- We run java files directly without `javac` command.
```bash
$ javac HelloWorld.java
$ java HelloWorld 
Hello Java 8!
```

```bash
$ java HelloWorld.java
Hello Java 11!
```

# Java 17
1. Enhanced Pseudo-Random Number Generators
2. New macOS Rendering Pipeline
3. macOS/AArch64 Port
4. Deprecate the Applet API for Removal
5. Pattern Matching for Switch (Preview feature)
6. Remove RMI Activation
7. Sealed Classes
	- The feature restricts which other classes or interfaces may extend or implement a sealed component.
8. Remove the Experimental AOT and JIT Compiler