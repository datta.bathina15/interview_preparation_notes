# Aspect Oriented Programming

Some requirements that might not fit well in the architecture, such as:

- The application must have an authentication system, used before any query or update.
- The data must be validated before it’s written to the database.
- The application must have auditing and logging for sensible operations.
- The application must maintain a debugging log to check if operations are OK.
- Some operations must have their performance measured to see if they’re in the desired range.

Any of these requirements need a lot of work and, more than that, code duplication. You have to add the same code in many parts of the system, which goes against the “don’t repeat yourself” (DRY) principle and makes maintenance more difficult. Any requirement change causes a massive change in the program.

* * *

==Aspect-oriented programming (AOP) is a programming paradigm that aims to increase modularity by allowing the separation of cross-cutting concerns.==

It separates general code from aspects that cross the boundaries of an object or a layer.

It deals with functionality that occurs in multiple parts of the system and separates it from the core of the application, thus improving separation of concerns while avoiding duplication of code and coupling.

The key unit of modularity in OOP is the class, whereas in AOP the unit of modularity is the aspect.

## AOP Concepts

1.  **Aspect**: A modularization of a concern that cuts across multiple classes. Transaction management is a good example of a crosscutting concern in enterprise Java applications. In Spring AOP, aspects are implemented by using regular classes (the schema-based approach) or regular classes annotated with the @Aspect annotation.
2.  **Join point**: A point during the execution of a program, such as the execution of a method or the handling of an exception. In Spring AOP, a join point always represents a method execution.
3.  **Advice**: Action taken by an aspect at a particular join point. Different types of advice include “around”, “before” and “after” advice. (Advice types are discussed later.) Many AOP frameworks, including Spring, model an advice as an interceptor and maintain a chain of interceptors around the join point.
4.  **Pointcut**: A predicate that matches join points. Advice is associated with a pointcut expression and runs at any join point matched by the pointcut (for example, the execution of a method with a certain name). The concept of join points as matched by pointcut expressions is central to AOP, and Spring uses the AspectJ pointcut expression language by default.
5.  **Introduction**: Declaring additional methods or fields on behalf of a type. Spring AOP lets you introduce new interfaces (and a corresponding implementation) to any advised object. For example, you could use an introduction to make a bean implement an IsModified interface, to simplify caching. (An introduction is known as an inter-type declaration in the AspectJ community.)
6.  **Target object**: An object being advised by one or more aspects. Also referred to as the “advised object”. Since Spring AOP is implemented by using runtime proxies, this object is always a proxied object.
7.  **AOP proxy**: An object created by the AOP framework in order to implement the aspect contracts (advise method executions and so on). In the Spring Framework, an AOP proxy is a JDK dynamic proxy or a CGLIB proxy.
8.  **Weaving**: linking aspects with other application types or objects to create an advised object. This can be done at compile time (using the AspectJ compiler, for example), load time, or at runtime. Spring AOP, like other pure Java AOP frameworks, performs weaving at runtime.

## Types of Advice

- **Before advice**: Advice that runs before a join point but that does not have the ability to prevent execution flow proceeding to the join point (unless it throws an exception).
- **After returning advice**: Advice to be run after a join point completes normally (for example, if a method returns without throwing an exception).
- **After throwing advice**: Advice to be run if a method exits by throwing an exception.
- **After (finally) advice**: Advice to be run regardless of the means by which a join point exits (normal or exceptional return).
- **Around advice**: Advice that surrounds a join point such as a method invocation. This is the most powerful kind of advice. Around advice can perform custom behavior before and after the method invocation. It is also responsible for choosing whether to proceed to the join point or to shortcut the advised method execution by returning its own return value or throwing an exception.

## Spring AOP Capabilities and Goals

Spring AOP is implemented in pure Java. Spring AOP does not need to control the class loader hierarchy and is thus suitable for use in a servlet container or application server.

**Spring AOP curently only supports method execution join points.** Filed level advice is not supported.

## AOP Proxies

AOP created a proxy object and stores the target object in it.

Spring AOP defaults to using standard **JDK dynamic proxies for AOP proxies**. This enables any interface (or set of interfaces) to be proxied.

Spring AOP can also use **CGLIB proxies**. This is necessary to proxy classes rather than interfaces. **By default, CGLIB is used if a business object does not implement an interface.**

==Spring AOP is proxy-based.==

![Proxy Object Example representation](https://docs.spring.io/spring-framework/docs/current/reference/html/images/aop-proxy-call.png)

Once the call has finally reached the target object (the SimplePojo reference in this case), any method calls that it may make on itself, such as this.bar() or this.foo(), are going to be invoked against the this reference, and not the proxy. This has important implications. **It means that self-invocation is not going to result in the advice associated with a method invocation getting a chance to run.**

>  ==**Advising aspects with other aspects?**==
> In Spring AOP, ==aspects themselves **cannot** be the targets of advice from other aspects==. The @Aspect annotation on a class marks it as an aspect and, hence, excludes it from auto-proxying. 

##  Enabling @AspectJ Support

```java
@Configuration
@EnableAspectJAutoProxy
public class AppConfig {

}
```

## Declaring an Aspect

```java
package org.xyz;
import org.aspectj.lang.annotation.Aspect;

@Aspect
public class NotVeryUsefulAspect {

}
```

## Declaring a Pointcut

Spring AOP only supports method execution join points for Spring beans, so you can think of a pointcut as matching the execution of methods on Spring beans. 

**A pointcut declaration has two parts:** 
- a signature comprising a name and any parameters
- a pointcut expression that determines exactly which method executions we are interested in.

```java
@Pointcut("execution(* transfer(..))") // the pointcut expression
private void anyOldTransfer() {} // the pointcut signature
```

### Supported Pointcut Designators

There are 9 Pointcut Designators(PCD).

#### execution:

For matching method execution join points. This is the primary pointcut designator to use when working with Spring AOP.

##### examples 

The execution of any public method:
```
execution(public * *(..))
```

The execution of any method with a name that begins with set:
```
execution(* set*(..))
```

The execution of any method defined by the AccountService interface:

```
execution(* com.xyz.service.AccountService.*(..))
```

The execution of any method defined in the service package:

```
execution(* com.xyz.service.*.*(..))
```

The execution of any method defined in the service package or one of its sub-packages:
```
execution(* com.xyz.service..*.*(..))
```

#### within:

Limits matching to join points within certain types (the execution of a method declared within a matching type when using Spring AOP).

##### examples

Any join point (method execution only in Spring AOP) within the service package:

```
within(com.xyz.service.*)
```

Any join point (method execution only in Spring AOP) within the service package or one of its sub-packages:

```
within(com.xyz.service..*)
```

#### this:

Limits matching to join points (the execution of methods when using Spring AOP) where the bean reference (Spring AOP proxy) is an instance of the given type.

##### examples

Any join point (method execution only in Spring AOP) where the proxy implements the AccountService interface:

```
this(com.xyz.service.AccountService)
```

#### target:

Limits matching to join points (the execution of methods when using Spring AOP) where the target object (application object being proxied) is an instance of the given type.

##### examples

Any join point (method execution only in Spring AOP) where the target object implements the AccountService interface:

```
target(com.xyz.service.AccountService)
```

#### args:

Limits matching to join points (the execution of methods when using Spring AOP) where the arguments are instances of the given types.

##### examples

Any join point (method execution only in Spring AOP) that takes a single parameter and where the argument passed at runtime is Serializable:

```
args(java.io.Serializable)
```

#### @target:

Limits matching to join points (the execution of methods when using Spring AOP) where the class of the executing object has an annotation of the given type.

##### examples

Any join point (method execution only in Spring AOP) where the target object has a @Transactional annotation:

```
@target(org.springframework.transaction.annotation.Transactional)
```

#### @args:

Limits matching to join points (the execution of methods when using Spring AOP) where the runtime type of the actual arguments passed have annotations of the given types.

##### examples

Any join point (method execution only in Spring AOP) which takes a single parameter, and where the runtime type of the argument passed has the @Classified annotation:

```
@args(com.xyz.security.Classified)
```


#### @within:

Limits matching to join points within types that have the given annotation (the execution of methods declared in types with the given annotation when using Spring AOP).

##### examples

Any join point (method execution only in Spring AOP) where the declared type of the target object has an @Transactional annotation:

```
@within(org.springframework.transaction.annotation.Transactional)
```

#### @annotation:

Limits matching to join points where the subject of the join point (the method being run in Spring AOP) has the given annotation.

##### examples

Any join point (method execution only in Spring AOP) where the executing method has an @Transactional annotation:

```
@annotation(org.springframework.transaction.annotation.Transactional)
```

#### bean:

Spring AOP also supports an additional PCD named bean. This PCD lets you limit the matching of join points to a particular named Spring bean or to a set of named Spring beans (when using wildcards). The bean PCD has the following form:

```java
bean(idOrNameOfBean)
```

##### examples 

Any join point (method execution only in Spring AOP) on a Spring bean named tradeService:

```
bean(tradeService)
```

Any join point (method execution only in Spring AOP) on Spring beans having names that match the wildcard expression *Service:

```
bean(*Service)
```

### Execution expression format

Spring AOP users are likely to use the execution pointcut designator the most often. The format of an execution expression follows:

```
execution(modifiers-pattern? ret-type-pattern declaring-type-pattern?name-pattern(param-pattern)
                throws-pattern?)
```

Patterns marked with **?** at the end are optional. 

 ## Declaring Advice
 
Advice is associated with a pointcut expression and runs before, after, or around method executions matched by the pointcut.

### Before

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class BeforeExample {

    @Before("com.xyz.myapp.CommonPointcuts.dataAccessOperation()")
    public void doAccessCheck() {
        // ...
    }
}
```

**If we use an in-place pointcut expression, we could rewrite the preceding example as the following example:**

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class BeforeExample {

    @Before("execution(* com.xyz.myapp.dao.*.*(..))")
    public void doAccessCheck() {
        // ...
    }
}
```

> We can use pointcut expressions directly in advice annotations.

### After Returning Advice

After returning advice runs when a matched method execution returns normally. You can declare it by using the @AfterReturning annotation:

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.AfterReturning;

@Aspect
public class AfterReturningExample {

    @AfterReturning("com.xyz.myapp.CommonPointcuts.dataAccessOperation()")
    public void doAccessCheck() {
        // ...
    }
}
```

To get the return value to be used in the method body use **returning** in the annotation.

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.AfterReturning;

@Aspect
public class AfterReturningExample {

    @AfterReturning(
        pointcut="com.xyz.myapp.CommonPointcuts.dataAccessOperation()",
        returning="retVal")
    public void doAccessCheck(Object retVal) {
        // ...
    }
}
```

A returning clause also restricts matching to only those method executions that return a value of the specified type (in this case, Object, which matches any return value).

### After Throwing Advice

**@AfterThrowing**

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.AfterThrowing;

@Aspect
public class AfterThrowingExample {

    @AfterThrowing("com.xyz.myapp.CommonPointcuts.dataAccessOperation()")
    public void doRecoveryActions() {
        // ...
    }
}
```

To get the exception and also restrict the matching to certain exception types only use `throwing`.

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.AfterThrowing;

@Aspect
public class AfterThrowingExample {

    @AfterThrowing(
        pointcut="com.xyz.myapp.CommonPointcuts.dataAccessOperation()",
        throwing="ex")
    public void doRecoveryActions(DataAccessException ex) {
        // ...
    }
}
```

When a method execution exits by throwing an exception, the exception is passed to the advice method as the corresponding argument value. A throwing clause also restricts matching to only those method executions that throw an exception of the specified type (DataAccessException, in this case).

### After (Finally) Advice

After (finally) advice runs when a matched method execution exits. It is declared by using the `@After` annotation. 

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.After;

@Aspect
public class AfterFinallyExample {

    @After("com.xyz.myapp.CommonPointcuts.dataAccessOperation()")
    public void doReleaseLock() {
        // ...
    }
}
```

### Around Advice

Around advice runs "around" a matched method’s execution. It has the opportunity to do work both before and after the method runs and to determine when, how, and even if the method actually gets to run at all. Around advice is often used **if you need to share state before and after a method execution in a thread-safe manner** – for example, starting and stopping a timer.

Around advice is declared by annotating a method with the `@Around` annotation. 

The method should declare `Object` as its return type, and the first parameter of the method must be of type `ProceedingJoinPoint`. 

Within the body of the advice method, you must invoke proceed() on the ProceedingJoinPoint in order for the underlying method to run.

Invoking `proceed()` without arguments will result in the **caller’s original arguments being supplied to the underlying method when it is invoked**. For advanced use cases, there is an **overloaded** variant of the proceed() method **which accepts an array of arguments (Object[])**. **The values in the array will be used as the arguments to the underlying method when it is invoked.**

> If you declare the return type of your around advice method as void, null will always be returned to the caller, effectively ignoring the result of any invocation of proceed(). It is therefore recommended that an around advice method declare a return type of Object. 

```java
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.ProceedingJoinPoint;

@Aspect
public class AroundExample {

    @Around("com.xyz.myapp.CommonPointcuts.businessService()")
    public Object doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
        // start stopwatch
        Object retVal = pjp.proceed();
        // stop stopwatch
        return retVal;
    }
}
```

### Advice Parameters

**Any advice method may declare, as its first parameter**, a parameter of type `org.aspectj.lang.JoinPoint`. Note that around advice is required to declare a first parameter of type `ProceedingJoinPoint`, which is a subclass of JoinPoint.

The JoinPoint interface provides a number of useful methods:
- getArgs(): Returns the method arguments.
- getThis(): Returns the proxy object.
- getTarget(): Returns the target object.
- getSignature(): Returns a description of the method that is being advised.
- toString(): Prints a useful description of the method being advised.

#### Passing Parameters to Advice

To make argument values available to the advice body, you can use the binding form of args. 

```java
@Before("com.xyz.myapp.CommonPointcuts.dataAccessOperation() && args(account,..)")
public void validateAccount(Account account) {
    // ...
}
```

Another way of writing this is to declare a pointcut that "provides" the Account object value when it matches a join point, and then refer to the named pointcut from the advice. This would look as follows:

```java
@Pointcut("com.xyz.myapp.CommonPointcuts.dataAccessOperation() && args(account,..)")
private void accountDataAccessOperation(Account account) {}

@Before("accountDataAccessOperation(account)")
public void validateAccount(Account account) {
    // ...
}
```

#### Advice Parameters and Generics

```java
public interface Sample<T> {
    void sampleGenericMethod(T param);
    void sampleGenericCollectionMethod(Collection<T> param);
}
```

You can restrict interception of method types to certain parameter types by tying the advice parameter to the parameter type for which you want to intercept the method:

```java
@Before("execution(* ..Sample+.sampleGenericMethod(*)) && args(param)")
public void beforeSampleMethod(MyType param) {
    // Advice implementation
}
```

==This approach does not work for generic collections.==
```java
@Before("execution(* ..Sample+.sampleGenericCollectionMethod(*)) && args(param)")
public void beforeSampleMethod(Collection<MyType> param) {
    // Advice implementation
}
```

**The above example doesn't work!!!**

We can use wildcards to achieve such operation. E.g. `Collection<?>`

#### Determining Argument Names

**The parameter binding in advice invocations relies on matching names used in pointcut expressions to declared parameter names in advice and pointcut method signatures.**

Parameter names are not available through Java reflection, so Spring AOP uses the following strategy to determine parameter names:
- **argNames**
	```java
	@Before(value="com.xyz.lib.Pointcuts.anyPublicMethod() && target(bean) && @annotation(auditable)",
			argNames="bean,auditable")
	public void audit(Object bean, Auditable auditable) {
		AuditCode code = auditable.value();
		// ... use code and bean
	}
	```
	*If the first parameter is of the JoinPoint, ProceedingJoinPoint, or JoinPoint.StaticPart type, you can leave out the name of the parameter from the value of the argNames attribute.*
- **Debug Information**: if the argNames attribute has not been specified, **Spring AOP looks at the debug information for the class and tries to determine the parameter names from the local variable table.** This information is present as long as the classes have been compiled with debug information (-g:vars at a minimum). 
	The consequences of compiling with this flag on are: 
	1. your code is slightly easier to understand (reverse engineer), 
	2. the class file sizes are very slightly bigger (typically inconsequential) 
	3. the optimization to remove unused local variables is not applied by your compiler. In other words, you should encounter no difficulties by building with this flag on.
- If the code has been compiled without the necessary debug information, Spring AOP tries to deduce the pairing of binding variables to parameters (for example, if only one variable is bound in the pointcut expression, and the advice method takes only one parameter, the pairing is obvious). If the binding of variables is ambiguous given the available information, an `AmbiguousBindingException` is thrown.
- If all of the above strategies fail, an `IllegalArgumentException` is thrown.

### Advice Ordering

The highest precedence advice runs first "on the way in" (so, given two pieces of before advice, the one with highest precedence runs first). "On the way out" from a join point, the highest precedence advice runs last (so, given two pieces of after advice, the one with the highest precedence will run second).

Before advice - High -> Low 
After advice - Low -> High

When two pieces of advice defined in different aspects both need to run at the same join point, unless you specify otherwise, **the order of execution is undefined**. 

>As of Spring Framework 5.2.7, advice methods defined in the same @Aspect class that need to run at the same join point are assigned precedence based on their advice type in the following order, from highest to lowest precedence: @Around, @Before, @After, @AfterReturning, @AfterThrowing. 

We can control ordering of **aspect** classes by implementing the `org.springframework.core.Ordered` or with `@Order` annotation. **The aspect returning the lower value from Ordered.getOrder() (or the annotation value) has the higher precedence.**

## Introductions

==Introductions (known as inter-type declarations in AspectJ) enable an aspect to declare that advised objects implement a given interface, and to provide an implementation of that interface on behalf of those objects.==

You can make an introduction by using the `@DeclareParents` annotation. 

```java
@Aspect
public class UsageTracking {

    @DeclareParents(value="com.xzy.myapp.service.*+", defaultImpl=DefaultUsageTracked.class)
    public static UsageTracked mixin;

    @Before("com.xyz.myapp.CommonPointcuts.businessService() && this(usageTracked)")
    public void recordUsage(UsageTracked usageTracked) {
        usageTracked.incrementUseCount();
    }

}
```

The value attribute of the @DeclareParents annotation is an AspectJ type pattern. Any bean of a matching type implements the UsageTracked interface.

