# Convenience Implementations

Several mini-implementations that can be more convenient and more efficient than general-purpose implementations when you don't need their full power. All the implementations in this section are made available via static factory methods rather than public classes.

## List View of an Array

`Array.asList` - Method returns a `List` view of its array argument. Changes to the List write through to the array and vice versa. **The size of the collection is that of the array and cannot be changed**. If the add or the remove method is called on the List, an `UnsupportedOperationException` will result.

```java
List<String> list = Arrays.asList(new String[size]);
```

## Immutable Multiple-Copy List

`Collections.nCopies`	returns an immutable list consiting of  `n` copies of the given element.

Uses:
* To initialize a newly created `list`.
	```java
	List<Type> list = new ArrayList<Type>(Collections.nCopies(1000, (Type)null));
	```
* To grow a `List`.
	```java
	lovablePets.addAll(Collections.nCopies(69, "fruit bat"));
	```
	
## Immutable Singleton Set

`Collections.singleton(o)` gives an immutable set with the given element. 
E.g. Remove all occurences of an element from a Set.
```java
exampleSet.removeAll(Collections.singleton(e));
```

A related idiom removes all elements that map to a specified value from a Map. For example, suppose you have a Map — job — that maps people to their line of work and suppose you want to eliminate all the lawyers. The following one-liner will do the deed.
```java
job.values().removeAll(Collections.singleton(LAWYER));
```

## Empty Set, List, and Map Constants

> The Collections class provides methods to return the empty Set, List, and Map — emptySet, emptyList, and emptyMap. The main use of these constants is as input to methods that take a Collection of values when you don't want to provide any values at all, as in this example.
```java
tourist.declarePurchases(Collections.emptySet());
```

All the above mentioned methods return immutable collections.