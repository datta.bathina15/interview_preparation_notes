# General-Purpose Queue Implementations

`LinkedList` implemets `Queue` interface, providing first in, first out (FIFO) queue operations for add, poll, and so on.

## PriorityQueue

The PriorityQueue is based on heap data structure. This queue orders elements according to the **order specified at construction time**, which can be the elements' **natural ordering** or **the ordering imposed by an explicit Comparator**.

PriorityQueue does not allow null values.

The queue retrieval operations — poll, remove, peek, and element — **access the element at the head of the queue**. 

**The head of the queue is the least element with respect to the specified ordering**. If multiple elements are tied for least value, the head is one of those elements; ties are broken arbitrarily.

# Concurrent Queue Implementations

## BlockingQueue

Interface implemented in `java.util.concurrent`. 

`BlockingQueue` extends `Queue` with opeartions that wait for queue to become **non-empty** before retrieving elements and for **space to become available** before inserting.

BlockingQueue does not accept null elementes.

BlockingQueue Implementations: 
* LinkedBlockingQueue — an optionally bounded FIFO blocking queue backed by linked nodes
* ArrayBlockingQueue — a bounded FIFO blocking queue backed by an array
* PriorityBlockingQueue — an unbounded blocking priority queue backed by a heap
* DelayQueue — a time-based scheduling queue backed by a heap
* SynchronousQueue — a simple rendezvous mechanism that uses the BlockingQueue interface
* LinkedTransferQueue — an unbounded TransferQueue based on linked nodes.