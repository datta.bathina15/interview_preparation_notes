A [`Set`](https://docs.oracle.com/javase/8/docs/api/java/util/Set.html) is a [`Collection`](https://docs.oracle.com/javase/8/docs/api/java/util/Collection.html) that cannot contain duplicate elements. It models the mathematical set abstraction. The `Set` interface contains _only_ methods inherited from `Collection` and adds the restriction that duplicate elements are prohibited.

`Set` implementations: 
1. `HashSet`
2. `TreeSet`
3. `LinkedHashSet`

```java
Collection<Type> noDups = new HashSet<Type>(c);

c.stream()
.collect(Collectors.toSet()); // no duplicates
```

## Set Interface Bulk Operations
-   `s1.containsAll(s2)` — returns `true` if `s2` is a **subset** of `s1`. (`s2` is a subset of `s1` if set `s1` contains all of the elements in `s2`.)
-   `s1.addAll(s2)` — transforms `s1` into the **union** of `s1` and `s2`. (The union of two sets is the set containing all of the elements contained in either set.)
-   `s1.retainAll(s2)` — transforms `s1` into the **intersection** of `s1` and `s2`. (The intersection of two sets is the set containing only the elements common to both sets.)
-   `s1.removeAll(s2)` — transforms `s1` into the (asymmetric) **set difference** of `s1` and `s2`. (For example, the set difference of `s1` minus `s2` is the set containing all of the elements found in `s1` but not in `s2`.)