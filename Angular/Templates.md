# Text Interpolation
Text interpolation lets you incorporate dynamic string values into your HTML templates.

src/app/app.component.html
```html
<h3>Current customer: {{ currentCustomer }}</h3>
```

src/app/app.component.ts
```typescript
currentCustomer = 'Maria';
```

# Template statements
Template statements are methods or properties that you can use in your HTML to respond to user events.

```html
<button type="button" (click)="deleteHero()">Delete hero</button>
```

Template statements use a language that looks like JavaScript. However, the parser for template statements differs from the parser for template expressions. In addition, the template statements parser specifically supports both basic assignment (`=`) and chaining expressions with semicolons (`;`).

The following JavaScript and template expression syntax is not allowed:
-   `new`
-   Increment and decrement operators, `++` and `--`
-   Operator assignment, such as `+=` and `-=`
-   The bitwise operators, such as `|` and `&`
-   The [pipe operator](https://angular.io/guide/pipes)

## Context
Statements have a context —a particular part of the application to which the statement belongs.

Statements can refer only to what's in the statement context, which is typically the component instance.

Template statements can have either **Component instace context** or **Template context**.

Template context example
```html
<button type="button" (click)="onSave($event)">Save</button>
<button type="button" *ngFor="let hero of heroes" (click)="deleteHero(hero)">{{hero.name}}</button>
<form #heroForm (ngSubmit)="onSubmit(heroForm)"> ... </form>
```

In this example, the context of the `$event` object, `hero`, and `#heroForm` is the template.

**Template context names take precedence over component context names.** In the preceding `deleteHero(hero)`, the `hero` is the template input variable, not the component's `hero` property.

## Best Practices
### Conciseness
Use method calls or basic property assignments to keep template statements minimal.

### Work within the context
The context of a template statement can be the component class instance or the template. Because of this, template statements cannot refer to anything in the global namespace such as `window` or `document`. **For example, template statements can't call `console.log()` or `Math.max()`.**

# Binding
In an Angular template, a binding creates a live connection between a part of the UI created from a template (a DOM element, directive, or component) and the model (the component instance to which the template belongs).

This connection can be used to synchronize the view with the model, to notify the model when an event or user action takes place in the view, or both. 

Angular's [Change Detection](https://angular.io/guide/change-detection) algorithm is responsible for keeping the view and the model in sync.

## Property binding
Property binding in Angular helps you set values for properties of HTML elements or directives. 

Use property binding to do things such as toggle button functionality, set paths programmatically, and share values between components.

**==Property binding moves a value in one direction==, from a component's property into a target element property.**

To bind to an element's property, enclose it in square brackets, `[]`, which identifies the property as a target property.

```html
<img alt="item" [src]="itemImageUrl">
```

Without the brackets, Angular treats the right-hand side as a string literal and sets the property to that static value.

**To set a property of a directive, type the following:**

src/app/app.component.html

```html
<p [ngClass]="classes">ngClass binding to the classes property making this blue</p>
```

**To set the model property of a custom component for parent and child components to communicated, type the following:**

src/app/app.component.html

```html
<app-item-detail [childItem]="parentItem"></app-item-detail>
```

## Event binding
Event binding lets you listen for and respond to user actions such as keystrokes, mouse movements, clicks, and touches.

To bind to an event you use the Angular event binding syntax. This syntax consists of a target event name within parentheses to the left of an equal sign, and a quoted template statement to the right.

Use `()` to bind events.

```html
<button (click)="onSave()">Save</button>
```

![Event binding|500](https://angular.io/generated/images/guide/template-syntax/syntax-diagram.svg)

To determine an event target, Angular checks if the name of the target event matches an event property of a known directive.

## Two-way binding
Two-way binding gives components in your application a way to share data. **Use two-way binding to listen for events and update values simultaneously between parent and child components.**

Angular's two-way binding syntax is a combination of square brackets and parentheses, `[()]`. The `[()]` syntax combines the brackets of property binding, `[]`, with the parentheses of event binding, `()`, as follows.

`[()]` syntax is also known as 'banana-in-a-box syntax'.

```html
<app-sizer [(size)]="fontSizePx"></app-sizer>
```

For two-way data binding to work, the `@Output` property must use the pattern, `inputChange`, where `input` is the name of the `@Input()` property. For example, if the `@Input()` property is `size`, the `@Output()` property must be `sizeChange`.

The following `sizerComponent` has a `size` value property and a `sizeChange` event. The `size` property is an `@Input()`, so data can flow into the `sizerComponent`. The `sizeChange` event is an `@Output()`, which lets data flow out of the `sizerComponent` to the parent component.

### Example
src/app/sizer.component.ts
```typescript
export class SizerComponent {

  @Input()  size!: number | string;
  @Output() sizeChange = new EventEmitter<number>();

  dec() { this.resize(-1); }
  inc() { this.resize(+1); }

  resize(delta: number) {
    this.size = Math.min(40, Math.max(8, +this.size + delta));
    this.sizeChange.emit(this.size);
  }
}
```

src/app/sizer.component.html
```html
<div>
  <button type="button" (click)="dec()" title="smaller">-</button>
  <button type="button" (click)="inc()" title="bigger">+</button>
  <span [style.font-size.px]="size">FontSize: {{size}}px</span>
</div>
```

In the `AppComponent` template, `fontSizePx` is two-way bound to the `SizerComponent`.

src/app/app.component.html
```html
<app-sizer [(size)]="fontSizePx"></app-sizer>
<div [style.font-size.px]="fontSizePx">Resizable Text</div>
```

src/app/app.component.ts
```typescript
fontSizePx = 16;
```

### NgModel
Because no built-in HTML element follows the `x` value and `xChange` event pattern, two-way binding with form elements requires `NgModel`.

`[(ngModel)]`

```typescript
import {Component} from '@angular/core';

@Component({
  selector: 'example-app',
  template: `
    <input [(ngModel)]="name" #ctrl="ngModel" required>

    <p>Value: {{ name }}</p>
    <p>Valid: {{ ctrl.valid }}</p>

    <button (click)="setValue()">Set value</button>
  `,
})
export class SimpleNgModelComp {
  name: string = '';

  setValue() {
    this.name = 'Nancy';
  }
}
```

#### Displaying and updating properties with `NgModel`
1. Import [FormsModule](https://angular.io/api/forms/FormsModule) and add it to the NgModule's `imports` list.
	```typescript
	import { FormsModule } from '@angular/forms'; // <--- JavaScript import from Angular
	/* . . . */
	@NgModule({
	  /* . . . */
	
	  imports: [
	    BrowserModule,
	    FormsModule // <--- import into the NgModule
	  ],
	  /* . . . */
	})
	export class AppModule { }
	```
2. Add an `[(ngModel)]` binding on an HTML `<form>` element and set it equal to the property, here `name`.
	```html
	<label for="example-ngModel">[(ngModel)]:</label>
	<input [(ngModel)]="currentItem.name" id="example-ngModel">
	```

## Attribute binding
Attribute binding in Angular helps you set values for attributes directly. With attribute binding, you can improve accessibility, style your application dynamically, and manage multiple CSS classes or styles simultaneously.

```html
<p [attr.attribute-you-are-targeting]="expression"></p>

%%Example%%
<!-- create and set an aria attribute for assistive technology -->
<button type="button" [attr.aria-label]="actionName">{{actionName}} with Aria</button>

<!--  expression calculates colspan=2 -->
<tr><td [attr.colspan]="1 + 1">One-Two</td></tr>
```

## Class and style binding
Use class and style bindings to add and remove CSS class names from an element's `class` attribute and to set styles dynamically.

To bind to multiple classes, type the following:

`[class]="classExpression"`

The expression can be one of:

-   A space-delimited string of class names.
-   An object with class names as the keys and truthy or falsy expressions as the values.
-   An array of class names.

### Class binding
| Binding Type         | Syntax                    | Input Type                                       | Example Input Values               |
|----------------------|---------------------------|--------------------------------------------------|------------------------------------|
| Single class binding | [class.sale]="onSale"     | boolean | undefined | null                       | true, false                        |
| Multi-class binding  | [class]="classExpression" | string                                           | "my-class-1 my-class-2 my-class-3" |
| Multi-class binding  | [class]="classExpression" | Record&lt;string, boolean | undefined | null&gt; | {foo: true, bar: false}            |
| Multi-class binding  | [class]="classExpression" | Array&lt;string&gt;                              | ['foo', 'bar']                     |

### Style binding
| Binding Type                    | Syntax                    | Input Type                                      | Example Input Values              |
|---------------------------------|---------------------------|-------------------------------------------------|-----------------------------------|
| Single style binding            | [style.width]="width"     | string | undefined | null                       | "100px"                           |
| Single style binding with units | [style.width.px]="width"  | number | undefined | null                       | 100                               |
| Multi-style binding             | [style]="styleExpression" | string                                          | "width: 100px; height: 100px"     |
| Multi-style binding             | [style]="styleExpression" | Record&lt;string, string | undefined | null&gt; | {width: '100px', height: '100px'} |

# Pipes
Use [pipes](https://angular.io/guide/glossary#pipe "Definition of a pipe") to transform strings, currency amounts, dates, and other data for display.

Pipes are simple functions to use in [template expressions](https://angular.io/guide/glossary#template-expression "Definition of template expression") to accept an input value and return a transformed value.

## Built-in pipes
-   [`DatePipe`](https://angular.io/api/common/DatePipe): Formats a date value according to locale rules.
-   [`UpperCasePipe`](https://angular.io/api/common/UpperCasePipe): Transforms text to all upper case.
-   [`LowerCasePipe`](https://angular.io/api/common/LowerCasePipe): Transforms text to all lower case.
-   [`CurrencyPipe`](https://angular.io/api/common/CurrencyPipe): Transforms a number to a currency string, formatted according to locale rules.
-   [`DecimalPipe`](https://angular.io/api/common/DecimalPipe): Transforms a number into a string with a decimal point, formatted according to locale rules.
-   [`PercentPipe`](https://angular.io/api/common/PercentPipe): Transforms a number to a percentage string, formatted according to locale rules.

## Example
src/app/app.component.html
```html
<p>The hero's birthday is {{ birthday | date }}</p>
```

src/app/hero-birthday1.component.ts
```typescript
import { Component } from '@angular/core';

@Component({
  selector: 'app-hero-birthday',
  template: "<p>The hero's birthday is {{ birthday | date }}</p>"
})
export class HeroBirthdayComponent {
  birthday = new Date(1988, 3, 15); // April 15, 1988 -- since month parameter is zero-based
}
```

# Template Variables
Template variables help you use data from one part of a template in another part of the template. Use template variables to perform tasks such as respond to user input or finely tune your application's forms.

## Syntax
```html
<input #phone placeholder="phone number" />

<!-- lots of other elements -->

<!-- phone refers to the input element; pass its `value` to an event handler -->
<button type="button" (click)="callPhone(phone.value)">Call</button>
```

## How Angular assigns values to template variables
Angular assigns a template variable a value based on where you declare the variable:

-   If you declare the variable on a component, the variable refers to the component instance.
-   If you declare the variable on a standard HTML tag, the variable refers to the element.
-   If you declare the variable on an `<ng-template>` element, the variable refers to a `TemplateRef` instance which represents the template. For more information on `<ng-template>`, see [How Angular uses the asterisk, `*`, syntax](https://angular.io/guide/structural-directives#asterisk) in [Structural directives](https://angular.io/guide/structural-directives).

## Variable specifying a name
If the variable specifies a name on the right-hand side, such as `#var="ngModel"`, the variable refers to the directive or component on the element with a matching `exportAs` name.

## Template input variable
A _template input variable_ is a variable with a value that is set when an instance of that template is created.

Template input variables can be seen in action in the long-form usage of `NgFor`:

```html
<ul>
	<ng-template ngFor let-hero [ngForOf]="heroes">
		<li>{{hero.name}}
	</ng-template> 
</ul>
```

The `NgFor` directive will instantiate this once for each hero in the `heroes` array, and will set the `hero` variable for each instance accordingly.
The right-hand side of the `let-` declaration of an input variable can specify which value should be used for that variable.
`NgFor` for example also provides access to the `index` of each hero in the array:
```html
<ul>
  <ng-template ngFor let-hero let-i="index" [ngForOf]="heroes">
    <li>Hero number {{i}}: {{hero.name}}
  </ng-template>
</ul>
```
