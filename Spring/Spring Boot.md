Spring Boot helps you to create stand-alone, production-grade Spring-based applications that you can run. We take an opinionated view of the Spring platform and third-party libraries, so that you can get started with minimum fuss. Most Spring Boot applications need very little Spring configuration.

Benefits:
- Provide a radically faster and widely accessible **getting-started experience** for all Spring development.
- Be **opinionated** out of the box but get out of the way quickly as requirements start to diverge from the defaults.
- Provide a **range of non-functional features** that are common to large classes of projects (such as embedded servers, security, metrics, health checks, and externalized configuration).
- Absolutely **no code generation** and no requirement for XML configuration.

# Auto-configuration

Detects the jars available in the class path and provides configuration. 
We should use `@EnableAutoConfiguration` or `@SpringBootApplication`

To enable spring logging to see the auto configuration logs, add
`logging.level.org.springframework:
DEBUG`  to application.properties.

# @SpringBootApplication

SpringBootApplication annotation can be used to enable those three features, that is:
- `@EnableAutoConfiguration`: enable Spring Boot’s auto-configuration mechanism
- `@ComponentScan`: enable @Component scan on the package where the application is located
- `@SpringBootConfiguration`: enable registration of extra beans in the context or the import of additional configuration classes.

```java
@SpringBootConfiguration
@EnableAutoConfiguration
@ComponentScan
public @interface SpringBootApplication {
```

Example starting program: 
```java
@SpringBootApplication
public class MyApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyApplication.class, args);
    }

}
```

## Running a spring boot application

1. Running as a Packaged Application
	> **java -jar target/myapplication-0.0.1-SNAPSHOT.jar**
2. Using the Maven Plugin
	> **mvn spring-boot:run**

## Embedded Servers

Tomcat, Jetty or UnderTow

Default is Tomcat. 

# Starters

> Starters are a set of convenient dependency descriptors that you can include in your application. 

> All **official** starters follow a **similar naming pattern; spring-boot-starter-***, where * is a particular type of application.

## spring-boot-starter-parent

Core starter, including auto-configuration support, logging and YAML. 

Parent of all the starter projects. 

Has the java versions, etc. 

==It has a parent called spring-boot-dependencies.== 

### Spring-boot-dependencies

The curated list contains all the Spring modules that you can use with Spring Boot as well as a refined list of third party libraries. 

It is list of dependencies with compatible versions that play well together with spring. 

# Configuration

Spring Boot uses `application.properties` file for configuration. 

For different profiles, different properties files can be created. (**application-{profile}.properties**)
E.g: 
 application-live.properties
 application-prod.properties
 
 Use `@Value` to access the properties.
 E.g:
 ```java
@Component
public class MyBean {

    @Value("${name}")
    private String name;

    // ...

}
```

## @ConfigurationProperties

Using the @Value("${property}") annotation to inject configuration properties can sometimes be cumbersome, especially if you are working with multiple properties or your data is hierarchical in nature. Spring Boot provides an alternative method of working with properties that lets **strongly typed beans** govern and validate the configuration of your application.

==@ConfigurationProperties provides type safety==

E.g: 
```java
@ConfigurationProperties("my.service")
public class MyProperties {

    private boolean enabled;

    private InetAddress remoteAddress;

    private final Security security = new Security();

    // getters / setters...

    public static class Security {

        private String username;

        private String password;

        private List<String> roles = new ArrayList<>(Collections.singleton("USER"));

        // getters / setters...

    }
}
```

The preceding POJO defines the following properties:

my.service.enabled, with a value of false by default.

my.service.remote-address, with a type that can be coerced from String.

my.service.security.username, with a nested "security" object whose name is determined by the name of the property. In particular, the type is not used at all there and could have been SecurityProperties.

my.service.security.password.

my.service.security.roles, with a collection of String that defaults to USER.

E.g. YAML properties file
```YAML
my:
  service:
    remote-address: 192.168.1.1
    security:
      username: "admin"
      roles:
      - "USER"
      - "ADMIN"
```

The `MyProperties` bean will read the properties in application.properties and map them to the respective fields. 

==@ConfigurationProperties is recommended way of reading the properties.==

# Profiles

Any `@Component`, `@Configuration` or `@ConfigurationProperties` can be marked with **`@Profile`** to limit when it is loaded, as shown in the following example:
```java
@Configuration(proxyBeanMethods = false)
@Profile("production")
public class ProductionConfiguration {

    // ...

}
```


We can activate the profiles from application.properties as follows:
```
spring.profiles.active=dev,hsqldb
```

Or through **command line** with **--spring.profiles.active=dev,hsqldb**

## Profile Groups

```
spring.profiles.group.production[0]=proddb
spring.profiles.group.production[1]=prodmq
```

## Programmatically Setting Profiles

```java
SpringApplication.setAdditionalProfiles(…​)
```

## Profile Specific Files

As well as application property files, Spring Boot will also attempt to load profile-specific files using the naming convention **application-{profile}**.

If several profiles are specified, **a last-wins strategy** applies.

## Importing Additional Data

Application properties may import further config data from other locations using the **==spring.config.import== property**. Imports are processed as they are discovered, and are treated as additional documents inserted immediately below the one that declares the import.

E.g: *application.properties*
```
spring.application.name=myapp
spring.config.import=optional:file:./dev.properties
```

This will trigger the import of a dev.properties file in current directory (if such a file exists). **Values from the imported dev.properties will take precedence over the file that triggered the import. In the above example, the dev.properties could redefine spring.application.name to a different value.**

# Actuator

Actuator helps in Auditing, health, and metrics gathering.

> Definition of Actuator
> An actuator is a manufacturing term that refers to a mechanical device for moving or controlling something. Actuators can generate a large amount of motion from a small change.

To add the actuator to a Maven-based project, add the following ‘Starter’ dependency:
```maven
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-actuator</artifactId>
    </dependency>
</dependencies>
```

# Using the ApplicationRunner or CommandLineRunner

If you need to run some specific code once the SpringApplication has started, you can implement the **ApplicationRunner** or **CommandLineRunner** interfaces. **Both interfaces work in the same way and offer a single run method**, which is called just before SpringApplication.run(…​) completes.

The `CommandLineRunner` interfaces ==provides access to application arguments as a string array==, whereas the `ApplicationRunner` ==uses the ApplicationArguments interface==.

If several CommandLineRunner's or ApplicationRunner's are defined then we can order them using `@Order`(org.springframework.core.annotation.Order) or by implementing ordered interface. 	

CommandLineRunner Example
```java
@Component
public class MyCommandLineRunner implements CommandLineRunner {

    @Override
    public void run(String... args) {
        // Do something...
    }

}
```

ApplicationRunner example
```java
@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger LOG =
      LoggerFactory.getLogger(AppStartupRunner.class);

    public static int counter;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOG.info("Application started with option names : {}", 
          args.getOptionNames());
        LOG.info("Increment counter");
        counter++;
    }
}
```

`ApplicationRunner` parses the given arguments and puts them in an instance of `ApplicationArguments`, where as command line provides the arguments as raw strings. 