# NgModule
The `@NgModule()` decorator is a function that takes a single metadata object, whose properties describe the module. The most important properties are as follows.

| Properties   | Details                                                                                                                                                                                                    |
|--------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| declarations | The components, directives, and pipes that belong to this NgModule.                                                                                                                                        |
| exports      | The subset of declarations that should be visible and usable in the component templates of other NgModules.                                                                                                |
| imports      | Other modules whose exported classes are needed by component templates declared in this NgModule.                                                                                                          |
| providers    | Creators of services that this NgModule contributes to the global collection of services; they become accessible in all parts of the application. (You can also specify providers at the component level.) |
| bootstrap    | The main application view, called the root component, which hosts all other application views. Only the root NgModule should set the bootstrap property.                                                   |

src/app/app.module.ts
```ts
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
@NgModule({
  imports:      [ BrowserModule ],
  providers:    [ Logger ],
  declarations: [ AppComponent ],
  exports:      [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
```

![Angular Libraries](https://angular.io/generated/images/guide/architecture/library-module.png)

NgModule metadata does the following:

-   Declares which components, directives, and pipes belong to the module
-   Makes some of those components, directives, and pipes public so that other module's component templates can use them
-   Imports other modules with the components, directives, and pipes that components in the current module need
-   Provides services that other application components can use

