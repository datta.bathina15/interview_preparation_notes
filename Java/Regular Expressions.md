Regular expressions are a way to describe a set of strings based on common characteristics shared by each string in the set. 

The java.util.regex package primarily consists of three classes: `Pattern`, `Matcher`, and `PatternSyntaxException`.
- A `Pattern` object is a compiled representation of a regular expression. **The Pattern class provides no public constructors. To create a pattern, you must first invoke one of its public static compile methods, which will then return a Pattern object.** These methods accept a regular expression as the first argument; the first few lessons of this trail will teach you the required syntax.
- A `Matcher` object is the engine that interprets the pattern and performs match operations against an input string. Like the Pattern class, Matcher defines no public constructors. **You obtain a Matcher object by invoking the matcher method on a Pattern object.**
- A `PatternSyntaxException` object is an unchecked exception that indicates a syntax error in a regular expression pattern.

```java
Pattern p = Pattern.compile("a*b");
Matcher m = p.matcher("aaaaab");
boolean b = m.matches();
```

## Differences Among Greedy, Reluctant, and Possessive Quantifiers

| Greedy | Reluctant | Possessive | Meaning                                 |
|--------|-----------|------------|-----------------------------------------|
| X?     | X??       | X?+        | X, once or not at all                   |
| X*     | X*?       | X*+        | X, zero or more times                   |
| X+     | X+?       | X++        | X, one or more times                    |
| X{n}   | X{n}?     | X{n}+      | X, exactly n times                      |
| X{n,}  | X{n,}?    | X{n,}+     | X, at least n times                     |
| X{n,m} | X{n,m}?   | X{n,m}+    | X, at least n but not more than m times |

**Greedy quantifiers** are considered "greedy" because **they force the matcher to read in, or eat, the entire input string prior to attempting the first match. If the first match attempt (the entire input string) fails, the matcher backs off the input string by one character and tries again, repeating the process until a match is found or there are no more characters left to back off from.** Depending on the quantifier used in the expression, the last thing it will try matching against is 1 or 0 characters.

**The reluctant quantifiers**, however, take the opposite approach: T**hey start at the beginning of the input string, then reluctantly eat one character at a time looking for a match. The last thing they try is the entire input string.**

Finally, **the possessive quantifiers** **always eat the entire input string, trying once (and only once) for a match.** Unlike the greedy quantifiers, possessive quantifiers never back off, even if doing so would allow the overall match to succeed.

## Capturing Groups

Capturing groups are a way to treat multiple characters as a single unit. They are created by placing the characters to be grouped inside a set of parentheses. 
For example, the regular expression (dog) creates a single group containing the letters "d" "o" and "g". 

### Numbering

Capturing groups are numbered by counting their opening parentheses from left to right. In the expression ((A)(B(C))), for example, there are four such groups:
1. ((A)(B(C)))
2. (A)
3. (B(C))
4. (C)

To find out how many groups are present in the expression, call the `groupCount` method on a matcher object.

==There is also a special group, group 0, which always represents the entire expression. This group is not included in the total reported by groupCount.==

## Backreferences

==The section of the input string matching the capturing group(s) is saved in memory for later recall via backreference.== A backreference is specified in the regular expression as a backslash (\) followed by a digit indicating the number of the group to be recalled. 
For example, the expression (\d\d) defines one capturing group matching two digits in a row, which can be recalled later in the expression via the backreference \1.

**Example:**

- To match any 2 digits, followed by the exact same two digits, you would use (\d\d)\1 as the regular expression:
	> Enter your regex: **(\d\d)\1**
	> Enter input string to search: 1212
	> I found the text "1212" starting at index 0 and ending at index 4.

- If you change the last two digits the match will fail:
	> Enter your regex: **(\d\d)\1**
	> Enter input string to search: 1234
	> No match found.

# Difference Between Java Matcher find() and matches()
## The _find()_ Method
The _find()_ method tries to **find the occurrence of a regex pattern within a given string**.
```java
@Test
public void whenFindFourDigitWorks_thenCorrect() {
    Pattern stringPattern = Pattern.compile("\\d\\d\\d\\d");
    Matcher m = stringPattern.matcher("goodbye 2019 and welcome 2020");

    assertTrue(m.find());
    assertEquals(8, m.start());
    assertEquals("2019", m.group());
    assertEquals(12, m.end());
    
    assertTrue(m.find());
    assertEquals(25, m.start());
    assertEquals("2020", m.group());
    assertEquals(29, m.end());
    
    assertFalse(m.find());
}
```

## The _matches()_ Method
The _matches()_ method tries to **match the whole string against the pattern**.
```java
@Test
public void whenMatchFourDigitWorks_thenFail() {
    Pattern stringPattern = Pattern.compile("\\d\\d\\d\\d");
    Matcher m = stringPattern.matcher("goodbye 2019 and welcome 2020");
 
    assertFalse(m.matches());
}
```

>[!INFO]
>_matches()_ will return _false_ because it will try to match _“\\d\\d\\d\\d”_ against the whole string “_goodbye 2019 and welcome 2020”_ — **unlike the _find()_ and _find(int)_ methods, both of which will find the occurrence of the pattern anywhere within the string**.
