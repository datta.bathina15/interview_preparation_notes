In the Java programming language, an _interface_ is a reference type, similar to a class, that can contain _only_ constants, method signatures, default methods, static methods, and nested types. 

Method bodies exist only for default methods and static methods. 

Interfaces cannot be instantiated—they can only be _implemented_ by classes or _extended_ by other interfaces.

```java
public interface OperateCar {

   // constant declarations, if any

   // method signatures
   
   // An enum with values RIGHT, LEFT
   int turn(Direction direction,
            double radius,
            double startSpeed,
            double endSpeed);
   int changeLanes(Direction direction,
                   double startSpeed,
                   double endSpeed);
   int signalTurn(Direction direction,
                  boolean signalOn);
   int getRadarFront(double distanceToCar,
                     double speedOfCar);
   int getRadarRear(double distanceToCar,
                    double speedOfCar);
         ......
   // more method signatures
}
```

# Interface Body
The interface body can contain [abstract methods](https://docs.oracle.com/javase/tutorial/java/IandI/abstract.html), [default methods](https://docs.oracle.com/javase/tutorial/java/IandI/defaultmethods.html), and [static methods](https://docs.oracle.com/javase/tutorial/java/IandI/defaultmethods.html#static).

All abstract, default, and static methods in an interface are implicitly `public`, so you can omit the `public` modifier.

## Abstract Methods
An abstract method within an interface is followed by a semicolon, but no braces (an abstract method does not contain an implementation).

```java
int changeLanes(Direction direction, double startSpeed, double endSpeed);
```

The `abstract` keyword is optional.

## Default Methods
Default methods enable you to add new functionality to the interfaces of your libraries and ensure binary compatibility with code written for older versions of those interfaces.

Use the `default` keyword to define a default method.
```java
public interface TimeClient {
    void setTime(int hour, int minute, int second);
    void setDate(int day, int month, int year);
    void setDateAndTime(int day, int month, int year,
                               int hour, int minute, int second);

	// Default method example
    default ZonedDateTime getZonedDateTime(String zoneString) {
        return ZonedDateTime.of(getLocalDateTime(), getZoneId(zoneString));
    }
}
```

When you extend an interface that contains a default method, you can do the following:
- Not mention the default method at all, which lets your extended interface inherit the default method.
- Redeclare the default method, which makes it `abstract`.
- Redefine the default method, which overrides it.

## Static Methods
Use `static` keyword to declare a static method.
```java
public interface TimeClient {
    // ...
    static public ZoneId getZoneId (String zoneString) {
        try {
            return ZoneId.of(zoneString);
        } catch (DateTimeException e) {
            System.err.println("Invalid time zone: " + zoneString +
                "; using default time zone instead.");
            return ZoneId.systemDefault();
        }
    }

    default public ZonedDateTime getZonedDateTime(String zoneString) {
        return ZonedDateTime.of(getLocalDateTime(), getZoneId(zoneString));
    }    
}
```

Just like static methods in classes, the static methods is shared by all instances of the interface.

## Constants 
Interface face can contains constans as well.

All constant values defined in an interface are implicitly `public`, `static`, and `final`. Once again, you can omit these modifiers.

# When to use Abstract classes and Interfaces
- **Consider using abstract classes if any of these statements apply to your situation:**
	- You want to share code among several closely related classes.
	- You expect that classes that extend your abstract class have many common methods or fields, or require access modifiers other than public (such as protected and private).
	- You want to declare non-static or non-final fields. This enables you to define methods that can access and modify the state of the object to which they belong.

- **Consider using interfaces if any of these statements apply to your situation:**
	- You expect that unrelated classes would implement your interface. For example, the interfaces [`Comparable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Comparable.html) and [`Cloneable`](https://docs.oracle.com/javase/8/docs/api/java/lang/Cloneable.html) are implemented by many unrelated classes.
	- You want to specify the behavior of a particular data type, but not concerned about who implements its behavior.
	- You want to take advantage of multiple inheritance of type.
