# Buit-in directives
The different types of Angular directives are as follows:

| Directive Types       | Details                                                                           |
|-----------------------|-----------------------------------------------------------------------------------|
| Components            | Used with a template. This type of directive is the most common directive type.   |
| Attribute directives  | Change the appearance or behavior of an element, component, or another directive. |
| Structural directives | Change the DOM layout by adding and removing DOM elements.                        |

## Built-in attribute directives
Attribute directives listen to and modify the behavior of other HTML elements, attributes, properties, and components.

| Common directives | Details                                            |
|-------------------|----------------------------------------------------|
| NgClass           | Adds and removes a set of CSS classes.             |
| NgStyle           | Adds and removes a set of HTML styles.             |
| NgModel           | Adds two-way data binding to an HTML form element. |

### NgClass
Add or remove multiple CSS classes simultaneously with `ngClass`.
```html
<!-- toggle the "special" class on/off with a property -->
<div [ngClass]="isSpecial ? 'special' : ''">This div is special</div>
```

### NgStyle
Use `NgStyle` to set multiple inline styles simultaneously, based on the state of the component.
```html
<some-element [ngStyle]="{'font-style': styleExp}">...</some-element>
```

### NgModel
[[Templates#NgModel|Check the NgModel section in templates]]

## Built-in structural directives
Structural directives are responsible for HTML layout. They shape or reshape the DOM's structure, typically by adding, removing, and manipulating the host elements to which they are attached.

| Common built-in structural directives | Details                                                          |
|---------------------------------------|------------------------------------------------------------------|
| NgIf                                  | Conditionally creates or disposes of subviews from the template. |
| NgFor                                 | Repeat a node for each item in a list.                           |
| NgSwitch                              | A set of directives that switch among alternative views.         |

### NgIf
Add or remove an element by applying an `NgIf` directive to a host element.
When `NgIf` is `false`, Angular removes an element and its descendants from the DOM. Angular then disposes of their components, which frees up memory and resources.
Example:
```html
<app-item-detail *ngIf="isActive" [item]="item"></app-item-detail>
```

Form with an "else" block:

```html
<div *ngIf="condition; else elseBlock">Content to render when condition is true.</div>
<ng-template #elseBlock>Content to render when condition is false.</ng-template>
```    

Shorthand form with "then" and "else" blocks:

```html
<div *ngIf="condition; then thenBlock else elseBlock"></div>
<ng-template #thenBlock>Content to render when condition is true.</ng-template>
<ng-template #elseBlock>Content to render when condition is false.</ng-template>
```

**By default, `NgIf` prevents display of an element bound to a null value.**

### NgFor
A [structural directive](https://angular.io/guide/structural-directives) that renders a template for each item in a collection. The directive is placed on an element, which becomes the parent of the cloned templates.
```html
<li *ngFor="let user of users; index as i; first as isFirst">
  {{i}}/{{users.length}}. {{user}} <span *ngIf="isFirst">default</span>
</li>
```

The following exported values can be aliased to local variables:
-   `$implicit: T`: The value of the individual items in the iterable (`ngForOf`).
-   `ngForOf: NgIterable<T>`: The value of the iterable expression. Useful when the expression is more complex then a property access, for example when using the async pipe (`userStreams | async`).
-   `index: number`: The index of the current item in the iterable.
-   `count: number`: The length of the iterable.
-   `first: boolean`: True when the item is the first item in the iterable.
-   `last: boolean`: True when the item is the last item in the iterable.
-   `even: boolean`: True when the item has an even index in the iterable.
-   `odd: boolean`: True when the item has an odd index in the iterable.

#### Tracking items with `*ngFor` `trackBy`
Reduce the number of calls your application makes to the server by tracking changes to an item list. With the `*ngFor` `trackBy` property, Angular can change and re-render only those items that have changed, rather than reloading the entire list of items.

1. Add a method to the component that returns the value `NgFor` should track. In this example, the value to track is the item's `id`. If the browser has already rendered `id`, Angular keeps track of it and doesn't re-query the server for the same `id`.

```typescript
trackByItems(index: number, item: Item): number { return item.id; }
```

2. In the short hand expression, set `trackBy` to the `trackByItems()` method.
   
```html
<div *ngFor="let item of items; trackBy: trackByItems">
  ({{item.id}}) {{item.name}}
</div>
```

### ng-container
The Angular `ng-container` is a grouping element that doesn't interfere with styles or layout because Angular doesn't put it in the DOM.

Use `ng-container` when there's no single element to host the directive.

```html
<p>
  I turned the corner
  <ng-container *ngIf="hero">
    and saw {{hero.name}}. I waved
  </ng-container>
  and continued on my way.
</p>
```

### NgSwitch
`NgSwitch` is a set of three directives:

| NgSwitch directives | Details                                                                                                                                                                |
|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [NgSwitch](https://angular.io/api/common/NgSwitch)            | An attribute directive that changes the behavior of its companion directives.                                                                                          |
| [NgSwitchCase](https://angular.io/api/common/NgSwitchCase)        | Structural directive that adds its element to the DOM when its bound value equals the switch value and removes its bound value when it doesn't equal the switch value. |
| [NgSwitchDefault](https://angular.io/api/common/NgSwitchDefault)     | Structural directive that adds its element to the DOM when there is no selected NgSwitchCase.                                                                          |

#### Syntax
```html
<container-element [ngSwitch]="switch_expression">
  <!-- the same view can be shown in more than one case -->
  <some-element *ngSwitchCase="match_expression_1">...</some-element>
  <some-element *ngSwitchCase="match_expression_2">...</some-element>
  <some-other-element *ngSwitchCase="match_expression_3">...</some-other-element>
  <!--default case when there are no matches -->
  <some-element *ngSwitchDefault>...</some-element>
</container-element>
```

#### Example
src/app/app.component.html
```html
<div [ngSwitch]="currentItem.feature">
  <app-stout-item    *ngSwitchCase="'stout'"    [item]="currentItem"></app-stout-item>
  <app-device-item   *ngSwitchCase="'slim'"     [item]="currentItem"></app-device-item>
  <app-lost-item     *ngSwitchCase="'vintage'"  [item]="currentItem"></app-lost-item>
  <app-best-item     *ngSwitchCase="'bright'"   [item]="currentItem"></app-best-item>
<!-- . . . -->
  <app-unknown-item  *ngSwitchDefault           [item]="currentItem"></app-unknown-item>
</div>
```

src/app/app.component.ts
```typescript
currentItem!: Item;
```

# Attribute Directive
## Building an attribute directive
Use the CLI command [`ng generate directive`](https://angular.io/cli/generate).
```bash
ng generate directive highlight
```

The CLI creates `src/app/highlight.directive.ts`, a corresponding test file `src/app/highlight.directive.spec.ts`, and declares the directive class in the `AppModule`.

src/app/highlight.directive.ts
```ts
import { Directive } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor() { }
}
```

### ElementRef
-   Import `ElementRef` from `@angular/core`. `ElementRef` grants direct access to the host DOM element through its `nativeElement` property. 
-   Add `ElementRef` in the directive's `constructor()` to [inject](https://angular.io/guide/dependency-injection) a reference to the host DOM element, the element to which you apply `appHighlight`.

src/app/highlight.directive.ts
```ts
import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
    constructor(private el: ElementRef) {
       this.el.nativeElement.style.backgroundColor = 'yellow';
    }
}
```

## Applying an attribute directive
src/app/app.component.html
```html
<p appHighlight>Highlight me!</p>
```

## Handling User events
Use [HostListener](https://angular.io/api/core/HostListener) to handle user events through directives.
```ts
@HostListener('mouseenter') onMouseEnter() {
  this.highlight('yellow');
}

@HostListener('mouseleave') onMouseLeave() {
  this.highlight('');
}

private highlight(color: string) {
  this.el.nativeElement.style.backgroundColor = color;
}
```

**Complete example:**
```ts
import { Directive, ElementRef, HostListener } from '@angular/core';
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {

  constructor(private el: ElementRef) { }

  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight('');
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

}
```

## Passing values into an attribute directive
Use [[Components#Parent to child|@Input()]] to pass data to the directive.

src/app/highlight.directive.ts
```ts
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Input() appHighlight = '';
```

src/app/app.component.ts
```ts
export class AppComponent {
  color = 'yellow';
}
```

src/app/app.component.html
```html
<p [appHighlight]="color">Highlight me!</p>
```

The `[appHighlight]` attribute binding performs two tasks:
-   Applies the highlighting directive to the `<p>` element
-   Sets the directive's highlight color with a property binding

## Deactivating Angular processing with `NgNonBindable`
To prevent expression evaluation in the browser, add `ngNonBindable` to the host element.

```html
<p>Use ngNonBindable to stop evaluation.</p>
<p ngNonBindable>This should not evaluate: {{ 1 + 1 }}</p>
```
Applying `ngNonBindable` to an element stops binding for that element's child elements. **However, `ngNonBindable` still lets directives work on the element where you apply `ngNonBindable`.**

# Structural Directives
See [[Directives#Built-in structural directives]] for details regarding NgIf, NgFor etc.

## Structural directive shorthand
When structural directives are applied they generally are prefixed by an asterisk, `*`, such as `*ngIf`. This convention is shorthand that Angular interprets and converts into a longer form.

```html
<!-- Shorthand -->
<div
  *ngFor="let hero of heroes; let i=index; let odd=odd; trackBy: trackById"
  [class.odd]="odd">
  ({{i}}) {{hero.name}}
</div>
<!-- Longhand -->
<ng-template ngFor let-hero [ngForOf]="heroes"
  let-i="index" let-odd="odd" [ngForTrackBy]="trackById">
  <div [class.odd]="odd">
    ({{i}}) {{hero.name}}
  </div>
</ng-template>
```

## One structural directive per element
Only one structural directive per element. Angular compiler will throw an error if multiple structural directives are found. 