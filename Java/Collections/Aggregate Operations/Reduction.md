# Reduce

> The Stream.reduce method is a general-purpose reduction operation.

The reduce operation in this example takes two arguments:

* **identity**: The identity element is both the initial value of the reduction and the default result if there are no elements in the stream. In this example, the identity element is 0; this is the initial value of the sum of ages and the default value if no members exist in the collection roster.
* **accumulator**: The accumulator function takes two parameters: a partial result of the reduction (in this example, the sum of all processed integers so far) and the next element of the stream (in this example, an integer). It returns a new partial result. In this example, the accumulator function is a lambda expression that adds two Integer values and returns an Integer value:
```java
(a, b) -> a + b
```

E.g:
```java
Integer totalAgeReduce = roster
   .stream()
   .map(Person::getAge)
   .reduce(
       0,
       (a, b) -> a + b);
```

# Collect

> Unlike the reduce method, which always creates a new value when it processes an element, the collect method modifies, or mutates, an existing value.

The collect operation is best suited for collections. The following example puts the names of the male members in a collection with the collect operation:

```java
List<String> namesOfMaleMembersCollect = roster
    .stream()
    .filter(p -> p.getGender() == Person.Sex.MALE)
    .map(p -> p.getName())
    .collect(Collectors.toList());
```

The collect operation in this example takes three arguments:
* **supplier**: The supplier is a factory function; it constructs new instances. For the collect operation, it creates instances of the result container.
* **accumulator**: The accumulator function incorporates a stream element into a result container.
* **combiner**: The combiner function takes two result containers and merges their contents. 

