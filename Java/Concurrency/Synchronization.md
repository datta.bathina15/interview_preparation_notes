# Synchronization

==Threads communicate by sharing access to fields and the objects reference fields refer to.== This form of communication is extremely efficient, but makes two kinds of errors possible: **thread interference** and **memory consistency errors**. The tool needed to prevent these errors is ***synchronization***. 

However, **synchronization can introduce thread contention**, which occurs when two or more threads try to access the same resource simultaneously and cause the Java runtime to execute one or more threads more slowly, or even suspend their execution. **Starvation and livelock are forms of thread contention.**

## Thread Interference

Interference happens when two operations, running in different threads, but acting on the same data, interleave. This means that the two operations consist of multiple steps, and the sequences of steps overlap.

## Memory Consistency Errors

Memory consistency error occur when different threads have incosistent views of what should be same data. 

## Synchronized methods

Two forms:
* Synchronized methods
* Synchronized statements

```java
public class SynchronizedCounter {
    private int c = 0;

    public synchronized void increment() {
        c++;
    }

    public synchronized void decrement() {
        c--;
    }

    public synchronized int value() {
        return c;
    }
}
```

Making methods synchronized has two effects:
* First it is not possible for two invocations of the synchronized methods on the same object to inteleave (overlap). When one thread is executing a synchronized method for an object, all other threads that invoke synchronized methods for the same object block (suspend execution) until the first thread is done with the object.
* Second, when a synchronized method exits, it automatically establishes a **happens-before** relationship with any subsequent invocation of a synchronized method for the same object. This guarantees that changes to the state of the object are visible to all threads.

==Note that constructors cannot be synchronized — using the synchronized keyword with a constructor is a syntax error. Synchronizing constructors doesn't make sense, because only the thread that creates an object should have access to it while it is being constructed.==

> Synchronized methods enable a simple strategy for preventing thread interference and memory consistency errors: if an object is visible to more than one thread, all reads or writes to that object's variables are done through synchronized methods. (An important exception: final fields, which cannot be modified after the object is constructed, can be safely read through non-synchronized methods, once the object is constructed) This strategy is effective, but can present problems with liveness, as we'll see later in this lesson.

## Intrinsic Locks and Synchronization

> Synchronization is built around an internal entity known as the intrinsic lock or monitor lock. (The API specification often refers to this entity simply as a "monitor.") Intrinsic locks play a role in both aspects of synchronization: enforcing exclusive access to an object's state and establishing happens-before relationships that are essential to visibility.

Every object has an intrinsic lock associated with it.

When a thread needs exclusive and consisten acces to a object's fields, it has to acquire the object's intrinsic lock before accessing them. And it has to relase the lock after it's done with it. 

> A thread is said to own the intrinsic lock between the time it has acquired the lock and released the lock. As long as a thread owns an intrinsic lock, no other thread can acquire the same lock. The other thread will block when it attempts to acquire the lock.

> When a thread releases an intrinsic lock, a happens-before relationship is established between that action and any subsequent acquisition of the same lock.

### Locks In Synchronized Methods

> When a thread invokes a synchronized method, it automatically acquires the intrinsic lock for that method's object and releases it when the method returns. The lock release occurs even if the return was caused by an uncaught exception.

For synchronized static methods, the intrinsic lock of the `Class` object is obtained. Thus access to class's static fields is controlled by a lock that's distinct from the lock for any instance of the class.

Class locks are different from object locks.

A lock is a tool for controlling access to a shared resource by multiple threads. Commonly, a lock provides exclusive access to a shared resource: only one thread at a time can acquire the lock and all access to the shared resource requires that the lock be acquired first.

### Synchronized Statements

E.g. 
```java
public void addName(String name) {
    synchronized(this) {
        lastName = name;
        nameCount++;
    }
    nameList.add(name);
}
```

In this example, the addName method needs to synchronize changes to lastName and nameCount, **but also needs to avoid synchronizing invocations of other objects' methods**.

Unlike synchronized methods, synchronized statements must specify the object that provides the intrinsic lock.

E.g. Synchronization with interleaving example
```java
public class MsLunch {
    private long c1 = 0;
    private long c2 = 0;
    private Object lock1 = new Object();
    private Object lock2 = new Object();

    public void inc1() {
        synchronized(lock1) {
            c1++;
        }
    }

    public void inc2() {
        synchronized(lock2) {
            c2++;
        }
    }
}
```

### Reentrant Synchronization

Re-acquiring the same lock owned by a thread is called Reentrant synchronization. 

> A thread cannot acquire a lock owned by another thread. But a thread can acquire a lock that it already owns. **Allowing a thread to acquire the same lock more than once enables reentrant synchronization.** This describes a situation where synchronized code, directly or indirectly, invokes a method that also contains synchronized code, and both sets of code use the same lock. Without reentrant synchronization, synchronized code would have to take many additional precautions to avoid having a thread cause itself to block.

## Atomic Access

**Atomic action**: Atomic action is an action that either happens completely or doesn't happen at all. It cannot stop in the middle. 

All the side effects happening during the action are visible only once the action is complete. 

There are actions you can specify that are atomic:
* Reads and writes are atomic for reference variables and for most primitive variables (all types except long and double).
* Reads and writes are atomic for all variables declared `volatile` (including long and double variables).

Atomic actions cannot be interleaved, so thread interference is avoided. However, memory consistency errors are still possible. 

> Using volatile variables reduces the risk of memory consistency errors, because any write to a volatile variable establishes a happens-before relationship with subsequent reads of that same variable. This means that changes to a volatile variable are always visible to other threads. What's more, it also means that when a thread reads a volatile variable, it sees not just the latest change to the volatile, but also the side effects of the code that led up the change.

# Liveness

A concurrent application's ability to execute in a timely manner is known as its liveness. 

## Deadlock

Deadlock describes a situation where two or more threads are blocked forever, waiting for each other.

## Starvation

Starvation describes a situation where a thread is unable to gain regular access to shared resources and is unable to make progress. This happens when shared resources are made unavailable for long periods by "greedy" threads. 

## Livelock

Similar to deadlock but the threads are not blocked. Threars get busy responding to other thread's actions. 

> A thread often acts in response to the action of another thread. If the other thread's action is also a response to the action of another thread, then livelock may result. As with deadlock, livelocked threads are unable to make further progress. However, the threads are not blocked — they are simply too busy responding to each other to resume work. This is comparable to two people attempting to pass each other in a corridor: Alphonse moves to his left to let Gaston pass, while Gaston moves to his right to let Alphonse pass. Seeing that they are still blocking each other, Alphone moves to his right, while Gaston moves to his left. They're still blocking each other, so...

# Guarded Block

Threads often have to coordinate their actions. The most common coordination idiom is the guarded block. Such a block begins by polling a condition that must be true before the block can proceed.

E.g: 
```java
public synchronized void guardedJoy() {
    // This guard only loops once for each special event, which may not
    // be the event we're waiting for.
    while(!joy) {
        try {
            wait();
        } catch (InterruptedException e) {}
    }
    System.out.println("Joy and efficiency have been achieved!");
}
```

Why is this version of guardedJoy synchronized? Suppose d is the object we're using to invoke wait. When a thread invokes d.wait, it must own the intrinsic lock for d — otherwise an error is thrown. Invoking wait inside a synchronized method is a simple way to acquire the intrinsic lock.

# Immutable objects

Immutable objects are useful in concuency as they cannot be corrupted by thread interference or observed in an inconsistent state.

## A Strategy for Defining Immutable Objects

1. Don't provide "setter" methods — methods that modify fields or objects referred to by fields.
2. Make all fields final and private.
3. Don't allow subclasses to override methods. The simplest way to do this is to declare the class as final. A more sophisticated approach is to make the constructor private and construct instances in factory methods.
4. If the instance fields include references to mutable objects, don't allow those objects to be changed:
	1. Don't provide methods that modify the mutable objects.
	2. Don't share references to the mutable objects. Never store references to external, mutable objects passed to the constructor; if necessary, create copies, and store references to the copies. Similarly, create copies of your internal mutable objects when necessary to avoid returning the originals in your methods.