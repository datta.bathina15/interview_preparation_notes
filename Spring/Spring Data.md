# Spring Data
## Repository Interface
At the core of the Spring Data abstraction is `Repository`. This takes in the domain class and `id`.

It is a marker interface to help you discover the **types**(remember we pass the domain class) to work with and discover the interfaces that extend this. The [`CrudRepository`](https://docs.spring.io/spring-data/commons/docs/current/api/org/springframework/data/repository/CrudRepository.html) interface provides sophisticated CRUD functionality for the entity class that is being managed.

# Transaction Management

==Transaction represents a single unit of work.==

The ACID properties describes the transaction management well. ACID stands for Atomicity, Consistency, isolation and durability.

**Atomicity** means either all successful or none.

**Consistency** ensures bringing the database from one consistent state to another consistent state.

**Isolation** ensures that transaction is isolated from other transaction.

**Durability** means once a transaction has been committed, it will remain so, even in the event of errors, power loss etc.

==We can use `@Transactional` to wrap a method in a database transaction.==

# Spring Data Repositories
JpaRepository – which extends PagingAndSortingRepository and, in turn, the CrudRepository.

Each of these defines its own functionality:

CrudRepository provides CRUD functions
PagingAndSortingRepository provides methods to do pagination and sort records
**JpaRepository provides JPA related methods such as flushing the persistence context and delete records in a batch**
