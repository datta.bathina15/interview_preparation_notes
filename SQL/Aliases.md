# Aliases
Aliases are the temporary names given to table or column for the purpose of a particular SQL query.

- For column alias:

```sql
SELECT column as alias_name FROM table_name;
```
**column:** fields in the table
**alias_name:** temporary alias name to be used in replacement of original column name 
**table_name:** name of table

- For table alias:

```sql
SELECT column FROM table_name as alias_name;
```
**column:** fields in the table 
**table_name:** name of table
**alias_name:** temporary alias name to be used in replacement of original table name