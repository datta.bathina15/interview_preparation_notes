Threads are created usign `Thread` class. 

> Every thread has a priority. Threads with higher priority are executed in preference to threads with lower priority. Each thread may or may not also be marked as a daemon. When code running in some thread creates a new Thread object, ==the new thread has its priority initially set equal to the priority of the creating thread==, and is a daemon thread if and only if the creating thread is a daemon.

The Java Virtual Machine continues to execute threads until either of the following occurs:

* The exit method of class Runtime has been called and the security manager has permitted the exit operation to take place.
* All threads that are not daemon threads have died, either by returning from the call to the run method or by throwing an exception that propagates beyond the run method.

**Every thread has a name for identification purposes**. More than one **thread may have the same name**. If a name is not specified when a thread is created, a new name is generated for it.

## Defining and Starting a Thread

Two ways to start a thread.

### Runnable

[Runnable](https://docs.oracle.com/javase/8/docs/api/java/lang/Runnable.html) is a funtional interface. 

E.g 
```java
public class HelloRunnable implements Runnable {

    public void run() {
        System.out.println("Hello from a thread!");
    }

    public static void main(String args[]) {
        (new Thread(new HelloRunnable())).start();
    }

}
```

Runnable is preferred over Thread class if you only have the use for `run()` method. 

### Thread

By subclassing Thread class we can create threads. 

==The Thread class itself implements Runnable.==

E.g.
```java
public class HelloThread extends Thread {

    public void run() {
        System.out.println("Hello from a thread!");
    }

    public static void main(String args[]) {
        (new HelloThread()).start();
    }

}
```

`Runnable` is preferred over `Thread` because the Runnable object can **subclass a class other than Thread**.

## Pausing Execution with Sleep

`Thread.sleep` method is used to suspend the current thread's execution for specific period.

Two overloaded methods are provided for sleep:
1. One that specifies the time in milli seconds
2. One that specifies the time in nano seconds.

> However, these sleep times are not guaranteed to be precise, because they are limited by the facilities provided by the underlying OS.

`Thread.sleep` throws **`InterruptedException`**. This is an exception that sleep throws when another thread interrupts the current thread while sleep is active.

```java
public class SleepMessages {
    public static void main(String args[])
        throws InterruptedException {
        String importantInfo[] = {
            "Mares eat oats",
            "Does eat oats",
            "Little lambs eat ivy",
            "A kid will eat ivy too"
        };

        for (int i = 0;
             i < importantInfo.length;
             i++) {
            //Pause for 4 seconds
            Thread.sleep(4000);
            //Print a message
            System.out.println(importantInfo[i]);
        }
    }
}
```

### Difference between sleep and wait
* * *
`Thread.sleep` suspends the method for given period of time and **The thread does not lose ownership of any monitors**.

* * *
`wait()` Causes the current thread to wait until another thread invokes the notify() method or the notifyAll() , or a specified amount of time has elapsed.
The current thread must own this object's monitor.
This method causes the current thread (call it T) to place itself in the wait set for this object and then to relinquish any and all synchronization claims on this object.
The thread starts execution once it regains the synchronization right on the object. 
> once it has gained control of the object, all its synchronization claims on the object are restored to the status quo ante - that is, to the situation as of the time that the wait method was invoked. Thread T then returns from the invocation of the wait method. Thus, on return from the wait method, the synchronization state of the object and of thread T is exactly as it was when the wait method was invoked.

`wait()` is a method of the `Object`  class  and inherited by the `Thread` class.
* * *

## Interrupts

==An interrupt is an indication to a thread that it should stop what it is doing and do something else.==

A thread sends an interrupt by invoking `interrupt` on the Thread object for the thread to be interrupted.

> Many methods that throw InterruptedException, such as sleep, are designed to cancel their current operation and return immediately when an interrupt is received.

**The interrupt mechanism is implemented using an internal flag known as the interrupt status.** Invoking `Thread.interrupt` sets this flag. When a thread checks for an interrupt by invoking the static method `Thread.interrupted`, interrupt status is cleared. The non-static `isInterrupted` method, which is used by one thread to query the interrupt status of another, does not change the interrupt status flag.

## Joins

==The join method allows one thread to wait until the completion of others.==

```java
t.join();
```

`join()` also provides overloaded method which take in time. 

Like sleep, join responds to an interrupt by exiting with an InterruptedException.

