# COMMIT

```sql
COMMIT;

-- Example
DELETE FROM Student WHERE AGE = 20;
COMMIT;
```

# ROLLBACK

```sql
ROLLBACK;

-- Example
DELETE FROM Student WHERE AGE = 20;
ROLLBACK;
```

# SAVEPOINT

A SAVEPOINT is a point in a transaction in which you can roll the transaction back to a certain point without rolling back the entire transaction. 

```sql
SAVEPOINT SAVEPOINT_NAME;

ROLLBACK TO SAVEPOINT_NAME;

-- Example
SAVEPOINT SP1;
-- Savepoint created.
DELETE FROM Student WHERE AGE = 20;
-- deleted
SAVEPOINT SP2;
-- Savepoint created.
```

##  RELEASE SAVEPOINT

This command is used to remove a SAVEPOINT that you have created. 

```sql
RELEASE SAVEPOINT SAVEPOINT_NAME
```

# SET TRANSACTION

```sql
BEGIN TRANSACTION transaction_name ;

SET TRANSACTION [ READ WRITE | READ ONLY ];

```

## What are Transactions?

Transactions group a set of tasks into a single execution unit. Each transaction begins with a specific task and ends when all the tasks in the group successfully complete. If any of the tasks fail, the transaction fails. Therefore, a transaction has only two results: success or failure. 