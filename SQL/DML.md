# INSERT

```sql
INSERT INTO tablename (column1, column2, column3, ...)
    VALUES (value1, value2, value3, ...);

-- If you are adding values for all the columns of the table, you do not need to specify the column names in the SQL query.
INSERT INTO tablename
    VALUES (value1, value2, value3, ...);
```

# UPDATE

```sql
UPDATE tablename
    SET column1 = value1, column2 = value2, ...
    WHERE condition;
	
-- Example 
UPDATE patients
    SET first_name = 'John', weight = 120
    WHERE patient_id = 1;

-- UPDATE Multiple Records
UPDATE patients
    SET allergies = NULL
    WHERE allergies = 'NKA';
```

# Delete

```sql
DELETE FROM tablename WHERE condition;
-- Example
DELETE FROM patients WHERE first_name = 'Paul';
```