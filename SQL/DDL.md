# CREATE

## Create Database

```SQL
CREATE DATABASE database_name;
```

## Create Table

```SQL
CREATE TABLE table_name (
    column1 datatype,
    column2 datatype,
    column3 datatype,
   ....
);
```

### Create Table from another table

A copy of an existing table can also be created using CREATE TABLE.

The new table gets the same column definitions. All columns or specific columns can be selected.

If you create a new table using an existing table, the new table will be filled with the existing values from the old table.
Syntax

```sql
CREATE TABLE new_table_name AS

SELECT column1, column2,...

FROM existing_table_name

WHERE ....;
```

The following SQL creates a new table called "TestPatients" (which is a copy of the "Patients" table with the patient_id, first_name, last_name copied):
```sql
CREATE TABLE TestPatients AS

SELECT patient_id,first_name, last_name

FROM patients;
```

## SQL Create Constraints

```sql
CREATE TABLE table_name (
    column1 datatype constraint,
    column2 datatype constraint,
    column3 datatype constraint,
    ....
);

-- Example
CREATE TABLE Persons (
	ID int NOT NULL UNIQUE, /* Mulitple constraints example */
	LastName varchar(255) NOT NULL,
	FirstName varchar(255) NOT NULL,
	Age int,
	City varchar(255) DEFAULT 'Sandnes' /* Default Constraint */
);
```

- NOT NULL - Ensures that a column cannot have a NULL value
- UNIQUE - Ensures that all values in a column are different
- PRIMARY KEY - A combination of a NOT NULL and UNIQUE. Uniquely identifies each row in a table
- FOREIGN KEY - Prevents actions that would destroy links between tables
- CHECK - Ensures that the values in a column satisfies a specific condition
- DEFAULT - Sets a default value for a column if no value is specified

## PRIMARY KEY

```sql
-- MySQL:

CREATE TABLE Persons (
    ID int NOT NULL,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (ID)
);
```
[PRIMARY KEY on ALTER TABLE](#primary-key-on-alter-table)

## FOREIGN KEY

A FOREIGN KEY is a field (or collection of fields) in one table, that refers to the PRIMARY KEY in another table.

The following SQL creates a FOREIGN KEY on the "PersonID" column when the "Orders" table is created:
```sql
CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    FOREIGN KEY (PersonID) REFERENCES Persons(PersonID)
);
```

To allow naming of a FOREIGN KEY constraint, and for defining a FOREIGN KEY constraint on multiple columns, use the following SQL syntax:

```sql
CREATE TABLE Orders (
    OrderID int NOT NULL,
    OrderNumber int NOT NULL,
    PersonID int,
    PRIMARY KEY (OrderID),
    CONSTRAINT FK_PersonOrder FOREIGN KEY (PersonID)
    REFERENCES Persons(PersonID)
);
```

[Foreign Key through ALTER command](#foreign-key-on-alter-table)

# DROP

```sql
DROP object object_name

DROP TABLE table_name;
-- table_name: Name of the table to be deleted.

DROP DATABASE database_name;
-- database_name: Name of the database to be deleted.
```

# ALTER

The ALTER TABLE statement is used to add, delete, or modify columns in an existing table.

The ALTER TABLE statement is also used to add and drop various constraints on an existing table.

## ADD COLUMN

```sql
ALTER TABLE table_name ADD column_name datatype;

-- Example
-- The following SQL adds an "Email" column to the "Customers" table:
ALTER TABLE Customers ADD Email varchar(255);
```

## DROP COLUMN 

```sql
ALTER TABLE table_name DROP COLUMN column_name;

-- Example
ALTER TABLE patients DROP COLUMN last_name;
```

## ALTER/MODIFY COLUMN

```sql
-- SQL Server / MS Access
ALTER TABLE table_name ALTER COLUMN column_name datatype;

-- My SQL / Oracle (prior version 10G):
ALTER TABLE table_name MODIFY COLUMN column_name datatype;

-- Oracle 10G and later:
ALTER TABLE table_name MODIFY column_name datatype;
```

## PRIMARY KEY on ALTER TABLE

```sql
ALTER TABLE Persons ADD PRIMARY KEY (ID);
```

To allow naming of a PRIMARY KEY constraint, and for defining a PRIMARY KEY constraint on multiple columns, use the following SQL syntax:
```sql
ALTER TABLE Persons
	ADD CONSTRAINT PK_Person PRIMARY KEY (ID,LastName);
```

### DROP a PRIMARY KEY Constraint

```sql
ALTER TABLE Persons DROP PRIMARY KEY;

-- Delete a named constraint
ALTER TABLE Persons DROP CONSTRAINT PK_Person;
```

## FOREIGN KEY on ALTER TABLE

To create a FOREIGN KEY constraint on the "PersonID" column when the "Orders" table is already created, use the following SQL:

```sql
ALTER TABLE Orders ADD FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);
```

To allow naming of a FOREIGN KEY constraint, and for defining a FOREIGN KEY constraint on multiple columns, use the following SQL syntax:

```sql
ALTER TABLE Orders
	ADD CONSTRAINT FK_PersonOrder
	FOREIGN KEY (PersonID) REFERENCES Persons(PersonID);
```

### DROP a FOREIGN KEY Constraint

```sql
ALTER TABLE Orders DROP FOREIGN KEY FK_PersonOrder;
```

## DEFAULT on ALTER TABLE

```sql
ALTER TABLE Persons ALTER City SET DEFAULT 'Sandnes';
```

### DROP a DEFAULT Constraint
```sql
ALTER TABLE Persons ALTER City DROP DEFAULT;
```

## AUTO INCREMENT Field

Auto-increment allows a unique number to be generated automatically when a new record is inserted into a table.

```sql
CREATE TABLE Persons (
    Personid int NOT NULL AUTO_INCREMENT,
    LastName varchar(255) NOT NULL,
    FirstName varchar(255),
    Age int,
    PRIMARY KEY (Personid)
);
```

To let the AUTO_INCREMENT sequence start with another value, use the following SQL statement:
```sql
ALTER TABLE Persons AUTO_INCREMENT=100;
```

## INDEX

Indexes are used to retrieve data from the database more quickly than otherwise. The users cannot see the indexes, they are just used to speed up searches/queries.

```sql
CREATE INDEX index_name ON table_name (column1, column2, ...);

-- Creates a unique index on a table. Duplicate values are not allowed:
CREATE UNIQUE INDEX index_name ON table_name (column1, column2, ...);
```

> [! INFO]
> The column is necessary.

Examples:
```sql
/* The SQL statement below creates an index named "idx_lastname" on the "LastName" column in the "Persons" table:*/
CREATE INDEX idx_last_name ON patients (last_name);

/*combination of columns*/
CREATE INDEX idx_pname ON patients (last_name, first_name);
```

### DROP INDEX Statement
```sql
-- MySQL
ALTER TABLE table_name DROP INDEX index_name;

-- DB2/Oracle
DROP INDEX index_name;
```

# TRUNCATE

DROP is used to delete a whole database or just a table.

TRUNCATE statement is a Data Definition Language (DDL) operation that is used to mark the extents of a table for deallocation (empty for reuse).

```sql
TRUNCATE TABLE table_name;
```

## DROP vs TRUNCATE

- Truncate is normally ultra-fast and its ideal for deleting data from a temporary table.
- Truncate preserves the structure of the table for future use, unlike drop table where the table is deleted with its full structure.
- Table or Database deletion using DROP statement cannot be rolled back, so it must be used wisely.

# COMMENTS

## Single line comments
```sql
-- single line comment
-- another comment
SELECT * FROM Customers;
```

## Multi line comments
```sql
/* multi line comment
another comment */
SELECT * FROM Customers; 
```

## In line comments
```sql
SELECT * FROM /* Customers; */ 
```

# RENAME

```sql
ALTER TABLE table_name RENAME TO new_table_name;
```
Columns can be also be given new name with the use of ALTER TABLE.
```sql
-- Syntax(MySQL, Oracle):
ALTER TABLE table_name RENAME COLUMN old_name TO new_name;
```